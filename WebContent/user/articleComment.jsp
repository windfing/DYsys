<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>养生知识</title>

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0,user-scalable=0,minimal-ui">
<meta name="format-detection" content="telephone=no" />
<meta name="description"
	content="MetInfo企业建站系统专注于为中小企业提供高质量的建站服务，海量模板请登录www.metinfo.cn，本站为中医养生馆行业响应式网站模板演示站" />
<meta name="keywords" content="中医养生馆行业网站模版,中医养生馆行业网页模版,响应式模版,网站制作,网站建站" />
<meta name="generator" content="MetInfo 6.0.0"
	data-variable="other/|cn|18|0|2|mui145" />
<link href="other/favicon.ico" rel="shortcut icon" type="image/x-icon" />
<link rel='stylesheet' type='text/css'
	href='${pageContext.request.contextPath}/user/css/basic.css'>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/user/css/news_cn.css?1553567240" />

<link href="${pageContext.request.contextPath}/user/css/bootstrap.css"
	type="text/css" rel="stylesheet" media="all">
<link href="${pageContext.request.contextPath}/user/css/style.css"
	type="text/css" rel="stylesheet" media="all">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/user/css/flexslider.css"
	type="text/css" media="screen" />
<!-- js -->
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
<!-- //js -->
<!-- start-smoth-scrolling-->
<script type="text/javascript"
	src="${pageContext.request.contextPath}/user/js/move-top.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/user/js/easing.js"></script>

<script defer
	src="${pageContext.request.contextPath}/user/js/jquery.flexslider.js"></script>
<script src="${pageContext.request.contextPath}/user/js/classie.js"></script>
<script src="${pageContext.request.contextPath}/user/js/uisearch.js"></script>
<script
	src="${pageContext.request.contextPath}/user/js/responsiveslides.min.js"></script>

<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/user/layui/css/layui.css">
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/user/res/css/main.css">

<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event) {
			event.preventDefault();
			$('html,body').animate({
				scrollTop : $(this.hash).offset().top
			}, 1000);
		});
	});
</script>
<!--//end-smoth-scrolling-->
<!--pop-up-->
<script src="${pageContext.request.contextPath}/user/js/menu_jquery.js"></script>


<style>
body {
	background-image: url(other/);
	background-position: center;
	background-repeat: no-repeat;
	background-color: #ffffff;
	font-family: other/;
}
</style>
<body class="met-navfixed     ny-banner ">
	<header class='met-head navbar-fixed-top' m-id='4' m-type='head_nav'
		met-imgmask> <nav
		class="navbar navbar-default box-shadow-none head_nav_met_16_4     ">
	<div class="container">
		<div class="row">
			<h3 hidden></h3>
			<button type="button"
				class="navbar-toggler hamburger hamburger-close collapsed p-x-5 head_nav_met_16_4-toggler"
				data-target="#head_nav_met_16_4-collapse" data-toggle="collapse">
				<span class="sr-only"></span> <span class="hamburger-bar"></span>
			</button>
			<!-- 会员注册登录 -->

			<!-- 会员注册登录 -->
			<!-- 导航 -->
			<%@include file="header.jsp"%>
			<!-- 导航 -->
		</div>
	</div>
	</nav> </header>
	<div class="banner_met_16_2 page-bg" data-height='' style='' m-id='3' m-type='banner'>
				<div class="slick-slide">
					<img class="cover-image" src="${pageContext.request.contextPath}/user/images/1533699549.jpg" srcset='${pageContext.request.contextPath}/user/images/1533699549.jpg 767w,${pageContext.request.contextPath}/user/images/1533699549.jpg' sizes="(max-width: 767px) 767px" alt="" data-height='0|0|0'>
				</div>
				<div class="slick-slide">
					<img class="cover-image" src="${pageContext.request.contextPath}/user/images/1527575184.jpg" srcset='${pageContext.request.contextPath}/user/images/1527575184.jpg 767w,${pageContext.request.contextPath}/user/images/1527575184.jpg' sizes="(max-width: 767px) 767px" alt="" data-height='0|0|0'>
				</div>
			</div>
	<div class="content whisper-content leacots-content details-content">
		<div class="cont w1000">
			<div class="whisper-list">
				<div class="item-box">
					<div class="review-version">
						<div class="form-box">
							<div class="article-cont">
								<div class="title">
									<h3>${kShow.kTitle}</h3>
									<p class="cont-info">
										<span class="data">${kSow.kTime}</span><span class="types">${kSow.issuer}</span>
									</p>
								</div>
				${kShow.kContent}

							</div>
							<div class="form">
								<form class="layui-form" action="">
									<div class="layui-form-item layui-form-text">
										<div class="layui-input-block">
											<textarea name="kdContent" placeholder="既然来了，就说几句"
												class="layui-textarea" id="kdCon"></textarea>
										</div>
									</div>
									<div class="layui-form-item">
										<div class="layui-input-block" style="text-align: right;">
											<input type="button" class="layui-btn definite" value="发布" id="btnIssue">
										</div>
									</div>
								</form>
							</div>
						</div>
						<div class="volume">
							全部评论 <span>${count}</span>
						</div>
	
	
						<div class="list-cont">
						<c:forEach items="${list}" var="u">
							<c:if test="${empty u.dName}">
							<div class="cont">
								<div class="text">
									<p class="tit">
										<span class="name">用户${u.uName}</span>
										<span class="data">${u.kdTime}</span>
									</p>
									<p class="ct">${u.kdContent}</p>
								</div>
							</div>
							</c:if>
							
							<c:if test="${empty u.uName}">
							<div class="cont">
								<div class="text">
									<p class="tit">
										<span class="name">医生${u.dName}</span>
										<span class="data">${u.kdTime}</span>
									</p>
									<p class="ct">${u.kdContent}</p>
								</div>
							</div>
							</c:if>
						</c:forEach>

						</div>
					</div>
				</div>
			</div>
			<div id="demo" style="text-align: center;"></div>
		</div>
	</div>
	<%@include file="foot.jsp"%>
	
			<!-- 登录的方法 及样式的链接 -->
	<link rel="stylesheet"
		href="${pageContext.request.contextPath}/user/layui/css/layui.css"
		media="all">
	<script src="${pageContext.request.contextPath}/user/layui/layui.js"
		charset="utf-8"></script>
						
		<script type="text/javascript">
			$(function(){
				$("#login").click(function(){
					$.post("${pageContext.request.contextPath}/ULogin.action?op=login",$("#loginForm").serialize(),function(data,status){
						if(data.msg=="用户登录成功"||data.msg=="医生登录成功"){
						layui.use(['laypage','layer' ],function() {
							var laypage = layui.laypage, layer = layui.layer;
							layer.msg(data.msg,{icon : 1,time : 2000	},
									function() {location.reload();
									});
							});
						}else {
							layui.use(['laypage','layer' ],function() {
								var laypage = layui.laypage, layer = layui.layer;
								layer.msg(data.msg,{icon : 2,time : 2000	});
								});
						}
					});
				});
				

				
				
				
			})

					
		</script>
	
	<script
		src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/admin/lib/layer/2.4/layer.js"></script>
					<!-- 退出登录的方法 -->
	<script type="text/javascript">
			$(function(){
				$("#exit").click(function(){
					$.get("${pageContext.request.contextPath}/ULogin.action","op=exit",function(data,status){
						location.href="${pageContext.request.contextPath}/user/index.jsp";
					});
				});
			});
		</script>
	
	<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
	<script src="${pageContext.request.contextPath}/admin/lib/layer/2.4/layer.js" charset="utf-8"></script>
	<script type="text/javascript">
		 $(function(){
			$("#btnIssue").click(function(){
				layui.use(['laypage','layer' ],function() {
					var laypage = layui.laypage, layer = layui.layer;
				
					
				if(${!empty users}||${!empty doctor}){
					if($("#kdCon").val().trim()!=null&&$("#kdCon").val().trim()!=""){
						 if($("#kdCon").val().trim().length<=200){
							 $.ajax({
									type:"get",
									url:"UKnowledgedisServlet.action",
									data:{
										op : "addkd",
										kdContent : $("#kdCon").val(),
										kId:"${param.kId}"   //获取article传过来的kId
									},
									success:function(data) {
										//如果信息是评论成功，就提示信息，重新加载页面
										  if (data == "评论成功") {
											  
											layer.msg(data, {
												icon : 1,
												time : 1000

											}, function() {
												location.reload();
											});
										}  
										
									}
									
								});
						}  else{
							layer.msg("评论长度不能超过200！", {
								icon : 5,
								time : 1000

							});
						}
						
						
					}else{
						layer.msg("评论不能为空！", {
							icon : 5,
							time : 1000

						});
					}
				}else{
					layer.msg("请先登录！", {
						icon : 5,
						time : 1000

					});

			}
				
				
			});

			});
		}); 
	
	</script>

</body>
</html>