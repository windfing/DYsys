<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
     <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html> 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>在线预约</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=0,minimal-ui">
		<meta name="format-detection" content="telephone=no" />
		<meta name="description" content="MetInfo企业建站系统专注于为中小企业提供高质量的建站服务，海量模板请登录www.metinfo.cn，本站为中医养生馆行业响应式网站模板演示站" />
		<meta name="keywords" content="中医养生馆行业网站模版,中医养生馆行业网页模版,响应式模版,网站制作,网站建站" />
		<meta name="generator" content="MetInfo 6.0.0" data-variable="other/|cn|23|23|8|mui145" />
		<link href="other/favicon.ico" rel="shortcut icon" type="image/x-icon" />
		<link rel='stylesheet' type='text/css' href='${pageContext.request.contextPath}/user/css/basic.css'>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/user/css/feedback_cn.css?1553649247" />
		
		<link href="${pageContext.request.contextPath}/user/css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
<link href="${pageContext.request.contextPath}/user/css/style.css" type="text/css" rel="stylesheet" media="all">
<link rel="stylesheet" href="${pageContext.request.contextPath}/user/css/flexslider.css" type="text/css" media="screen" />
<!-- js -->
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
<!-- //js -->	
<!-- start-smoth-scrolling-->
<script type="text/javascript" src="${pageContext.request.contextPath}/user/js/move-top.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/user/js/easing.js"></script>	

<script defer src="${pageContext.request.contextPath}/user/js/jquery.flexslider.js"></script>
<script src="${pageContext.request.contextPath}/user/js/classie.js"></script>
<script src="${pageContext.request.contextPath}/user/js/uisearch.js"></script>
<script src="${pageContext.request.contextPath}/user/js/responsiveslides.min.js"></script>

<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});
		});
</script>
<!--//end-smoth-scrolling-->
<!--pop-up-->
<script src="${pageContext.request.contextPath}/user/js/menu_jquery.js"></script>
		
		
		<style>
			body {
				background-image: url(other/);
				background-position: center;
				background-repeat: no-repeat;
				background-color: #ffffff;
				font-family: other/;
			}
		</style>
		<!--[if lte IE 9]>
<script src="other/public/ui/v2/static/js/lteie9.js"></script>
<![endif]-->
	</head>
	<!--[if lte IE 8]>
<div class="text-xs-center m-b-0 bg-blue-grey-100 alert">
    <button type="button" class="close" aria-label="Close" data-dismiss="alert">
        <span aria-hidden="true">×</span>
    </button>
    你正在使用一个 <strong>过时</strong> 的浏览器。请 <a href=https://browsehappy.com/ target=_blank>升级您的浏览器</a>，以提高您的体验。</div>
<![endif]-->

	<body>

		<body class="met-navfixed     ny-banner ">
			<header class='met-head navbar-fixed-top' m-id='4' m-type='head_nav' met-imgmask>
				<nav class="navbar navbar-default box-shadow-none head_nav_met_16_4     ">
					<div class="container">
						<div class="row">
							<h3 hidden></h3>
							<!-- logo 
							<div class="navbar-header pull-xs-left">
								<a href="other/" class="met-logo vertical-align block pull-xs-left p-y-5" title="">
									<div class="vertical-align-middle">
										<img src="images/1532482808.png" alt="" class="logo">
										<img src="images/1532483375.png" alt="" class="logo1  hidden">
									</div>
								</a>
							</div>
							logo -->
							<button type="button" class="navbar-toggler hamburger hamburger-close collapsed p-x-5 head_nav_met_16_4-toggler" data-target="#head_nav_met_16_4-collapse" data-toggle="collapse">
                    <span class="sr-only"></span>
                    <span class="hamburger-bar"></span>
                </button>
							<!-- 会员注册登录 -->

							<!-- 会员注册登录 -->
							<!-- 导航 -->
							<%@include file="header.jsp" %>
							<!-- 导航 -->
						</div>
					</div>
				</nav>
			</header>
			<div class="banner_met_16_2 page-bg" data-height='' style='' m-id='3' m-type='banner'>
				<div class="slick-slide">
					<img class="cover-image" src="images/1533699549.jpg" srcset='images/1533699549.jpg 767w,images/1533699549.jpg' sizes="(max-width: 767px) 767px" alt="" data-height='0|0|0'>
				</div>
				<div class="slick-slide">
					<img class="cover-image" src="images/1527575184.jpg" srcset='images/1527575184.jpg 767w,images/1527575184.jpg' sizes="(max-width: 767px) 767px" alt="" data-height='0|0|0'>
				</div>
			</div>
			<section class="feedback_met_16_1 animsition" m-id='28' m-type="nocontent">
				<div class="container">
					<div class="row">
						<div class="feedback_met_16_1-body">
							<div m-id='28' m-type='feedback'>

								<form class="met-form met-form-validation" enctype="multipart/form-data">
									<!-- <input type='hidden' name='id' value="23" /> -->
									<!-- <input type='hidden' name='lang' value="cn" /> -->
									<!-- <input type='hidden' name='fdtitle' value='在线反馈' /> -->
									<div class='form-group'><input name='uName' id="uName" class='form-control' type='text' value='${uName} ' /></div>
									<div class='form-group'><input name='aTime' id="aTime" class='form-control' type='text' placeholder='预约时间' /></div>
									
									<div class='form-group'>
										<select name='aService' id="aService" class='form-control' data-fv-notempty="true" data-fv-message="不能为空">
											<option value=''>预约项目</option>
											<option value='疏通十二经络'>疏通十二经络</option>
											<option value='全息塔灸'>全息塔灸</option>
											<option value='太极足心道'>太极足心道</option>
											<option value='红砭背部经络刮痧'>红砭背部经络刮痧</option>
											<option value='内功正骨'>内功正骨</option>
											<option value='排湿寒火罐'>排湿寒火罐</option>
											<option value='中药养筋浴'>中药养筋浴</option>
										</select>
									</div>
									<!-- <div class='form-group'><input name='aStatus' id="aStatus" class='form-control' type='text' placeholder='状态' /></div> -->
									
									
									<div class="form-group m-b-0">
										<button type="button" id="btnAdd" class="btn btn-primary btn-lg btn-block btn-squared">预约服务</button>
				 					</div>
								</form>
							</div> 
						</div>
					</div>
				</div>
			<%-- <%@include file="foot.jsp" %> --%>
			<input type="hidden" name="met_lazyloadbg" value="other/">
			<script src="${pageContext.request.contextPath}/user/js/basic.js"></script>
			
			
				
		<!-- 登录的方法 及样式的链接 -->
	<link rel="stylesheet"
		href="${pageContext.request.contextPath}/user/layui/css/layui.css"
		media="all">
	<script src="${pageContext.request.contextPath}/user/layui/layui.js"
		charset="utf-8"></script>
						
		<script type="text/javascript">
			$(function(){
				$("#login").click(function(){
					$.post("${pageContext.request.contextPath}/ULogin.action?op=login",$("#loginForm").serialize(),function(data,status){
						if(data.msg=="用户登录成功"||data.msg=="医生登录成功"){
						layui.use(['laypage','layer' ],function() {
							var laypage = layui.laypage, layer = layui.layer;
							layer.msg(data.msg,{icon : 1,time : 2000	},
									function() {location.reload();
									});
							});
						}else {
							layui.use(['laypage','layer' ],function() {
								var laypage = layui.laypage, layer = layui.layer;
								layer.msg(data.msg,{icon : 2,time : 2000	});
								});
						}
					});
				});
			});
			
			$(function(){
				$("#btnAdd").click(function(){
					
					$.ajax({
						type:"post",
						dataType:"json",
						url:"${pageContext.request.contextPath}/UAppointment.action?op=addAppAjax",
						data:{
							uName:$("#uName").val(),
							aTime:$("#aTime").val(),
							aDoctor:$("#aDoctor").val(),
							aService:$("#aService").val(),
							/* aStatus:$("aStatus").val() */
							uId:"${users.uId}"
						},
						success:function(data){
							if(data.msg=="预约成功"){
								alert("预约成功!");
								location.reload();
							}
						}
					});
					
					
				});
				
				
			});
			
			
			
		</script>
			
			
				<script type="text/javascript" src="${pageContext.request.contextPath}/admin/lib/layer/2.4/layer.js"></script>
					<!-- 退出登录的方法 -->
	<script type="text/javascript">
			$(function(){
				$("#exit").click(function(){
					$.get("${pageContext.request.contextPath}/ULogin.action","op=exit",function(data,status){
						location.href="${pageContext.request.contextPath}/user/index.jsp";
					});
				});
			});
		</script>
		
		
		
		</body>

</html>