<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<title>用户注册</title>
<meta name="renderer" content="webkit">
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0,user-scalable=0,minimal-ui">
<meta name="format-detection" content="telephone=no" />
<meta name="generator" content="MetInfo 6.0.0"
	data-variable="other/|cn|3|0|3|mui145" />
<link href="other/favicon.ico" rel="shortcut icon" type="image/x-icon" />
<link rel='stylesheet' type='text/css' href='${pageContext.request.contextPath}/user/css/basic.css'>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/user/css/product_cn.css?1553566664" />

<link href="${pageContext.request.contextPath}/user/css/bootstrap.css" type="text/css" rel="stylesheet"
	media="all">
<link href="${pageContext.request.contextPath}/user/css/style.css" type="text/css" rel="stylesheet" media="all">
<link href="${pageContext.request.contextPath}/user/css/bootstrap.css" type="text/css" rel="stylesheet"
	media="all">
<link href="${pageContext.request.contextPath}/user/css/style.css" type="text/css" rel="stylesheet" media="all">
<link rel="stylesheet" href="${pageContext.request.contextPath}/user/css/flexslider.css" type="text/css"
	media="screen" />
<!-- js -->
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
<!-- //js -->
<!-- start-smoth-scrolling-->
<script type="text/javascript" src="${pageContext.request.contextPath}/user/js/move-top.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/user/js/easing.js"></script>


<script defer src="${pageContext.request.contextPath}/user/js/jquery.flexslider.js"></script>
<script src="${pageContext.request.contextPath}/user/js/classie.js"></script>
<script src="${pageContext.request.contextPath}/user/js/uisearch.js"></script>
<script src="${pageContext.request.contextPath}/user/js/responsiveslides.min.js"></script>


<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event) {
			event.preventDefault();
			$('html,body').animate({
				scrollTop : $(this.hash).offset().top
			}, 1000);
		});
	});
</script>
<!--//end-smoth-scrolling-->
<!--pop-up-->
<script src="${pageContext.request.contextPath}/user/js/menu_jquery.js"></script>


<style>
body {
	background-image: url(other/);
	background-position: center;
	background-repeat: no-repeat;
	background-color: #ffffff;
	font-family: other/;
}
</style>
</head>
<body class="met-navfixed     ny-banner ">
	<header class='met-head navbar-fixed-top' m-id='4' m-type='head_nav'
		met-imgmask> <nav
		class="navbar navbar-default box-shadow-none head_nav_met_16_4     ">
	<div class="container">
		<div class="row">
			<h3 hidden></h3>
			<button type="button"
				class="navbar-toggler hamburger hamburger-close collapsed p-x-5 head_nav_met_16_4-toggler"
				data-target="#head_nav_met_16_4-collapse" data-toggle="collapse">
				<span class="sr-only"></span> <span class="hamburger-bar"></span>
			</button>
			<!-- 会员注册登录 -->

			<!-- 会员注册登录 -->
			<!-- 导航 -->
			<%@include file="header.jsp"%>
			<!-- 导航 -->
		</div>
	</div>
	</nav> </header>

	<div class="banner_met_16_2 page-bg" data-height='' style='' m-id='3'
		m-type='banner'>
		<div class="slick-slide">
			<img class="cover-image" src="${pageContext.request.contextPath}/user/images/1533699549.jpg"
				srcset='${pageContext.request.contextPath}/user/images/1533699549.jpg 767w,${pageContext.request.contextPath}/user/images/1533699549.jpg'
				sizes="(max-width: 767px) 767px" alt="" data-height='0|0|0'>
		</div>
		<div class="slick-slide">
			<img class="cover-image" src="${pageContext.request.contextPath}/user/images/1527575184.jpg"
				srcset='${pageContext.request.contextPath}/user/images/1527575184.jpg 767w,${pageContext.request.contextPath}/user/images/1527575184.jpg'
				sizes="(max-width: 767px) 767px" alt="" data-height='0|0|0'>
		</div>
	</div>
	<div
		class="product_list_page_met_21_6 met-product animsition 
	    	    	    	    "
		m-id='55'>
		<div class="account">
			<div class="container">
				<div class="account" align="center">
					<h1>
						新用户？<span> 注册一个账号 </span>
					</h1>
					<!-- Form -->

					<div class="row">
						<div class="col-xs-3 col-md-3 "></div>
						<div class="registration_form col-xs-12 col-md-6" align="center">
							<form id="registration_form" >
								<div>
									<label> <input placeholder="用户名" type="text"
										name="uName" tabindex="1" id="uName">
									</label>
								</div>
								<div>
									<label> <input placeholder="密码" type="password"
										name="uPassword" tabindex="2">
									</label>
								</div>
								<div class="sky_form1">
									<ul>
										<li><label class="radio left"><input type="radio"
												value="男" name="uSex" checked=""><i></i>男</label></li>
										<li><label class="radio"><input type="radio"
												name="uSex" value="女"><i></i>女</label></li>
										<div class="clearfix"></div>
									</ul>
								</div>


								<div>
									<label> <input placeholder="手机号" type="text" id="uPhone"
										name="uPhone" tabindex="2">
									</label>
								</div>
								<div>
									<label> <input placeholder="生日" type="text"
										name="uBirthday" tabindex="3">
									</label>
								</div>
								<div>
									<label> <input placeholder="住址" type="text"
										name="uAddress" tabindex="3">
									</label>
								</div>


								<div>
									<button type="button"  class="btn  btn-lg btn-block btn-info"  value="" id="register">注册</button>
								</div>

							</form>
						</div>

						<!-- /Form -->
					</div>
				</div>
				<div class="col-md-4"></div>
			</div>



			<%@include file="foot.jsp"%>
			<footer class='foot_info_met_16_2 met-foot p-y-10 border-top1'
				m-id='47' m-type='foot'>
			<div class="container text-xs-center">
				<div>
					<span class="powered_by_metinfo">Powered by <b><a
							href=http://www.metinfo.cn target=_blank>MetInfo 6.0.0</a></b>
						&copy;2008-2019 &nbsp;<a href=http://www.metinfo.cn target=_blank>MetInfo
							Inc.</a></span>
				</div>
			</div>
			</footer>
			<button type="button"
				class="btn btn-icon btn-primary btn-squared back_top_met_16_1 met-scroll-top"
				hidden m-id='8' m-type='nocontent'>
				<i class="icon wb-chevron-up" aria-hidden="true"></i>
			</button>
			<input type="hidden" name="met_lazyloadbg" value="other/">
			<script src="${pageContext.request.contextPath}/user/js/basic.js"></script>
			<script src="${pageContext.request.contextPath}/user/js/product_cn.js?1553566664"></script>
			<link rel="stylesheet"
		href="${pageContext.request.contextPath}/user/layui/css/layui.css"
		media="all">
	<script src="${pageContext.request.contextPath}/user/layui/layui.js"
		charset="utf-8"></script>
		
	
   <script type="text/javascript">
		$(function(){
			$("#register").click(function(){
				$.post("../UUsers.action?op=regist",$("#registration_form").serialize(),function(data,status){
					layui.use(['laypage','layer' ],function() {
						var laypage = layui.laypage, layer = layui.layer;
						if(data.msg=="注册成功"){
						layer.msg(data.msg,{icon : 1,time : 2000	},
								function() {location.reload();
								});
						}else{
						layer.msg(data.msg,{icon : 2,time : 2000	},
								function() {location.reload();
								});
						}
						
						
						
						});
			});
		});
		});
		
		</script>
		
		
			
		<!-- 登录的方法 及样式的链接 -->
	<link rel="stylesheet"
		href="${pageContext.request.contextPath}/user/layui/css/layui.css"
		media="all">
	<script src="${pageContext.request.contextPath}/user/layui/layui.js"
		charset="utf-8"></script>
						
		<script type="text/javascript">
			$(function(){
				$("#login").click(function(){
					$.post("${pageContext.request.contextPath}/ULogin.action?op=login",$("#loginForm").serialize(),function(data,status){
						if(data.msg=="用户登录成功"||data.msg=="医生登录成功"){
						layui.use(['laypage','layer' ],function() {
							var laypage = layui.laypage, layer = layui.layer;
							layer.msg(data.msg,{icon : 1,time : 2000	},
									function() {location.reload();
									});
							});
						}else {
							layui.use(['laypage','layer' ],function() {
								var laypage = layui.laypage, layer = layui.layer;
								layer.msg(data.msg,{icon : 2,time : 2000	});
								});
						}
					});
				});
			});
		</script>
		
		
		
					<!-- 退出登录的方法 -->
	<script type="text/javascript">
			$(function(){
				$("#exit").click(function(){
					$.get("${pageContext.request.contextPath}/ULogin.action","op=exit",function(data,status){
						location.reload();
					});
				});
			});
		</script>
			<script type="text/javascript">
			$(function(){
				<!-- Ajax异步校验手机号   -->
				$("#uPhone").blur(function(){
					$.get("../UUsers.action?op=checkPhone","uPhone="+$(this).val(),function(data,status){
						layui.use(['laypage','layer' ],function() {
						var laypage = layui.laypage, layer = layui.layer;
						layer.tips(data.msg,'#uPhone');
						});
					});
				});
			});
			</script>
		<script type="text/javascript">
		<!-- Ajax异步校验用户名 -->
		$(function(){
			$("#uName").blur(function(){
				$.get("../UUsers.action?op=checkuName","uName="+$(this).val(),function(data,status){
					layui.use(['laypage','layer' ],function() {
					var laypage = layui.laypage, layer = layui.layer;
					layer.tips(data.msg,'#uName');
					});
				});
			});
		});
		</script>
			
			
			
</body>
</html>