<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%> 
     <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>

	<head>
		<title>团队介绍</title>
		<meta name="renderer" content="webkit">
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=0,minimal-ui">
		<meta name="format-detection" content="telephone=no" />
		<meta name="generator" content="MetInfo 6.0.0" data-variable="http://www.ayt999.com/|cn|2|2|1|mui145" />
		<link href="http://www.ayt999.com/favicon.ico" rel="shortcut icon" type="image/x-icon" />
		<link rel='stylesheet' type='text/css' href='http://www.ayt999.com/public/ui/v2/static/css/basic.css'>
		<link rel="stylesheet" type="text/css" href="http://www.ayt999.com/templates/mui145/cache/show_cn.css?1553579261" />

<link href="${pageContext.request.contextPath}/user/css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
<link href="${pageContext.request.contextPath}/user/css/style.css" type="text/css" rel="stylesheet" media="all">
<link rel="stylesheet" href="${pageContext.request.contextPath}/user/css/flexslider.css" type="text/css" media="screen" />
<!-- js -->
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
<!-- //js -->	
<!-- start-smoth-scrolling-->
<script type="text/javascript" src="${pageContext.request.contextPath}/user/js/move-top.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/user/js/easing.js"></script>	

<script defer src="${pageContext.request.contextPath}/user/js/jquery.flexslider.js"></script>
<script src="${pageContext.request.contextPath}/user/js/classie.js"></script>
<script src="${pageContext.request.contextPath}/user/js/uisearch.js"></script>
<script src="${pageContext.request.contextPath}/user/js/responsiveslides.min.js"></script>

<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});
		});
</script>
<!--//end-smoth-scrolling-->
<!--pop-up-->
<script src="${pageContext.request.contextPath}/user/js/menu_jquery.js"></script>



		<style>
				body {
				background-image: url(http://www.ayt999.com/);
				background-position: center;
				background-repeat: no-repeat;
				background-color: #ffffff;
				font-family: http: //www.ayt999.com/;}
		</style>
		<!--[if lte IE 9]>
<script src="http://www.ayt999.com/public/ui/v2/static/js/lteie9.js"></script>
<![endif]-->
	</head>
	<!--[if lte IE 8]>
<div class="text-xs-center m-b-0 bg-blue-grey-100 alert">
    <button type="button" class="close" aria-label="Close" data-dismiss="alert">
        <span aria-hidden="true">×</span>
    </button>
    你正在使用一个 <strong>过时</strong> 的浏览器。请 <a href=https://browsehappy.com/ target=_blank>升级您的浏览器</a>，以提高您的体验。</div>
<![endif]-->

	<body>

		<body class="met-navfixed     ny-banner ">
			<header class='met-head navbar-fixed-top' m-id='4' m-type='head_nav' met-imgmask>
				<nav class="navbar navbar-default box-shadow-none head_nav_met_16_4     ">
					<div class="container">
						<div class="row">
							<h3 hidden></h3>
							<!-- logo 
							<div class="navbar-header pull-xs-left">
								<a href="http://www.ayt999.com/" class="met-logo vertical-align block pull-xs-left p-y-5" title="">
									<div class="vertical-align-middle">
										<img src="http://www.ayt999.com/upload/201807/1532482808.png" alt="" class="logo">
										<img src="http://www.ayt999.com/upload/201807/1532483375.png" alt="" class="logo1  hidden">
									</div>
								</a>
							</div>
							<h1 hidden>公司简介</h1>
							<h2 hidden>公司介绍</h2>
							logo -->
							<button type="button" class="navbar-toggler hamburger hamburger-close collapsed p-x-5 head_nav_met_16_4-toggler" data-target="#head_nav_met_16_4-collapse" data-toggle="collapse">
                    <span class="sr-only"></span>
                    <span class="hamburger-bar"></span>
                </button>
							<!-- 会员注册登录 -->

							<!-- 会员注册登录 -->

							<!-- 导航 -->
						<%@ include file="header.jsp" %>
							<!-- 导航 -->
						</div>
					</div>
				</nav>
			</header>

			<div class="banner_met_16_2 page-bg" data-height='' style='' m-id='3' m-type='banner'>
				<div class="slick-slide">
					<img class="cover-image" src="http://www.ayt999.com/upload/201808/1533699549.jpg" srcset='http://www.ayt999.com/upload/201808/1533699549.jpg 767w,http://www.ayt999.com/upload/201808/1533699549.jpg' sizes="(max-width: 767px) 767px" alt="" data-height='0|0|0'>
				</div>
				<div class="slick-slide">
					<img class="cover-image" src="http://www.ayt999.com/upload/201805/1527575184.jpg" srcset='http://www.ayt999.com/upload/201805/1527575184.jpg 767w,http://www.ayt999.com/upload/201805/1527575184.jpg' sizes="(max-width: 767px) 767px" alt="" data-height='0|0|0'>
				</div>
			</div>

			<div class="subcolumn_nav_met_16_1      border-bottom1" m-id='12' m-type='nocontent'>
				<div class="">
					<div class="subcolumn-nav text-xs-center">
						<ul class="subcolumn_nav_met_16_1-ul m-b-0 p-y-10 p-x-0 ulstyle" style="background-image: url(images/背景.jpg);">
							<li>
							<h2><a href="about.html" title="公司简介" class='active'>DY团队介绍</a></h2>
							</li>
						</ul>
					</div>
				</div>
			</div>

			<main class="show_met_16_1 met-show-body panel panel-body m-b-0 
	    		bgcolor
	" boxmh-mh m-id='11' style="background-image: url(images/背景.jpg);">
				<div class="container" style="margin-top:30px;" >
					<!-- <section class="met-editor clearfix"> -->
						<div style="float:left;">
						<section class="met-editor clearfix">
						<!-- <h3>小组成员</h3> -->
					
						<p>组长：黄志鹏</p>
						<p>介绍介绍介绍介绍介绍介绍介绍介绍介绍介绍</p>
						<p>成员：蒋丽娟</p>
						<p>介绍介绍介绍介绍介绍介绍介绍介绍介绍介绍</p>
						<p>成员：薛佳鑫</p>
						<p>介绍介绍介绍介绍介绍介绍介绍介绍介绍介绍</p>
						<p>成员：陈超群</p>
						<p>介绍介绍介绍介绍介绍介绍介绍介绍介绍介绍</p>
						<p>成员：黄伟鸿</p>
						<p>介绍介绍介绍介绍介绍介绍介绍介绍介绍介绍</p>
						<p>成员：高文乾</p>
						<p>介绍介绍介绍介绍介绍介绍介绍介绍介绍介绍</p>
						</section>
						</div>
						<p style="float:right"><img src="${pageContext.request.contextPath}/user/images/dyteam.jpg"   title="DY.jpg"  alt="DY" width="600px"></p>
				<!-- 	</section> -->
			
					<div class="col-md-2 "></div>
					<!-- sidebar -->
			
				</div>
			</main>
			<!-- /sidebar -->

			<%@include file="foot.jsp" %>
			<input type="hidden" name="met_lazyloadbg" value="other/">
			<script src="http://www.ayt999.com/public/ui/v2/static/js/basic.js"></script>
			<script src="http://www.ayt999.com/templates/mui145/cache/show_cn.js?1553579261"></script>
			
			
				
		<!-- 登录的方法 及样式的链接 -->
	<link rel="stylesheet"
		href="${pageContext.request.contextPath}/user/layui/css/layui.css"
		media="all">
	<script src="${pageContext.request.contextPath}/user/layui/layui.js"
		charset="utf-8"></script>
						
		<script type="text/javascript">
			$(function(){
				$("#login").click(function(){
					$.post("${pageContext.request.contextPath}/ULogin.action?op=login",$("#loginForm").serialize(),function(data,status){
						if(data.msg=="用户登录成功"||data.msg=="医生登录成功"){
						layui.use(['laypage','layer' ],function() {
							var laypage = layui.laypage, layer = layui.layer;
							layer.msg(data.msg,{icon : 1,time : 2000	},
									function() {location.reload();
									});
							});
						}else {
							layui.use(['laypage','layer' ],function() {
								var laypage = layui.laypage, layer = layui.layer;
								layer.msg(data.msg,{icon : 2,time : 2000	});
								});
						}
					});
				});
			});
		</script>
			
			
			
							<!-- 退出登录的方法 -->
	<script type="text/javascript">
			$(function(){
				$("#exit").click(function(){
					$.get("${pageContext.request.contextPath}/ULogin.action","op=exit",function(data,status){
						location.reload();
					});
				});
			});
		</script>
		</body>

</html>