<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<div class="met-footnav text-center foot_nav_met_27_1" m-id="49" m-type='foot_nav'>
			<div class="container">
				<div class="row mob-masonry">
					<div class="col-lg-8 col-xs-12 footnav-left clearfix">
						<div class="col-md-2 col-sm-2 col-xs-6 list masonry-item">
							<h4><a href="${pageContext.request.contextPath}/user/Introduce.jsp" title="团队介绍" target='_self'>团队介绍</a></h4>
						</div>
						<div class="col-md-2 col-sm-2 col-xs-6 list masonry-item">
							<h4><a href="${pageContext.request.contextPath}/UService.action" title="特色疗法" target='_self'>特色疗法</a></h4>
						</div>
						<div class="col-md-2 col-sm-2 col-xs-6 list masonry-item">
							<h4><a href="${pageContext.request.contextPath}/user/doctorTeam.jsp" title="专家团队" target='_self'>专家团队</a></h4>
						</div>
						<div class="col-md-2 col-sm-2 col-xs-6 list masonry-item">
							<h4><a href="${pageContext.request.contextPath}/ArticleServlet.action?op=allArticle" title="养生知识" target='_self'>养生知识</a></h4>
						</div>
						
					</div>
					<div class="col-lg-4 col-xs-12 footnav-right clearfix">
						<div class="info masonry-item">
							<div class="info-right">
								<!--微信-->
								
								<!--微博-->
								<!--email-->
								<!--电话-->
								<!--QQ-->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<button type="button" class="btn btn-icon btn-primary btn-squared back_top_met_16_1 met-scroll-top" hidden m-id='8' m-type='nocontent'>
	<i class="icon wb-chevron-up" aria-hidden="true"></i>
</button>