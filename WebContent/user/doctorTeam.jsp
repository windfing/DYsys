<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
     <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>   
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>专家团队</title>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=0,minimal-ui">
		<meta name="format-detection" content="telephone=no" />
		<meta name="description" content="MetInfo企业建站系统专注于为中小企业提供高质量的建站服务，海量模板请登录www.metinfo.cn，本站为中医养生馆行业响应式网站模板演示站" />
		<meta name="keywords" content="中医养生馆行业网站模版,中医养生馆行业网页模版,响应式模版,网站制作,网站建站" />
		<meta name="generator" content="MetInfo 6.0.0" data-variable="other/|cn|3|0|3|mui145" />
		<link href="other/favicon.ico" rel="shortcut icon" type="image/x-icon" />
		<link rel='stylesheet' type='text/css' href='${pageContext.request.contextPath}/user/css/basic.css'>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/user/css/product_cn.css?1553566664" />
		
		<link href="${pageContext.request.contextPath}/user/css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
<link href="${pageContext.request.contextPath}/user/css/style.css" type="text/css" rel="stylesheet" media="all">
<link rel="stylesheet" href="${pageContext.request.contextPath}/user/css/flexslider.css" type="text/css" media="screen" />
<!-- js -->
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
<!-- //js -->	
<!-- start-smoth-scrolling-->
<script type="text/javascript" src="${pageContext.request.contextPath}/user/js/move-top.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/user/js/easing.js"></script>	

<script defer src="${pageContext.request.contextPath}/user/js/jquery.flexslider.js"></script>
<script src="${pageContext.request.contextPath}/user/js/classie.js"></script>
<script src="${pageContext.request.contextPath}/user/js/uisearch.js"></script>
<script src="${pageContext.request.contextPath}/user/js/responsiveslides.min.js"></script>


<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});
		});
</script>
<!--//end-smoth-scrolling-->
<!--pop-up-->
<script src="${pageContext.request.contextPath}/user/js/menu_jquery.js"></script>
		
		
		<style>
			body {
				background-image: url(other/);
				background-position: center;
				background-repeat: no-repeat;
				background-color: #ffffff;
				font-family: other/;
			}
			 .docs{
			margin-top:40px;
			} 
		
		</style>
		
	</head>
	

		<body class="met-navfixed     ny-banner ">
			<header class='met-head navbar-fixed-top' m-id='4' m-type='head_nav' met-imgmask>
				<nav class="navbar navbar-default box-shadow-none head_nav_met_16_4     ">
					<div class="container">
						<div class="row">
							<h3 hidden></h3>
							<button type="button" class="navbar-toggler hamburger hamburger-close collapsed p-x-5 head_nav_met_16_4-toggler" data-target="#head_nav_met_16_4-collapse" data-toggle="collapse">
                    <span class="sr-only"></span>
                    <span class="hamburger-bar"></span>
                </button>
							<!-- 会员注册登录 -->

							<!-- 会员注册登录 -->
							<!-- 导航 -->
							<%@include file="header.jsp" %>
							<!-- 导航 -->
						</div>
					</div>
				</nav>
			</header>
			<div class="banner_met_16_2 page-bg" data-height='' style='' m-id='3' m-type='banner'>
				<div class="slick-slide">
					<img class="cover-image" src="images/1533699549.jpg" srcset='images/1533699549.jpg 767w,images/1533699549.jpg' sizes="(max-width: 767px) 767px" alt="" data-height='0|0|0'>
				</div>
				<div class="slick-slide">
					<img class="cover-image" src="images/1527575184.jpg" srcset='images/1527575184.jpg 767w,images/1527575184.jpg' sizes="(max-width: 767px) 767px" alt="" data-height='0|0|0'>
				</div>
			</div>
			
			
			<div class="product_list_page_met_21_6 met-product animsition 
	    	    	    	    " m-id='55' style="background-image: url(images/背景.jpg);">
				<div class="    container">
				<!--动态添加li  -->
					<ul class="blocks-xs-2	blocks-md-2 blocks-lg-4 blocks-xxl-4  docs"  id="met-grid" data-scale='400x400' style="background-image: url(images/背景.jpg);">
					</ul>
				</div>
			</div>
		
 <%-- <%@include file="foot.jsp" %>
			<input type="hidden" name="met_lazyloadbg" value="other/">
			<script src="${pageContext.request.contextPath}/user/js/basic.js"></script>
			<script src="${pageContext.request.contextPath}/user/js/product_cn.js?1553566664"></script> 
			 --%>
				
		<!-- 登录的方法 及样式的链接 -->
	<link rel="stylesheet"
		href="${pageContext.request.contextPath}/user/layui/css/layui.css"
		media="all">
	<script src="${pageContext.request.contextPath}/user/layui/layui.js"
		charset="utf-8"></script>
						
		<script type="text/javascript">
			$(function(){
				$("#login").click(function(){
					$.post("${pageContext.request.contextPath}/ULogin.action?op=login",$("#loginForm").serialize(),function(data,status){
						if(data.msg=="用户登录成功"||data.msg=="医生登录成功"){
						layui.use(['laypage','layer' ],function() {
							var laypage = layui.laypage, layer = layui.layer;
							layer.msg(data.msg,{icon : 1,time : 2000	},
									function() {location.reload();
									});
							});
						}else {
							layui.use(['laypage','layer' ],function() {
								var laypage = layui.laypage, layer = layui.layer;
								layer.msg(data.msg,{icon : 2,time : 2000	});
								});
						}
					});
				});
				

				
				
				
			})

					
		</script>
			
			
			
			
							<!-- 退出登录的方法 -->
	<script type="text/javascript">
			$(function(){
				<!-- 显示医生 -->
				
				$.ajax({
					type:"get",
					url:"../UDoctor.action",
					data:{
						op : "queryDocTeam"
					},
					success:function(data) {
						
						$.each(data,function(index,obj){
							for(var i=0;i<obj.length;i++){
								$(".docs").append('<li class="gx-li" style="background-image: url(images/背景.jpg);">'+
							'<div class="col-md-3 gx-li-img"><img width="260px" height="260px" alt="'+obj[i].dName+'"  src=${pageContext.request.contextPath}'+obj[i].dUrl+'></div>'+
							'<div class="col-md-9 gx-li-txt"><h3>'+obj[i].dName+'</h3>'+
								'<section class="met-editor clearfix">'+
									'<p style="line-height: 3em;"><span style="font-size: 20px;">'+ obj[i].dDesc+'</span></p>'+
								'</section></div></li>' );
								
							}
							
						});
						
					}
					
				});
				
				
				
				$("#exit").click(function(){
					$.get("${pageContext.request.contextPath}/ULogin.action","op=exit",function(data,status){
						location.reload();
					});
				});
			});
		</script>
		</body>

</html>