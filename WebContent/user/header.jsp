<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
   	 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- 导航 -->
						<div class="collapse navbar-collapse navbar-collapse-toolbar pull-md-right p-0" id="head_nav_met_16_4-collapse">
							<ul class="nav navbar-nav navlist">
								<li class='nav-item'>
									<a href="${pageContext.request.contextPath}/user/index.jsp" title="网站首页" class="nav-link">网站首页</a>
								</li>
								<li class="nav-item dropdown m-l-0">
									<a href="${pageContext.request.contextPath}/user/Introduce.jsp" target='_self' title="公司介绍" class="nav-link">
										团队介绍</a>
								</li>
								<li class='nav-item m-l-0'>
									<a href="${pageContext.request.contextPath}/UService.action" target='_self' title="特色疗法" class="nav-link ">特色疗法</a>
								</li>
								<li class="nav-item dropdown m-l-0">
									<a href="${pageContext.request.contextPath}/user/doctorTeam.jsp" target='_self' title="doctorTeam.jsp" class="nav-link" >
										专家团队</a>

								</li>
								<li class="nav-item dropdown m-l-0">
									<a href="${pageContext.request.contextPath}/ArticleServlet.action?op=allArticle" target='_self' title="养生知识" class="nav-link  " >
										养生知识</a>
								</li>
								
			
								<c:if test="${not empty users.uName && !(users.uName eq null)}">
								<li class="nav-item dropdown m-l-0">
									<a target='_self'  class="nav-link">您好：
										${users.uName}</a>
								</li>
								<li class="nav-item dropdown m-l-0">
									<a href="${pageContext.request.contextPath}/UUsers.action?op=personal&uId=${users.uId}" target='_self' title="个人中心" class="nav-link dropdown-toggle " data-toggle="dropdown" data-hover="dropdown">
										个人中心</a>
									<div class="dropdown-menu dropdown-menu-right dropdown-menu-bullet two-menu">



										<a href="${pageContext.request.contextPath}/UUsers.action?op=modify&uId=${users.uId}" target='_self' title="修改资料" class='dropdown-item border-top1 hassub '>修改资料</a>
										<a href="${pageContext.request.contextPath}/user/userAppointment.jsp" target='_self' title="预约查询" class='dropdown-item border-top1 hassub '>预约查询</a>
                                        <a href="${pageContext.request.contextPath}/ArticleServlet.action?op=personArticle&uId=${users.uId}&dId=${doctor.dId}" target='_self' title="个人文章" class='dropdown-item border-top1 hassub '>个人文章</a>


									</div>
								</li>
								<li class="nav-item dropdown m-l-0">
									<a  href="#" target='_self'  class="nav-link" id="exit">退出登录</a>
								</li>
								</c:if>
								
								
								<c:if test="${not empty doctor && !(doctor eq null)}">
								<li class="nav-item dropdown m-l-0">
									<a target='_self'  class="nav-link">欢迎您：
										${doctor.dName}  医生</a>
								</li>
								<li class="nav-item dropdown m-l-0">
									<a href="${pageContext.request.contextPath}/UDoctor.action?op=personal&dId=${doctor.dId}" target='_self' title="个人中心" class="nav-link dropdown-toggle " data-toggle="dropdown" data-hover="dropdown">
										个人中心</a>
									<div class="dropdown-menu dropdown-menu-right dropdown-menu-bullet two-menu">
										<a href="${pageContext.request.contextPath}/UDoctor.action?op=modify&dId=${doctor.dId}" target='_self' title="修改资料" class='dropdown-item border-top1 hassub '>修改资料</a>
										<a href="${pageContext.request.contextPath}/user/doctorAppointment.jsp" target='_self' title="预约查询" class='dropdown-item border-top1 hassub '>预约查询</a>
										<a href="${pageContext.request.contextPath}/ArticleServlet.action?op=personArticle&dId=${doctor.dId}&uId=${uers.uId}" target='_self' title="个人文章" class='dropdown-item border-top1 hassub '>个人文章</a>
									</div>
								</li>
								<li class="nav-item dropdown m-l-0">
									<a  href="#" target='_self'  class="nav-link" id="exit">退出登录</a>
								</li>
								</c:if>
								
							
								<c:if test="${empty users && empty doctor}">
						<li class="nav-item dropdown m-l-0 login">
									<a href="#" id="loginButton" class="nav-link "><span>登录</span></a>
									<div id="loginBox">
										<form id="loginForm" >
											<fieldset id="body">
												<fieldset>
													<label for="email">请输入账户名或手机号</label>
													<input type="text" name="account" >
												</fieldset>
												<fieldset>
													<label for="password">请输入密码</label>
													<input type="password" name="Password" >
												</fieldset>
												<fieldset>
													<input type="button" id="login" value="登录">
												</fieldset>
												<fieldset>
													<input type="button" id="regist" value="注册" />
												</fieldset>
											</fieldset>
										</form>
									</div>
								</li>
								</c:if>
			</ul>
		</div>
		
		<script type="text/javascript">
			$(function(){
				$("#regist").click(function(){
				//跳转到注册页面
				location.href="${pageContext.request.contextPath}/user/regist.jsp";
				});
			});
		</script>	
		
		
	