<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<title>服务项目</title>
		<meta name="renderer" content="webkit">
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=0,minimal-ui">
		<meta name="format-detection" content="telephone=no" />
		<meta name="description" content="MetInfo企业建站系统专注于为中小企业提供高质量的建站服务，海量模板请登录www.metinfo.cn，本站为中医养生馆行业响应式网站模板演示站" />
		<meta name="keywords" content="中医养生馆行业网站模版,中医养生馆行业网页模版,响应式模版,网站制作,网站建站" />
		<meta name="generator" content="MetInfo 6.0.0" data-variable="other/|cn|3|0|3|mui145" />
		<link rel='stylesheet' type='text/css' href='${pageContext.request.contextPath}/user/css/basic.css'>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/user/css/product_cn.css?1553566664" />
		
		<link href="${pageContext.request.contextPath}/user/css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
<link href="${pageContext.request.contextPath}/user/css/style.css" type="text/css" rel="stylesheet" media="all">
<link rel="stylesheet" href="${pageContext.request.contextPath}/user/css/flexslider.css" type="text/css" media="screen" />
<!-- js -->
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
<!-- //js -->	
<!-- start-smoth-scrolling-->
<script type="text/javascript" src="${pageContext.request.contextPath}/user/js/move-top.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/user/js/easing.js"></script>	

<script defer src="${pageContext.request.contextPath}/user/js/jquery.flexslider.js"></script>
<script src="${pageContext.request.contextPath}/user/js/classie.js"></script>
<script src="${pageContext.request.contextPath}/user/js/uisearch.js"></script>
<script src="${pageContext.request.contextPath}/user/js/responsiveslides.min.js"></script>

<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});
		});
</script>
<!--//end-smoth-scrolling-->
<!--pop-up-->
<script src="${pageContext.request.contextPath}/user/js/menu_jquery.js"></script>
		
		
		<style>
			body {
				background-image: url(other/);
				background-position: center;
				background-repeat: no-repeat;
				background-color: #ffffff;
				font-family: other/;
			}
		</style>
		<!--[if lte IE 9]>
<script src="other/public/ui/v2/static/js/lteie9.js"></script>
<![endif]-->

<style type="text/css">
p{
	font-size: 20px;

}
</style>
	</head>
	<!--[if lte IE 8]>
<div class="text-xs-center m-b-0 bg-blue-grey-100 alert">
    <button type="button" class="close" aria-label="Close" data-dismiss="alert">
        <span aria-hidden="true">×</span>
    </button>
    你正在使用一个 <strong>过时</strong> 的浏览器。请 <a href=https://browsehappy.com/ target=_blank>升级您的浏览器</a>，以提高您的体验。</div>
<![endif]-->

	

		<body class="met-navfixed     ny-banner ">
			<header class='met-head navbar-fixed-top' m-id='4' m-type='head_nav' met-imgmask>
				<nav class="navbar navbar-default box-shadow-none head_nav_met_16_4     ">
					<div class="container">
						<div class="row">
							<h3 hidden></h3>
							
							<button type="button" class="navbar-toggler hamburger hamburger-close collapsed p-x-5 head_nav_met_16_4-toggler" data-target="#head_nav_met_16_4-collapse" data-toggle="collapse">
                    <span class="sr-only"></span>
                    <span class="hamburger-bar"></span>
                </button>
							<!-- 会员注册登录 -->

							<!-- 会员注册登录 -->
							<!-- 导航 -->
							<%@include file="header.jsp" %>
							<!-- 导航 -->
						</div>
					</div>
				</nav>
			</header>
			<div class="banner_met_16_2 page-bg" data-height='' style='' m-id='3' m-type='banner' >
				<div class="slick-slide" >
					<img class="cover-image" src="${pageContext.request.contextPath}/user/images/1533699549.jpg" srcset='${pageContext.request.contextPath}/user/images/1533699549.jpg 767w,${pageContext.request.contextPath}/user/images/1533699549.jpg' sizes="(max-width: 767px) 767px" alt="" data-height='0|0|0'>
				</div>
				<div class="slick-slide">
					<img class="cover-image" src="${pageContext.request.contextPath}/user/images/1527575184.jpg" srcset='${pageContext.request.contextPath}/user/images/1527575184.jpg 767w,${pageContext.request.contextPath}/user/images/1527575184.jpg' sizes="(max-width: 767px) 767px" alt="" data-height='0|0|0'>
				</div>
			</div>
			<div class="product_list_page_met_21_6 met-product animsition 
	    	    	    	    " m-id='55' style="background-image: url(images/背景.jpg);">
				<div class="    container">
					<ul class="blocks-xs-2 blocks-md-2 blocks-lg-4 blocks-xxl-4  met-pager-ajax imagesize cover met-product-list met-grid" id="met-grid" data-scale='400x400' >
						
						<c:forEach items="${list}" var="list">
							<li class="gx-li" style="background-image: url(images/背景.jpg);">
								<div class="col-md-3 gx-li-img">
									<img src="${pageContext.request.contextPath}${list.sUrl}" alt="${list.sTitle}">
								</div>
								<div class="col-md-9 gx-li-txt">
									<h3>${list.sTitle}</h3>
									<p>${list.sContent}</p>
									<br/>
									<br/>
									<!-- <input type="button" class="btn btn-danger btnAdd" value="预约服务"> -->
									<c:if test="${not empty users.uName && !(users.uName eq null)}">
									<a class="more-btn" href="${pageContext.request.contextPath}/UAppointment.action?op=addAppointment&sId=${list.sId}&uId=${users.uId}&uName=${users.uName}" title="${list.sTitle}" target=_self>预约服务</a>
									</c:if>
								</div>
						</li>
						
						</c:forEach>
						
					</ul>
					<%-- <div class='m-t-20 text-xs-center hidden-sm-down' m-type="nosysdata">
						<div class='met_pager'><span class='PreSpan'>上一页</span>
							<a href='${pageContext.request.contextPath}/user/service.jsp?class1=3' class='Ahover'>1</a>
							<a href='${pageContext.request.contextPath}/user/service02.jsp?class1=3&page=2'>2</a>
							<a href='other/product/index.php?class1=3&page=2' class='NextA'>下一页</a>
							<span class='PageText'>转至第</span>
							<input type='text' id='metPageT' data-pageurl='index.php?lang=cn&class1=3&page=||2' value='1' />
							<input type='button' id='metPageB' value='页' />
						</div>
					</div> --%>
					<div class="met_pager met-pager-ajax-link hidden-md-up" data-plugin="appear" data-animate="slide-bottom" data-repeat="false" m-type="nosysdata">
						<button type="button" class="btn btn-primary btn-block btn-squared ladda-button" id="met-pager-btn" data-plugin="ladda" data-style="slide-left" data-url="" data-page="1">
				        <i class="icon wb-chevron-down m-r-5" aria-hidden="true"></i>
				        				    </button>
					</div>
				</div>
			</div>
			<!--                 </div>
        </div>
    </main>
 -->
	<%@include file="foot.jsp" %>
	<script src="${pageContext.request.contextPath}/user/js/basic.js"></script>
	<script src="${pageContext.request.contextPath}/user/js/product_cn.js?1553566664"></script>
			
		<!-- 登录的方法 及样式的链接 -->
	<link rel="stylesheet"
		href="${pageContext.request.contextPath}/user/layui/css/layui.css"
		media="all">
	<script src="${pageContext.request.contextPath}/user/layui/layui.js"
		charset="utf-8"></script>
						
		<script type="text/javascript">
			$(function(){
				$("#login").click(function(){
					$.post("${pageContext.request.contextPath}/ULogin.action?op=login",$("#loginForm").serialize(),function(data,status){
						if(data.msg=="用户登录成功"||data.msg=="医生登录成功"){
						layui.use(['laypage','layer' ],function() {
							var laypage = layui.laypage, layer = layui.layer;
							layer.msg(data.msg,{icon : 1,time : 2000	},
									function() {location.reload();
									});
							});
						}else {
							layui.use(['laypage','layer' ],function() {
								var laypage = layui.laypage, layer = layui.layer;
								layer.msg(data.msg,{icon : 2,time : 2000	});
								});
						}
					});
				});
			});
		</script>
		
		
			
			
							<!-- 退出登录的方法 -->
		<script type="text/javascript">
			$(function(){
				$("#exit").click(function(){
					$.get("${pageContext.request.contextPath}/ULogin.action","op=exit",function(data,status){
						location.reload();
					});
				});
			});
		</script>
		
		<!-- <script type="text/javascript">
			$(function(){
				$(".btnAdd").click(function(){
					location.href="${pageContext.request.contextPath}/UAppointment.action?"
				});
			});
		
		
		</script> -->
		
		
		</body>
</html>