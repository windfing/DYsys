<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%> 
     <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html> 
<head> 
 <style type="text/css">
 .dian{
overflow:hidden; 
 	text-overflow:ellipsis; 
 	display: -webkit-box;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 3;
    height:65px;
}
.table td, .table th {
     padding: 0px; }
  .table td, .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
     padding: 0px; 
}
.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
     padding: 0px; 

}
 </style>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>养生知识</title>

<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=0,minimal-ui">
		<meta name="format-detection" content="telephone=no" />
		<meta name="description" content="MetInfo企业建站系统专注于为中小企业提供高质量的建站服务，海量模板请登录www.metinfo.cn，本站为中医养生馆行业响应式网站模板演示站" />
		<meta name="keywords" content="中医养生馆行业网站模版,中医养生馆行业网页模版,响应式模版,网站制作,网站建站" />
		<meta name="generator" content="MetInfo 6.0.0" data-variable="other/|cn|18|0|2|mui145" />
		<link href="other/favicon.ico" rel="shortcut icon" type="image/x-icon" />
		<meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel='stylesheet' type='text/css' href='${pageContext.request.contextPath}/user/css/basic.css'>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/user/css/news_cn.css?1553567240" />
<!-- 	增加了leyui的样式 -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/user/layui/css/layui.css"  media="all">
	<link href="${pageContext.request.contextPath}/user/css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
<link href="${pageContext.request.contextPath}/user/css/style.css" type="text/css" rel="stylesheet" media="all">
<link rel="stylesheet" href="${pageContext.request.contextPath}/user/css/flexslider.css" type="text/css" media="screen" />
<!-- js -->
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
<!-- //js -->	
<!-- start-smoth-scrolling-->
<script type="text/javascript" src="${pageContext.request.contextPath}/user/js/move-top.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/user/js/easing.js"></script>	

<script defer src="${pageContext.request.contextPath}/user/js/jquery.flexslider.js"></script>
<script src="${pageContext.request.contextPath}/user/js/classie.js"></script>
<script src="${pageContext.request.contextPath}/user/js/uisearch.js"></script>
<script src="${pageContext.request.contextPath}/user/js/responsiveslides.min.js"></script>
<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});
		});
</script>
<!--//end-smoth-scrolling-->
<!--pop-up-->
<script src="${pageContext.request.contextPath}/user/js/menu_jquery.js"></script>
	
	
	<style>
			body {
				background-image: url(other/);
				background-position: center;
				background-repeat: no-repeat;
				background-color: #ffffff;
				font-family: other/;
			}
		</style>
	<body>
		<body class="met-navfixed     ny-banner ">
			<header class='met-head navbar-fixed-top' m-id='4' m-type='head_nav' met-imgmask>
				<nav class="navbar navbar-default box-shadow-none head_nav_met_16_4     ">
					<div class="container">
						<div class="row">
							<h3 hidden></h3>
							<button type="button" class="navbar-toggler hamburger hamburger-close collapsed p-x-5 head_nav_met_16_4-toggler" data-target="#head_nav_met_16_4-collapse" data-toggle="collapse">
                    <span class="sr-only"></span>
                    <span class="hamburger-bar"></span>
                </button>
							<!-- 会员注册登录 -->

							<!-- 会员注册登录 -->
							<!-- 导航 -->
					<%@include file="header.jsp" %>
							<!-- 导航 -->
						</div>
					</div>
				</nav>
			</header>
			<div class="banner_met_16_2 page-bg" data-height='' style='' m-id='3' m-type='banner'>
		<div class="slick-slide">
					<img class="cover-image" src="${pageContext.request.contextPath}/user/images/1533699549.jpg" srcset='${pageContext.request.contextPath}/user/images/1533699549.jpg 767w,${pageContext.request.contextPath}/user/images/1533699549.jpg' sizes="(max-width: 767px) 767px" alt="" data-height='0|0|0'>
				</div>
				<div class="slick-slide">
					<img class="cover-image" src="${pageContext.request.contextPath}/user/images/1527575184.jpg" srcset='${pageContext.request.contextPath}/user/images/1527575184.jpg 767w,${pageContext.request.contextPath}/user/images/1527575184.jpg' sizes="(max-width: 767px) 767px" alt="" data-height='0|0|0'>
				</div>
			</div>

			<main class="news_list_page_met_16_1 met-news" style="background-image: url(${pageContext.request.contextPath}/user/images/背景.jpg);">
				<div class="container">
					<div class="row">
						<div class="col-md-9 met-news-body">
							<div class="row">
								<div class="met-news-list" style="background-image: url(${pageContext.request.contextPath}/user/images/背景.jpg);">
						<form class="layui-form">
				 <div class="layui-form-item">
    <div class="layui-input-block">
      <input type="text" id="title" lay-verify="title" autocomplete="off" placeholder="请输入标题" class="layui-input">
    </div>
  </div>
   <div class="layui-form-item layui-form-text">
   
    <div class="layui-input-block">
      <textarea placeholder="请输入内容" class="layui-textarea"  id="articleArea" lay-verify="articleArea"></textarea>
    </div>
  </div>
    <div class="layui-form-item">
    <div class="layui-input-block">
      <input type="button" class="layui-btn" id="submit1" value="发表">
      <button type="reset" class="layui-btn layui-btn-primary">重置</button>
    </div>
  </div>
</form>
								</div>
							</div>
						</div>
						
					<div class="col-md-12 met-news-body">
							<div class="row">
								<div class="met-news-list" style="background-image: url(${pageContext.request.contextPath}/user/images/背景.jpg);">
									<table class="table table-striped">
									<tbody id="articleTable">
									<c:forEach items="${pd.data}" var="e">
									<tr>
									<td >
									<ul class="ulstyle met-pager-ajax imagesize" data-scale='300x400' m-id='13' style="background-image: url(${pageContext.request.contextPath}/user/images/背景.jpg);">
										<li class='border-bottom1'>
											<h4>
			<a href="UKnowledgedisServlet.action?op=querykd&kId=${e.kId}" title="" target=_self>${e.kTitle}</a>
		</h4>
											<p class="des font-weight-300 dian">${e.kContent}</p>
											<p class="info font-weight-300">
												<span>${e.kTime}</span>
												<span></span>
												
												<a href="#" class="delete">删除<span style="display:none" >${e.kId}</span></a>
											</p>
										</li>
									</ul>
									</td>
								</tr>
								
							</c:forEach>
								
								<tbody>
								</table>
								<div id="demo"></div>
								</div>
							</div>
						</div>	
						
						
					</div>
				</div>
			</main>
			<%@include file="foot.jsp" %>
			<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
			<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
			<script src="${pageContext.request.contextPath}/user/layui/layui.js" charset="utf-8"></script>
							<!-- 退出登录的方法 -->
	<script>
	layui.use(['laypage', 'layer'], function(){
		  var laypage = layui.laypage
		  ,layer = layui.layer;
		  laypage.render({
			    elem: 'demo'//div块元素
			    ,theme: '#ba2525'//颜色
			    ,page:true
			    ,count : ${pd!=null?pd.total:0}//总页数
		        ,curr:${pd!=null?pd.page:1}//当前页
		        ,limit:${pd!=null?pd.pageSize:4},//当前显示的条数
		        layout : [ 'count', 'prev', 'page', 'next'],//分页显示的信息
		        jump : function(obj,first) {
					if(!first)
					{
location.href="ArticleServlet.action?op=personArticle&page="+obj.curr+"&pageSize="+obj.limit+"&dId=${doctor.dId}&uId=${users.uId}";


					}
				}
			  });
	});
				
		</script>
		
			<script type="text/javascript">
			
		
			$(function(){	
				$("#submit1").click(function(){
					$.ajax({
							type:"get",
							url:"ArticleServlet.action",
							data:{
								op:"addArticle",
								uId:"${users.uId}",
								dId:"${doctor.dId}",
								kTitle:$("#title").val(),
								kContent:$("#articleArea").val()
							},
							success:function(data){
								if(data=="发表成功"){
									$("#title").html("");	
									$("#articleArea").html("");
								window.location.reload();
									layer.msg(data);
								}else{
									layer.msg(data);
								}
							
							}							
						});
				});
				
				$("#exit").click(function(){
					$.get("${pageContext.request.contextPath}/ULogin.action","op=exit",function(data,status){
						location.href="${pageContext.request.contextPath}/user/index.jsp";
					});
				});
			
				$("[href='#']").click(function(){
					$.ajax({
							type:"get",
							url:"ArticleServlet.action",
							data:{
								op:"deleteArticle",
								kId:$(this).children().html(),
							},
							success:function(data){
								if(data=="删除成功"){
									layer.msg(data);
								location.reload();	
								}else{
									layer.msg(data);
								}
							
							}							
						});
				});
			});
			</script>
		</body>

</html>