<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
     <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>公司环境-</title>
		<meta name="renderer" content="webkit">
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=0,minimal-ui">
		<meta name="format-detection" content="telephone=no" />
		<meta name="description" content="MetInfo企业建站系统专注于为中小企业提供高质量的建站服务，海量模板请登录www.metinfo.cn，本站为中医养生馆行业响应式网站模板演示站" />
		<meta name="keywords" content="中医养生馆行业网站模版,中医养生馆行业网页模版,响应式模版,网站制作,网站建站" />
		<meta name="generator" content="MetInfo 6.0.0" data-variable="other/|cn|13|13|1|mui145" />
		<link href="other/favicon.ico" rel="shortcut icon" type="image/x-icon" />
		<link rel='stylesheet' type='text/css' href='${pageContext.request.contextPath}/user/css/basic.css'>
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/user/css/show_cn.css?1553648762" />
		<link href="${pageContext.request.contextPath}/user/css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
		<link href="${pageContext.request.contextPath}/user/css/style.css" type="text/css" rel="stylesheet" media="all">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/user/css/flexslider.css" type="text/css" media="screen" />
<!-- js -->
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
<!-- //js -->	
<!-- start-smoth-scrolling-->
<script type="text/javascript" src="${pageContext.request.contextPath}/user/js/move-top.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/user/js/easing.js"></script>	

<script defer src="${pageContext.request.contextPath}/user/js/jquery.flexslider.js"></script>
<script src="${pageContext.request.contextPath}/user/js/classie.js"></script>
<script src="${pageContext.request.contextPath}/user/js/uisearch.js"></script>
<script src="${pageContext.request.contextPath}/user/js/responsiveslides.min.js"></script>

<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});
		});
</script>
<!--//end-smoth-scrolling-->
<!--pop-up-->
<script src="${pageContext.request.contextPath}/user/js/menu_jquery.js"></script>
		
		
		<style>
			body {
				background-image: url(other/);
				background-position: center;
				background-repeat: no-repeat;
				background-color: #ffffff;
				font-family: other/;
			}
		</style>
</head>
<body>
				<body class="met-navfixed     ny-banner ">
			<header class='met-head navbar-fixed-top' m-id='4' m-type='head_nav' met-imgmask>
				<nav class="navbar navbar-default box-shadow-none head_nav_met_16_4     ">
					<div class="container">
						<div class="row">
							<h3 hidden></h3>
							<!-- logo 
							<div class="navbar-header pull-xs-left">
								<a href="http://www.ayt999.com/" class="met-logo vertical-align block pull-xs-left p-y-5" title="">
									<div class="vertical-align-middle">
										<img src="http://www.ayt999.com/upload/201807/1532482808.png" alt="" class="logo">
										<img src="http://www.ayt999.com/upload/201807/1532483375.png" alt="" class="logo1  hidden">
									</div>
								</a>
							</div>
							<h1 hidden>公司简介</h1>
							<h2 hidden>公司介绍</h2>
							logo -->
							<button type="button" class="navbar-toggler hamburger hamburger-close collapsed p-x-5 head_nav_met_16_4-toggler" data-target="#head_nav_met_16_4-collapse" data-toggle="collapse">
                    <span class="sr-only"></span>
                    <span class="hamburger-bar"></span>
                </button>
							<!-- 会员注册登录 -->

							<!-- 会员注册登录 -->

							<!-- 导航 -->
						<%@ include file="header.jsp" %>
							<!-- 导航 -->
						</div>
					</div>
				</nav>
			</header>
			<div class="banner_met_16_2 page-bg" data-height='' style='' m-id='3' m-type='banner'>
				<div class="slick-slide">
					<img class="cover-image" src="images/1533699549.jpg" srcset='images/1533699549.jpg 767w,images/1533699549.jpg' sizes="(max-width: 767px) 767px" alt="" data-height='0|0|0'>
				</div>
				<div class="slick-slide">
					<img class="cover-image" src="images/1527575184.jpg" srcset='images/1527575184.jpg 767w,images/1527575184.jpg' sizes="(max-width: 767px) 767px" alt="" data-height='0|0|0'>
				</div>
			</div>
			<div class="subcolumn_nav_met_16_1      border-bottom1" m-id='12' m-type='nocontent' style="background-image: url(images/背景.jpg);">
				<div class="">
					<div class="subcolumn-nav text-xs-center">
						<ul class="subcolumn_nav_met_16_1-ul m-b-0 p-y-10 p-x-0 ulstyle">
							<li>
								<a href="about.html" title="公司简介" class=''>公司简介</a>
							</li>
							<li>
								<a href="about(1).html" title="公司环境" class='active'>公司环境</a>
							</li>
						</ul>
					</div>
				</div>
			</div>

			<main class="show_met_16_1 met-show-body panel panel-body m-b-0 
	    		bgcolor
	" boxmh-mh m-id='11' style="background-image: url(images/背景.jpg);">
				<div class="container" >

					<!-- sidebar -->
					<!-- /sidebar -->
					<section class="met-editor clearfix">
						<p><img src="images/1533102704279571.jpg" data-width="354" width="354" data-height="236" height="236" title="1533102704279571.jpg" style="float: left;" / alt="图片关键词"></p>
						<p style="text-align:center"><img src="images/1533102704770203.jpg" data-width="354" width="354" data-height="236" height="236" title="1533102704770203.jpg" style="float: right;" / alt="图片关键词"><img src="images/1533102705246187.jpg" data-width="354" width="354" data-height="236" height="236" title="1533102705246187.jpg" / alt="图片关键词"><br/></p>
						<p><img src="images/1533102704976243.jpg" data-width="354" width="354" data-height="236" height="236" title="1533102704976243.jpg" style="float: left;" / alt="图片关键词"></p>
						<p><img src="images/1533102705205954.jpg" data-width="354" width="354" data-height="236" height="236" style="float: right;" title="1533102705205954.jpg" / alt="图片关键词"></p>
						<p style="text-align:center"><img src="images/1533103152612575.jpg" data-width="354" width="354" data-height="236" height="236" title="中医养生馆行业网站模版,中医养生馆行业网页模版,响应式模版,网站制作,网站建站" alt="中医养生馆行业网站模版,中医养生馆行业网页模版,响应式模版,网站制作,网站建站" /></p>
						<p><img src="images/1533102705245843.jpg" data-width="354" width="354" data-height="236" height="236" title="1533102705245843.jpg" style="float: left;" / alt="图片关键词"><img src="images/1533103226837401.jpg" data-width="354" width="354" data-height="236" height="236" title="中医养生馆行业网站模版,中医养生馆行业网页模版,响应式模版,网站制作,网站建站" alt="中医养生馆行业网站模版,中医养生馆行业网页模版,响应式模版,网站制作,网站建站" style="float: right;" /></p>
						<p style="text-align:center"><img src="images/1533102705258456.jpg" data-width="354" width="354" data-height="236" height="236" title="1533102705258456.jpg" / alt="图片关键词"></p>
					</section>
					<!-- sidebar -->
				</div>
				</div>
			</main>
			<%@include file="foot.jsp" %>
			<input type="hidden" name="met_lazyloadbg" value="other/">
			<script src="${pageContext.request.contextPath}/user/js/basic.js"></script>
			<script src="${pageContext.request.contextPath}/user/js/show_cn.js?1553648762"></script>
				
		<!-- 登录的方法 及样式的链接 -->
	<link rel="stylesheet"
		href="${pageContext.request.contextPath}/user/layui/css/layui.css"
		media="all">
	<script src="${pageContext.request.contextPath}/user/layui/layui.js"
		charset="utf-8"></script>
						
		<script type="text/javascript">
			$(function(){
				$("#login").click(function(){
					$.post("${pageContext.request.contextPath}/ULogin.action?op=login",$("#loginForm").serialize(),function(data,status){
						if(data.msg=="用户登录成功"||data.msg=="医生登录成功"){
						layui.use(['laypage','layer' ],function() {
							var laypage = layui.laypage, layer = layui.layer;
							layer.msg(data.msg,{icon : 1,time : 2000	},
									function() {location.reload();
									});
							});
						}else {
							layui.use(['laypage','layer' ],function() {
								var laypage = layui.laypage, layer = layui.layer;
								layer.msg(data.msg,{icon : 2,time : 2000	});
								});
						}
					});
				});
			});
		</script>
			
							<!-- 退出登录的方法 -->
	<script type="text/javascript">
			$(function(){
				$("#exit").click(function(){
					$.get("${pageContext.request.contextPath}/ULogin.action","op=exit",function(data,status){
						location.reload();
					});
				});
			});
		</script>
</body>
</html>