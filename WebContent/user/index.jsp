<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
     <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>

	<head>
		<title>中医养生</title>
		<meta name="renderer" content="webkit">
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=0,minimal-ui">
		<meta name="format-detection" content="telephone=no" />
		<meta name="generator" content="MetInfo 6.0.0" data-variable="other/|cn|10001|0|10001|mui145" />
		<link href="other/favicon.ico" rel="shortcut icon" type="image/x-icon" />
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/user/css/basic.css">
		<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/user/css/index_cn.css?1553566375" />
		
		
		<link href="${pageContext.request.contextPath}/user/css/bootstrap.css" type="text/css" rel="stylesheet" media="all">
<link href="${pageContext.request.contextPath}/user/css/style.css" type="text/css" rel="stylesheet" media="all">
<link rel="stylesheet" href="${pageContext.request.contextPath}/user/css/flexslider.css" type="text/css" media="screen" />
<!-- js -->
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
<!-- //js -->	
<!-- start-smoth-scrolling-->
<script type="text/javascript" src="${pageContext.request.contextPath}/user/js/move-top.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/user/js/easing.js"></script>	

<script defer src="${pageContext.request.contextPath}/user/js/jquery.flexslider.js"></script>
<script src="${pageContext.request.contextPath}/user/js/classie.js"></script>
<script src="${pageContext.request.contextPath}/user/js/uisearch.js"></script>
<script src="${pageContext.request.contextPath}/user/js/responsiveslides.min.js"></script>

<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});
		});
</script>
<!--//end-smoth-scrolling-->
<!--pop-up-->
<script src="${pageContext.request.contextPath}/user/js/menu_jquery.js"></script>
		<style>
			body {
				background-position: center;
				background-repeat: no-repeat;
				background-color: #ffffff;
				font-family: other/;
				background: #00E7AF;
			}
		</style>

	<body class="met-navfixed     ny-banner " id="bd">
		<header class='met-head navbar-fixed-top' m-id='4' m-type='head_nav' met-imgmask>
			<nav class="navbar navbar-default box-shadow-none head_nav_met_16_4     ">
				<div class="container">
					<div class="row">
						<h1 hidden></h1>
						<button type="button" class="navbar-toggler hamburger hamburger-close collapsed p-x-5 head_nav_met_16_4-toggler" data-target="#head_nav_met_16_4-collapse" data-toggle="collapse">
                    <span class="sr-only"></span>
                    <span class="hamburger-bar"></span>
                </button>
						<!-- 导航 -->
						<%@include file="header.jsp" %>
						<!-- 导航 -->
					</div>
				</div>
			</nav>
		</header>
		<div class="banner_met_16_2 page-bg" data-height='' style='' m-id='3' m-type='banner'>
			<div class="slick-slide">
				<img class="cover-image" src="${pageContext.request.contextPath}/user/images/1.jpg" srcset='images/1.jpg 767w,images/1.jpg' sizes="(max-width: 767px) 767px" alt="" data-height='0|0|0'>
			</div>
			<div class="slick-slide">
				<img class="cover-image" src="${pageContext.request.contextPath}/user/images/2.jpg" srcset='images/2.jpg 767w,images/2.jpg' sizes="(max-width: 767px) 767px" alt="" data-height='0|0|0'>
			</div>
			<div class="slick-slide">
				<img class="cover-image" src="${pageContext.request.contextPath}/user/images/3.jpg" srcset='images/3.jpg 767w,images/3.jpg' sizes="(max-width: 767px) 767px" alt="" data-height='0|0|0'>
			</div>
		</div>
		<div class="service_list_met_16_1 met-index-body text-xs-center     bgcolor" m-id='45' style="background-image: url(images/背景.jpg);">
			<div class="    container">
				<h2 class="m-t-0 font-weight-300 invisible" data-plugin="appear" data-animate="slide-top" data-repeat="false">DY养生</h2>
				<p class="desc m-b-0 font-weight-300 invisible" data-plugin="appear" data-animate="fade" data-repeat="false">专家团队</p>
				<ul class="blocks-xs-2	blocks-md-2 blocks-lg-5 blocks-xxl-5 index-service-list" id="doctors">
					<li class="invisible" data-plugin="appear" data-animate="slide-bottom50" data-repeat="false">
						<a href="javascript:;"  target='_self'>
							<img >
							<h3 class='m-t-20 m-b-5 font-weight-300'></h3>
						</a>
					</li>
					<li class="invisible" data-plugin="appear" data-animate="slide-bottom50" data-repeat="false">
						<a href="javascript:;" target='_self'>
							<img >
							<h3 class='m-t-20 m-b-5 font-weight-300'></h3>
						</a>
					</li>
					<li class="invisible" data-plugin="appear" data-animate="slide-bottom50" data-repeat="false">
						<a href="javascript:;" target='_self'>
							<img >
							<h3 class='m-t-20 m-b-5 font-weight-300'></h3>
						</a>
					</li>
					<li class="invisible" data-plugin="appear" data-animate="slide-bottom50" data-repeat="false">
						<a href="javascript:;" target='_self'>
							<img >
							<h3 class='m-t-20 m-b-5 font-weight-300'></h3>
						</a>
					</li>
					<li class="invisible" data-plugin="appear" data-animate="slide-bottom50" data-repeat="false">
						<a href="javascript:;"  target='_self'>
							<img >
							<h3 class='m-t-20 m-b-5 font-weight-300'></h3>
						</a>
					</li>
				</ul>
				
			</div>
		</div>
		<div class="product_list_met_16_2 met-index-body text-xs-center bgcolor" m-id=' 44' style="background-image: url(images/背景.jpg)">
			<div class="container">
				<h2 class="m-t-0 font-weight-300 invisible" data-plugin="appear" data-animate="slide-top" data-repeat="false">特色疗法</h2>
				<p class="desc m-b-0 font-weight-300 invisible" data-plugin="appear" data-animate="fade" data-repeat="false">为顾客提供优异的产品和周到的服务！</p>
				<div class="tab-content">
					<ul class="
			    			blocks-xs-2					 	blocks-md-2 blocks-lg-4 blocks-xxl-4 no-space imagesize index-product-list tab-pane active animation-scale-up" id="all" role="tabpanel" data-scale='$ui[img_h]X$ui[img_w]'>
						<li class='p-r-10 m-b-10' data-type="list_0">
							<div class="card card-shadow">
								<figure class="card-header cover">
									<!-- <a href="other/product/showproduct.php?id=16" title="疏通十二经络" target=_self> -->
										<img class="cover-image lazy" data-original="${pageContext.request.contextPath}/user/images/1533777111.jpg" alt="疏通十二经络"></a>
								</figure>
								<h4 class="card-title m-0 p-x-10 text-shadow-none font-szie-16">
							<a href="other/product/showproduct.php?id=16" title="疏通十二经络" class="block text-truncate" target=_self>疏通十二经络</a>
							<p class='m-b-0 m-t-5 product_price'></p>
						</h4>
							</div>
						</li>
						<li class='p-r-10 m-b-10' data-type="list_0">
							<div class="card card-shadow">
								<figure class="card-header cover">
									<!-- <a href="other/product/showproduct.php?id=15" title="全息塔灸" target=_self> -->
										<img class="cover-image lazy" data-original="${pageContext.request.contextPath}/user/images/1533778182.jpg" alt="全息塔灸"></a>
								</figure>
								<h4 class="card-title m-0 p-x-10 text-shadow-none font-szie-16">
							<a href="other/product/showproduct.php?id=15" title="全息塔灸" class="block text-truncate" target=_self>全息塔灸</a>
							<p class='m-b-0 m-t-5 product_price'></p>
						</h4>
							</div>
						</li>
						<li class='p-r-10 m-b-10' data-type="list_0">
							<div class="card card-shadow">
								<figure class="card-header cover">
								<!-- 	<a href="other/product/showproduct.php?id=13" title="太极足心道" target=_self> -->
										<img class="cover-image lazy" data-original="${pageContext.request.contextPath}/user/images/1533870357.jpg" alt="太极足心道"></a>
								</figure>
								<h4 class="card-title m-0 p-x-10 text-shadow-none font-szie-16">
							<a href="other/product/showproduct.php?id=13" title="太极足心道" class="block text-truncate" target=_self>太极足心道</a>
							<p class='m-b-0 m-t-5 product_price'></p>
						</h4>
							</div>
						</li>
						<li class='p-r-10 m-b-10' data-type="list_0">
							<div class="card card-shadow">
								<figure class="card-header cover">
									<!-- <a href="other/product/showproduct.php?id=17" title="红砭背部经络刮痧" target=_self> -->
										<img class="cover-image lazy" data-original="${pageContext.request.contextPath}/user/images/1533715904.jpg" alt="红砭背部经络刮痧"></a>
								</figure>
								<h4 class="card-title m-0 p-x-10 text-shadow-none font-szie-16">
							<a href="other/product/showproduct.php?id=17" title="红砭背部经络刮痧" class="block text-truncate" target=_self>红砭背部经络刮痧</a>
							<p class='m-b-0 m-t-5 product_price'></p>
						</h4>
							</div>
						</li>
						<li class='p-r-10 m-b-10' data-type="list_0">
							<div class="card card-shadow">
								<figure class="card-header cover">
									<!-- <a href="other/product/showproduct.php?id=11" title="内功正骨" target=_self> -->
										<img class="cover-image lazy" data-original="${pageContext.request.contextPath}/user/images/1533776494.jpg" alt="内功正骨"></a>
								</figure>
								<h4 class="card-title m-0 p-x-10 text-shadow-none font-szie-16">
							<a href="other/product/showproduct.php?id=11" title="内功正骨" class="block text-truncate" target=_self>内功正骨</a>
							<p class='m-b-0 m-t-5 product_price'></p>
						</h4>
							</div>
						</li>
						<li class='p-r-10 m-b-10' data-type="list_0">
							<div class="card card-shadow">
								<figure class="card-header cover">
									<!-- <a href="other/product/showproduct.php?id=12" title="排湿寒火罐" target=_self> -->
										<img class="cover-image lazy" data-original="${pageContext.request.contextPath}/user/images/1533782997.jpg" alt="排湿寒火罐"></a>
								</figure>
								<h4 class="card-title m-0 p-x-10 text-shadow-none font-szie-16">
							<a href="other/product/showproduct.php?id=12" title="排湿寒火罐" class="block text-truncate" target=_self>排湿寒火罐</a>
							<p class='m-b-0 m-t-5 product_price'></p>
						</h4>
							</div>
						</li>
						<li class='p-r-10 m-b-10' data-type="list_0">
							<div class="card card-shadow">
								<figure class="card-header cover">
									<!-- <a href="other/product/showproduct.php?id=14" title="中药养筋浴" target=_self> -->
										<img class="cover-image lazy" data-original="${pageContext.request.contextPath}/user/images/1533777215.jpg" alt="中药养筋浴"></a>
								</figure>
								<h4 class="card-title m-0 p-x-10 text-shadow-none font-szie-16">
							<a href="other/product/showproduct.php?id=14" title="中药养筋浴" class="block text-truncate" target=_self>中药养筋浴</a>
							<p class='m-b-0 m-t-5 product_price'></p>
						</h4>
							</div>
						</li>
					</ul>
					<a href="${pageContext.request.contextPath}/UService.action" title="特色疗法" class="more">查看更多</a>
				</div>
			</div>
		</div>
		
		
		
		
		
		<div class="product_list_met_16_2 met-index-body text-xs-center     bgcolor" m-id='44' style="background-image: url(images/背景.jpg)">
			<div class="container">
				<h2 class="m-t-0 font-weight-300 invisible" data-plugin="appear" data-animate="slide-top" data-repeat="false">养生视频</h2>
				<p class="desc m-b-0 font-weight-300 invisible" data-plugin="appear" data-animate="fade" data-repeat="false">了解更多的养生方式！</p>
				<div class="tab-content">
					<!--动态创建视频  -->
					<ul class="blocks-xs-2	blocks-md-2 blocks-lg-4 blocks-xxl-4 no-space imagesize index-product-list tab-pane active animation-scale-up" id="allv" role="tabpanel" data-scale='$ui[img_h]X$ui[img_w]'>
						
					</ul> 	
					
				</div>
			</div>
		</div>
		
		
		<section class="news_list_met_21_7  text-xs-left     bgcolor" m-id='40' style="background-image: url(images/背景.jpg);">
			<div class="container">
				<div class="title-box clearfix">
					<h2 class="title" data-plugin="appear" data-animate="slide-top" data-repeat="false">养生知识</h2>
					
				</div>
				<div class="img-news clearfix">
					<ul class="imgnews-list">
						<li class="item-1">
								<div class="img">
									<img data-original="${pageContext.request.contextPath}/user/images/1533776637.jpg" alt="常见疾病艾灸疗法，有了它医院会恨你的">
								</div>
								<div class="text clearfix">
									<div class="date">
										<span class="day">2019</span>
									</div>
									<div class="content">
										<h3>常见疾病艾灸疗法，有了它医院会恨你的</h3>
										<p>归纳总结了一些常见病症的艾灸疗法：一. &nbsp;内 &nbsp;科二. &nbsp;外 &nbsp;科三. &nbsp;妇 &nbsp;科四. &nbsp;小儿科五. &nbsp;五官科一. 内 &nbsp;科病名主穴配穴用法高血压涌泉、百会、曲池、足三里、悬钟头痛头晕：风池、大椎；健忘失眠：神门、肾俞；肢麻：外关。温灸：每穴灸15-20分钟，每日1-2次，15次1疗程。脑动脉硬化足三里、悬钟</p>
									</div>
								</div>
							
						</li>
						<ul id="suibian">
<!-- 						<li class="item-other articleFor" > -->
<!-- 							<a href="other/news/shownews.php?id=17"  target=_self class="clearfix"> -->
<!-- 								<h3 class="title  kTitle"></h3> -->
<!-- 								<i class="fa fa-angle-right"></i> -->
<!-- 								<p><span class="item-other-desc kContent"></span><span class="time  kTime">2018-08-09</span></p> -->
<!-- 							</a> -->
<!-- 						</li> -->
						
					</ul>
						
					</ul>
				</div>
				<a href="../ArticleServlet.action?op=allArticle" title="常见问题" target='_self' class="btn-more">
					MORE <i class="fa fa-angle-right"></i>
				</a>
			</div>
		</section>
		<%@include file="foot.jsp" %>
		<script src="${pageContext.request.contextPath}/user/js/basic.js"></script>
		<script src="${pageContext.request.contextPath}/user/js/index_cn.js?1553566375"></script>
		
		
			
		<!-- 登录的方法 及样式的链接 -->
	<link rel="stylesheet"
		href="${pageContext.request.contextPath}/user/layui/css/layui.css"
		media="all">
	<script src="${pageContext.request.contextPath}/user/layui/layui.js"
		charset="utf-8"></script>
						
		<script type="text/javascript">
			$(function(){
				$("#login").click(function(){
					$.post("${pageContext.request.contextPath}/ULogin.action?op=login",$("#loginForm").serialize(),function(data,status){
						if(data.msg=="用户登录成功"||data.msg=="医生登录成功"){
						layui.use(['laypage','layer' ],function() {
							var laypage = layui.laypage, layer = layui.layer;
							layer.msg(data.msg,{icon : 1,time : 2000	},
									function() {location.reload();
									});
							});
						}else {
							layui.use(['laypage','layer' ],function() {
								var laypage = layui.laypage, layer = layui.layer;
								layer.msg(data.msg,{icon : 2,time : 2000	});
								});
						}
					});
				});
				
				/* 显示医生信息 */
				$.ajax({
					type:"get",
					url:"../UDoctor.action",
					data:{
						op : "queryDocs"
					},
					success:function(data) {
					
						
						$.each(data,function(index,obj){
							
							//console.log(item[0].dName);
							//设置医生团队的名字 图片路径 
							$("#doctors li:nth-child(1) a h3").text(obj[0].dName);
							$("#doctors li:nth-child(1) a img").attr("src","${pageContext.request.contextPath}"+obj[0].dUrl);
							$("#doctors li:nth-child(1) a").attr("title",obj[0].dName);
							$("#doctors li:nth-child(1) a img").attr("alt",obj[0].dName);
							
							$("#doctors li:nth-child(2) a h3").text(obj[1].dName);
							$("#doctors li:nth-child(2) a img").attr("src","${pageContext.request.contextPath}"+obj[1].dUrl);
							$("#doctors li:nth-child(2) a").attr("title",obj[1].dName);
							$("#doctors li:nth-child(2) a img").attr("alt",obj[1].dName);
							
							$("#doctors li:nth-child(3) a h3").text(obj[2].dName);
							$("#doctors li:nth-child(3) a img").attr("src","${pageContext.request.contextPath}"+obj[2].dUrl);
							$("#doctors li:nth-child(3) a").attr("title",obj[2].dName);
							$("#doctors li:nth-child(3) a img").attr("alt",obj[2].dName);
							
							$("#doctors li:nth-child(4) a h3").text(obj[3].dName);
							$("#doctors li:nth-child(4) a img").attr("src","${pageContext.request.contextPath}"+obj[3].dUrl);
							$("#doctors li:nth-child(4) a").attr("title",obj[3].dName);
							$("#doctors li:nth-child(4) a img").attr("alt",obj[3].dName);
							
							$("#doctors li:nth-child(5) a h3").text(obj[4].dName);
							$("#doctors li:nth-child(5) a img").attr("src","${pageContext.request.contextPath}"+obj[4].dUrl);
							$("#doctors li:nth-child(5) a").attr("title",obj[4].dName);
							$("#doctors li:nth-child(5) a img").attr("alt",obj[4].dName);
							
							
						});
						
					}
					
				});
				
				
				/* 显示视频  */
				
				$.ajax({
					type:"get",
					url:"../UVideo.action",
					data:{
						op : "queryVideos"
					},
					success:function(data) {
						$.each(data,function(index,video){
							for(var i=0;i<video.length;i++){
								$("#allv").append('<li class="p-r-10 m-b-10" data-type="list_0">'+
							'<div class="card card-shadow">'+
								'<figure class="card-header cover">'+
									'<video width="320" height="240" controls="controls">'+
										'<source src='+video[i].vSrc+' type="video/mp4" />'+
										'<object data="movie.mp4" width="320" height="240">'+
 								  				' <embed src="movie.swf" width="320" height="240" /></object>'+
									'</video></figure>'+
								'<h4 class="card-title m-0 p-x-10 text-shadow-none font-szie-16">'+
							'<a href="javascript:;">'+video[i].vTitle+'</a>'+
							'<p class="m-b-0 m-t-5 product_price"></p></h4></div></li>');
							}
							
						});
						
					}
					
				});
				
				
				
			});
		</script>
		
		
		
						<!-- 退出登录的方法 -->
	<script type="text/javascript">
			$(function(){
				$("#exit").click(function(){
					$.get("${pageContext.request.contextPath}/ULogin.action","op=exit",function(data,status){
						location.reload();
					});
				});
		//首页文章展示的Ajax		
				$.ajax({
						type:"get",
						url:"../ArticleServlet.action",
						data:{
							op:"articalIndex",
						}
				,success:function(data){
					console.log(data);
					  $.each(data,function(index,list){
						for (var i = 0; i < list.length; i++) {
							  $("#suibian").append('<li class="item-other articleFor" >'+
							'<a href="../UKnowledgedisServlet.action?op=querykd&kId='+list[i].kId+'"target=_self class="clearfix">'+
								'<h3 class="title  kTitle">'+list[i].kTitle+'</h3>'+
								'<i class="fa fa-angle-right"></i>'+
								'<p><span class="item-other-desc kContent">'+list[i].kContent+'</span><span class="time  kTime">'+list[i].kTime+'</span></p>'+
							'</a>'+
						'</li>');
						}
					  }); 
					
							}	
			});
					});
			
			
		</script>
	</body>

</html>