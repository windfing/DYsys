<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<title>预约信息</title>
<meta name="renderer" content="webkit">
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0,user-scalable=0,minimal-ui">
<meta name="format-detection" content="telephone=no" />
<meta name="generator" content="MetInfo 6.0.0"
	data-variable="other/|cn|3|0|3|mui145" />
<link href="other/favicon.ico" rel="shortcut icon" type="image/x-icon" />
<link rel='stylesheet' type='text/css' href='${pageContext.request.contextPath}/user/css/basic.css'>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/user/css/product_cn.css?1553566664" />

	<!-- dataTables.min.css -->
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"  type="text/css" media="all"/>
	<!-- js -->
<script src="http://libs.baidu.com/jquery/2.0.0/jquery.min.js"></script>
<!-- start-smoth-scrolling-->
<script type="text/javascript" src="${pageContext.request.contextPath}/user/js/move-top.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/user/js/easing.js"></script>

<link href="${pageContext.request.contextPath}/user/css/bootstrap.css" type="text/css" rel="stylesheet"
	media="all">
<script src="${pageContext.request.contextPath}/user/js/classie.js"></script>
<script src="${pageContext.request.contextPath}/user/js/uisearch.js"></script>
<script src="${pageContext.request.contextPath}/user/js/responsiveslides.min.js"></script>
<!-- 表格内容居中 -->
<style type="text/css">
.table>tbody>tr>td{
        text-align:center;
}
</style>

<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event) {
			event.preventDefault();
			$('html,body').animate({
				scrollTop : $(this.hash).offset().top
			}, 1000);
		});
	});
</script>
<!--//end-smoth-scrolling-->
<!--pop-up-->
<script src="${pageContext.request.contextPath}/user/js/menu_jquery.js"></script>


<style>
body {
	background-image: url(other/);
	background-position: center;
	background-repeat: no-repeat;
	background-color: #ffffff;
	font-family: other/;
}
</style>
</head>
<body class="met-navfixed     ny-banner ">
	<header class='met-head navbar-fixed-top' m-id='4' m-type='head_nav'
		met-imgmask> <nav
		class="navbar navbar-default box-shadow-none head_nav_met_16_4     ">
	<div class="container">
		<div class="row">
			<h3 hidden></h3>
			<button type="button"
				class="navbar-toggler hamburger hamburger-close collapsed p-x-5 head_nav_met_16_4-toggler"
				data-target="#head_nav_met_16_4-collapse" data-toggle="collapse">
				<span class="sr-only"></span> <span class="hamburger-bar"></span>
			</button>
			<!-- 会员注册登录 -->

			<!-- 会员注册登录 -->
			<!-- 导航 -->
			<%@include file="header.jsp"%>
			<!-- 导航 -->
		</div>
	</div>
	</nav> </header>

	<div class="banner_met_16_2 page-bg" data-height='' style='' m-id='3'
		m-type='banner'>
		<div class="slick-slide">
			<img class="cover-image" src="${pageContext.request.contextPath}/user/images/1533699549.jpg"
				srcset='${pageContext.request.contextPath}/user/images/1533699549.jpg 767w,${pageContext.request.contextPath}/user/images/1533699549.jpg'
				sizes="(max-width: 767px) 767px" alt="" data-height='0|0|0'>
		</div>
		<div class="slick-slide">
			<img class="cover-image" src="${pageContext.request.contextPath}/user/images/1527575184.jpg"
				srcset='${pageContext.request.contextPath}/user/images/1527575184.jpg 767w,${pageContext.request.contextPath}/user/images/1527575184.jpg'
				sizes="(max-width: 767px) 767px" alt="" data-height='0|0|0'>
		</div>
	</div>
	
	<!-- dateTable -->						
											<div id="demoo" align="center">
											<h2>已预约的服务</h2>
										<table class="table table-bordered table-hover display"
											id="example1" style="width: 100%" >

											<thead>
												<tr>
													<th class="text-center">预约项目</th>
													<th class="text-center">预约的医生</th>
													<th class="text-center">预约的时间</th>
													<th class="text-center">下单的时间</th>
													<th class="text-center">操作</th>
												</tr>
											</thead>

										</table>
										</div>	
										
										
									<div align="center" id="demoo">
												<h2>已完成的服务</h2>
										<table class="table table-bordered table-hover display"
											id="example2" style="width: 100%">

											<thead>
												<tr>
													<th class="text-center">预约项目</th>
													<th class="text-center">预约的医生</th>
													<th class="text-center">预约的时间</th>
													<th class="text-center">下单的时间</th>
													
												</tr>
											</thead>

										</table>
									</div>
									

			<%@include file="foot.jsp"%>
			<footer class='foot_info_met_16_2 met-foot p-y-10 border-top1'
				m-id='47' m-type='foot'>
			<div class="container text-xs-center">
				<div>
					<span class="powered_by_metinfo">Powered by <b><a
							href=http://www.metinfo.cn target=_blank>MetInfo 6.0.0</a></b>
						&copy;2008-2019 &nbsp;<a href=http://www.metinfo.cn target=_blank>MetInfo
							Inc.</a></span>
				</div>
			</div>
			</footer>
			<button type="button"
				class="btn btn-icon btn-primary btn-squared back_top_met_16_1 met-scroll-top"
				hidden m-id='8' m-type='nocontent'>
				<i class="icon wb-chevron-up" aria-hidden="true"></i>
			</button>
			<input type="hidden" name="met_lazyloadbg" value="other/">
			<script src="${pageContext.request.contextPath}/user/js/product_cn.js?1553566664"></script>
			

			<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script
		src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
		

<link rel="stylesheet"href="${pageContext.request.contextPath}/user/layui/css/layui.css"media="all"/>
<script src="${pageContext.request.contextPath}/user/layui/layui.js" charset="utf-8"></script>
		
		
						<!-- 退出登录的方法 -->
	<script type="text/javascript">
			$(function(){
				$("#exit").click(function(){
					$.get("${pageContext.request.contextPath}/ULogin.action","op=exit",function(data,status){
						location.href="${pageContext.request.contextPath}/user/index.jsp";
					});
				});
			});
		</script>
		
<script type="text/javascript">
   $(document).ready(
		function() {
			var table = $('#example1').DataTable({
				ajax : "../UAppointment.action?op=userShowunfinished&uId="+${users.uId},
				"oLanguage": {
					"sZeroRecords": "抱歉， 没有找到",
					"sInfoEmpty": "没有数据",
					"oPaginate": {
					"sFirst": "首页",
					"sPrevious": "前一页",
					"sNext": "后一页",
					"sLast": "尾页"
					},
					"sZeroRecords": "没有检索到数据",
					},
				columns : [ {
					data : "sTitle"
				}, {
					data : "dName"
				}, {
					data : "aTime"
				}, {
					data : "aCurrentTime"
				}, {
					"defaultContent" : "<input type='button' class='btn btn-danger btnDel' value='取消预约'>"
				}]
			});
			
			$('#example1').on('click','.btnDel',function() {
				var data = table.row($(this).parents('tr')).data();
				layui.use(['laypage','layer' ],function() {
					var laypage = layui.laypage, layer = layui.layer;
				
				layer.confirm('是否要删除？',{
					  btn: ['是','否'] //按钮
					}, function(){
						layer.msg('正在取消预约', {icon: 1});
						$.get("../UAppointment.action?op=userCanceAppointment&aId="+ data.aId,function(data,status) {
							layer.msg(data.msg,{icon : 1,time : 2000	},
							function() {location.reload();
							});
								});
					}, function(){
					  layer.msg('正在取消操作', {
					    time: 1000, 
					  });
					});
				});
			
				});
		});
</script>
<script type="text/javascript">
$(document)
.ready(
		function() {
			var table = $('#example2').DataTable({
				ajax : "../UAppointment.action?op=userShowsuccess&uId="+${users.uId},
				columns : [ {
					data : "sTitle"
				}, {
					data : "dName"
				}, {
					data : "aTime"
				}, {
					data : "aCurrentTime"
				}]
			});

		});
</script>
   
			
</body>
</html>