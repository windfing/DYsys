<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML>
<html>
<head>
<!-- 引入公共部分meta.jsp -->
<jsp:include page="meta.jsp"></jsp:include>

<title>专家列表 </title>
<style type="text/css">
/* dDesc样式设置 */
.desc{
    overflow:hidden; 
 	text-overflow:ellipsis; 
 	display: -webkit-box;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 3;
    height:56px;
      }

</style>

</head>
<body>
<!-- 导入公共部分header.jsp -->
	<jsp:include page="header.jsp"></jsp:include>

<!-- 导入公共部分left.jsp -->
	<jsp:include page="left.jsp"></jsp:include>
<section class="Hui-article-box">
	<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页
		<span class="c-gray en">&gt;</span>
		专家管理
		<span class="c-gray en">&gt;</span>
		专家列表 
		<!-- <a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a> -->
    </nav>
	<div class="Hui-article">
		<article class="cl pd-20">
			<div class="cl pd-5 bg-1 bk-gray mt-20">
				<span class="l"> 
				<!-- <a href="javascript:;" onclick="datadel()" class="btn btn-danger radius"><i class="Hui-iconfont">&#xe6e2;</i> 批量删除</a>  -->
				<a href="javascript:;" onclick="experts_add()" class="btn btn-primary radius"><i class="Hui-iconfont">&#xe600;</i> 添加专家</a>
				 </span>
				<!-- <span class="r">共有数据：<strong>54</strong> 条</span> -->
				
			</div> 
			<div class="mt-20">
			<table class="table table-border table-bordered table-bg table-hover table-sort ">
				<thead>
					
					<tr class="text-c">
						<th width="30"><input type="checkbox" name="" value="1"></th>
						<th width="40">ID</th>
						<th width="100">登录名</th>
						<th width="150">密码</th>
						<th width="50">性别</th>
						<th width="100">出生日期</th>
						<th width="200">描述</th>
						<th width="200">图片路径</th>
						<th width="100">是否已启用</th>
						<th width="100">操作</th>
					</tr>
				</thead>
				<tbody>
				<!-- 遍历所有医生 -->
				    <c:forEach items="${list}" var="d">
				    <tr class="text-c">
						<td><input type="checkbox" value="1" name=""></td>
						<td>${d.dId}</td>
						<td>${d.dName}</td>
						<td>${d.dPassword}</td>
						<td>${d.dSex}</td>
						<td>${d.dBirthday}</td>
						<td class="desc">${d.dDesc}</td>
						<td>${d.dUrl}</td>
						<td class="td-status">
						<c:if test="${d.dStatus==0}">
						<span class="label label-success radius">已启用</span>
						</c:if>
						<c:if test="${d.dStatus==1}">
						<span class="label label-default radius">已禁用</span>
						</c:if>
						</td>
						<td class="td-manage">
						<c:if test="${d.dStatus==1}">
						<a style="text-decoration:none" onclick="experts_start(this,${d.dId},${d.dStatus})" href="javascript:;" title="启用"><i class="Hui-iconfont">&#xe631;</i></a> 
						</c:if>
						<c:if test="${d.dStatus==0}">
						<a style="text-decoration:none" onclick="experts_stop(this,${d.dId},${d.dStatus})" href="javascript:;" title="停用"><i class="Hui-iconfont">&#xe631;</i></a> 
						</c:if>
						<a title="编辑" href="javascript:;" onclick="experts_edit(${d.dId})" class="ml-5" style="text-decoration:none"><i class="Hui-iconfont">&#xe6df;</i></a> 
						<a title="删除" href="javascript:;" onclick="experts_del(this,${d.dId})" class="ml-5" style="text-decoration:none"><i class="Hui-iconfont">&#xe6e2;</i></a>
						</td>				    
					</tr>
					</c:forEach>
				</tbody>
				
			</table>
			</div>
		</article>
	</div>
</section>

<!-- 导入公共部分footer.jsp 这是jquery.min.js、layer.js、H-ui.js、H-ui.admin.page.js -->
	<jsp:include page="footer.jsp"></jsp:include>

<!--请在下方写此页面业务相关的脚本-->
(jquery.dataTables.min.js:56) 
<script type="text/javascript" src="${pageContext.request.contextPath}/admin/lib/My97DatePicker/4.8/WdatePicker.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/admin/lib/datatables/1.10.0/jquery.dataTables.min.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/admin/lib/laypage/1.2/laypage.js"></script> 
<script type="text/javascript">

$(function() {

	//分页及搜索
	$('.table-sort').dataTable({
		"aaSorting" : [ [ 1, "asc" ] ],//默认第几个排序
		"bStateSave" : true,//状态保存
		"aoColumnDefs" : [
		//{"bVisible": false, "aTargets": [ 3 ]} //控制列的隐藏显示
		{
			"orderable" : false,
		//	"aTargets" : [ 0, 8, 9 ]
		} // 制定列不参与排序
		]
	});
	$('.table-sort tbody').on('click', 'tr', function() {
		if ($(this).hasClass('selected')) {
			$(this).removeClass('selected');
		} else {
			table.$('tr.selected').removeClass('selected');
			$(this).addClass('selected');
		}
	});
});


/*
	参数解释：
	title	标题
	url		请求的url
	id		需要操作的数据id
	w		弹出层宽度（缺省调默认值）
	h		弹出层高度（缺省调默认值）
*/
/*专家-增加*/
function experts_add(){
	location.href="${pageContext.request.contextPath}/admin/experts-add2.jsp"
}

/*专家-删除*/
function experts_del(obj,id){
	layer.confirm('确认要删除吗？',function(index){
		//此处请求后台程序，下方是成功后的前台处理……	
		$(obj).parents("tr").remove();
		layer.msg('已删除!',{icon:1,time:1000});
		
		location.href="${pageContext.request.contextPath}/Doctor.action?op=deletedoctor&dId="+id;
	});
}
/*专家-编辑*/
function experts_edit(id){

	location.href="${pageContext.request.contextPath}/Doctor.action?op=selectdoctor&dId="+id;
	
}
/*专家-停用*/
function experts_stop(obj,id,status){
	layer.confirm('确认要停用吗？',function(index){
		//此处请求后台程序，下方是成功后的前台处理……       
		$(obj).parents("tr").find(".td-manage").prepend('<a onClick="experts_start(this,id,status)" href="javascript:;" title="启用" style="text-decoration:none"><i class="Hui-iconfont">&#xe615;</i></a>');
		$(obj).parents("tr").find(".td-status").html('<span class="label label-default radius">已禁用</span>');
		$(obj).remove();
		layer.msg('已停用!',{icon: 5,time:1000});									
		location.href="${pageContext.request.contextPath}/Doctor.action?op=updatestatus&dId="+id+"&dStatus="+status;
	});
}

/*专家-启用*/
function experts_start(obj,id,status){
	layer.confirm('确认要启用吗？',function(index){
		//此处请求后台程序，下方是成功后的前台处理……
		
		$(obj).parents("tr").find(".td-manage").prepend('<a onClick="experts_stop(this,id,status)" href="javascript:;" title="停用" style="text-decoration:none"><i class="Hui-iconfont">&#xe631;</i></a>');
		$(obj).parents("tr").find(".td-status").html('<span class="label label-success radius">已启用</span>');
		$(obj).remove();
		layer.msg('已启用!', {icon: 6,time:1000});
		location.href="${pageContext.request.contextPath}/Doctor.action?op=updatestatus&dId="+id+"&dStatus="+status;
	});
}
</script> 
<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>