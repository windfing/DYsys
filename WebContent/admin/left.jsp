<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!--_menu 作为公共模版分离出去-->
	<aside class="Hui-aside">

		<div class="menu_dropdown bk_2">


			<dl id="menu-member">
				<dt>
					<i class="Hui-iconfont">&#xe60d;</i> 普通用户管理<i
						class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i>
				</dt>
				<dd>
					<ul>
						<li><a href="${pageContext.request.contextPath}/users.action" title="用户列表">用户列表</a></li>
					</ul>
				</dd>
			</dl>

			<dl id="menu-admin">
				<dt>
					<i class="Hui-iconfont">&#xe62d;</i> 专家管理<i
						class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i>
				</dt>
				<dd>
					<ul>
						<li><a href="${pageContext.request.contextPath}/Doctor.action?op=query" title="专家管理">专家列表</a></li>
					</ul>
				</dd>
			</dl>


			<dl id="menu-admin">
				<dt>
					<i class="Hui-iconfont">&#xe62d;</i> 预约管理<i
						class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i>
				</dt>
				<dd>
					<ul>
						<li><a href="${pageContext.request.contextPath}/admin/reservation-list.jsp" title="预约服务列表">预约列表</a></li>
					</ul>
				</dd>
			</dl>
			
			<dl id="menu-admin">
				<dt>
					<i class="Hui-iconfont">&#xe62d;</i> 服务管理(特色疗法)<i
						class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i>
				</dt>
				<dd>
					<ul>

						<li><a href="${pageContext.request.contextPath}/Service.action" title="预约服务列表">服务列表(特色疗法)</a></li>

					</ul>
				</dd>
			</dl>

			<dl id="menu-article">
				<dt>
					<i class="Hui-iconfont">&#xe616;</i> 养生知识管理<i
						class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i>
				</dt>
				<dd>
					<ul>

						<li><a href="${pageContext.request.contextPath}/Knowledge.action?op=knowledgePage" title="养生知识管理">养生知识管理</a></li>



					</ul>
				</dd>
			</dl>
			<dl id="menu-picture">
				<dt>
					<i class="Hui-iconfont">&#xe613;</i> 养生课堂管理<i
						class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i>
				</dt>
				<dd>
					<ul>
						<li><a href="${pageContext.request.contextPath}/admin/classroom-list.jsp" title="养生课堂管理">养生课堂管理</a></li>

					</ul>
				</dd>
			</dl>
			<dl id="menu-comments">
				<dt>
					<i class="Hui-iconfont">&#xe622;</i> 评论管理<i
						class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i>
				</dt>
				<dd>
					<ul>

						<li><a href="${pageContext.request.contextPath}/KnowledgedisServlet.action" title="评论列表">知识评论列表</a>

						
					</ul>
				</dd>
			</dl>
		</div>
	</aside>
	<div class="dislpayArrow hidden-xs">
		<a class="pngfix" href="javascript:void(0);"
			onClick="displaynavbar(this)"></a>
	</div>
	<!--/_menu 作为公共模版分离出去-->