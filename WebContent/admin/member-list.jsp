<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!--_meta 作为公共模版分离出去-->
<!DOCTYPE HTML>
<html>
<head>
<!-- 引入公共部分meta.jsp -->
<jsp:include page="meta.jsp"></jsp:include>


<title>用户列表 - 用户管理</title>

</head>
<body>
	<!-- 导入公共部分header.jsp -->
	<jsp:include page="header.jsp"></jsp:include>

	<!-- 导入公共部分left.jsp -->
	<jsp:include page="left.jsp"></jsp:include>

	<section class="Hui-article-box">
		<nav class="breadcrumb">
			<i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span>
			用户中心 <span class="c-gray en">&gt;</span> 用户列表<a
				class="btn btn-success radius r"
				style="line-height: 1.6em; margin-top: 3px"
				href="javascript:location.replace(location.href);" title="刷新"><i
				class="Hui-iconfont">&#xe68f;</i></a>
		</nav>
		<div class="Hui-article">
			<article class="cl pd-20">
				<!-- 用户名搜索 -->
				<div class="text-c">
					<!-- 日期范围： <input type="text"
						onfocus="WdatePicker({maxDate:'#F{$dp.$D(\'datemax\')||\'%y-%M-%d\'}'})"
						id="datemin" class="input-text Wdate" style="width: 120px;">
					- <input type="text"
						onfocus="WdatePicker({minDate:'#F{$dp.$D(\'datemin\')}',maxDate:'%y-%M-%d'})"
						id="datemax" class="input-text Wdate" style="width: 120px;"> -->
					<!-- <input type="text" class="input-text" style="width: 250px"
						placeholder="输入用户名或者用户名的关键字" id="" name="">
					<button type="submit" class="btn btn-success radius" id="" name="">
						<i class="Hui-iconfont">&#xe665;</i> 搜用户
					</button> -->
				</div>
				<div class="cl pd-5 bg-1 bk-gray mt-20">
					<!-- <span class="l"><a href="javascript:;" onclick="datadel()"
						class="btn btn-danger radius"><i class="Hui-iconfont">&#xe6e2;</i>
							批量删除</a>  -->
						<!-- 用户添加 -->	
							<a href="javascript:;"
						onclick="member_add('添加用户','${pageContext.request.contextPath}/admin/member-add.jsp','','510')"
						class="btn btn-primary radius"><i class="Hui-iconfont">&#xe600;</i>
							添加用户</a></span> <span class="r"><strong></strong> </span>
				</div>
				<div class="mt-20">
					<table
						class="table table-border table-bordered table-hover table-bg table-sort">
						<thead>
							<tr class="text-c">
								<th width="25"><input type="checkbox" id="allSelect"
									name="" value=""></th>
								<th width="80">ID</th>
								<th width="100">用户名</th>
								<th width="250">密码</th>
								<th width="40">性别</th>
								<th width="150">手机</th>
								<th width="150">生日</th>
								<th width="">地址</th>
								<th width="70">状态</th>
								<th width="100">操作</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${list}" var="u">
								<tr class="text-c">
									<td><input type="checkbox" value="${u.uId}" name="uId"></td>
									<td>${u.uId}</td>
									<td>${u.uName}</td>
									<td>${u.uPassword}</td>
									<td>${u.uSex}</td>
									<td>${u.uPhone}</td>
									<td>${u.uBirthday}</td>
									<td>${u.uAddress}</td>
									<td class="td-status"><c:if test="${u.uStatus==0}">
											<span class="label label-success radius">已启用</span>
										</c:if> <c:if test="${u.uStatus==1}">
											<span class="label label-defaunt radius">已停用</span>
										</c:if></td>
									<td class="td-manage"><c:if test="${u.uStatus==0}">
											<a style="text-decoration: none"
												onClick="member_stop(this,'10001')" href="javascript:;"
												title="停用"><i class="Hui-iconfont">&#xe631;</i></a>
										</c:if> <c:if test="${u.uStatus==1}">
											<a style="text-decoration: none"
												onClick="member_start(this,id)" href="javascript:;"
												title="启用"><i class="Hui-iconfont">&#xe6e1;</i></a>
										</c:if> <a title="编辑" href="javascript:;"
										onclick="member_edit('编辑','${pageContext.request.contextPath}/admin/member-update.jsp',${u.uId},'','510')"
										class="ml-5" style="text-decoration: none"><i
											class="Hui-iconfont">&#xe6df;</i></a> <%-- <a
										style="text-decoration: none" class="ml-5"
										onClick="change_password('修改密码','${pageContext.request.contextPath}/admin/change-password.jsp','10001','600','270')"
										href="javascript:;" title="修改密码"><i class="Hui-iconfont">&#xe63f;</i></a> --%>
										<a title="删除" href="javascript:;"
										onclick="member_del(this,'1')" class="ml-5"
										style="text-decoration: none"><i class="Hui-iconfont">&#xe6e2;</i></a></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</article>
		</div>
	</section>

	<!-- 导入公共部分footer.jsp 这是jquery.min.js、layer.js、H-ui.js、H-ui.admin.page.js -->
	<jsp:include page="footer.jsp"></jsp:include>

	<!--请在下方写此页面业务相关的脚本-->
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/admin/lib/My97DatePicker/4.8/WdatePicker.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/admin/lib/datatables/1.10.0/jquery.dataTables.min.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/admin/lib/laypage/1.2/laypage.js"></script>
	<script type="text/javascript">
		$(function() {
			$('.table-sort').dataTable({
				"aaSorting" : [ [ 1, "desc" ] ],//默认第几个排序
				"bStateSave" : true,//状态保存
				"aoColumnDefs" : [
				//{"bVisible": false, "aTargets": [ 3 ]} //控制列的隐藏显示
				{
					"orderable" : false,
					"aTargets" : [ 0, 8, 9 ]
				} // 制定列不参与排序
				]
			});
			/* $('.table-sort tbody').on('click', 'tr', function() {
				if ($(this).hasClass('selected')) {
					$(this).removeClass('selected');
				} else {
					table.$('tr.selected').removeClass('selected');
					$(this).addClass('selected');
				}
			}); */
		});
		/*用户-添加*/
		function member_add(title, url, w, h) {
			layer_show(title, url, w, h);
		}
		/*用户-查看*/
		function member_show(title, url, id, w, h) {
			layer_show(title, url, w, h);
		}
		/*用户-停用*/
		function member_stop(obj, id) {
			//获取要停用的那个id
			var uId = $(obj).parents("tr").find("td").eq(1).html();
			layer.confirm('确认要停用吗？', function() {
				$.ajax({
					type : "get",
					url : "users.action",
					data : {
						op : "stop",
						uId : uId
					},
					success : function(data) {
						//如果信息是已停用，就提示信息，重新加载页面
						if (data == "已停用") {
							layer.msg(data, {
								icon : 5,
								time : 1000

							}, function() {
								location.reload();
							});
						}
					}
				});

			});
		}

		/*用户-启用*/
		function member_start(obj, id) {
			//获取要启动的那个id
			var uId = $(obj).parents("tr").find("td").eq(1).html();
			layer.confirm('确认要启用吗？', function() {
				$.ajax({
					type : "get",
					url : "users.action",
					data : {
						op : "start",
						uId : uId
					},
					success : function(data) {
						//如果信息是已启用，就提示信息，重新加载页面
						if (data == "已启用") {
							layer.msg(data, {
								icon : 6,
								time : 1000

							}, function() {
								location.reload();
							});
						}
					}
				});

			});
		}
		/*用户-编辑*/
		function member_edit(title, url, id, w, h) {
			layer_show(title, url, w, h);
			location.href="${pageContext.request.contextPath}/users.action?op=sel&uId="+id;
		}
		/*密码-修改*/
		function change_password(title, url, id, w, h) {
			layer_show(title, url, w, h);
		}
		/*用户-删除*/

		function member_del(obj, id) {
			//获取要删除的那个id
			var uId = $(obj).parents("tr").find("td").eq(1).html();
			layer.confirm('确认要删除吗？', function() {
				$.ajax({
					type : "get",
					url : "users.action",
					data : {
						op : "del",
						uId : uId
					},
					success : function(data) {
						//如果信息是删除成功，就提示信息，重新加载页面
						if (data == "删除成功") {
							layer.msg(data, {
								icon : 1,
								time : 1000

							}, function() {
								location.reload();
							});
						}
					}
				});

			});

		}

		function member_open(obj, id) {
			var kContent = $(obj).parents("tr").find("td").eq(4).text();
			layer.open({
				type : 1,
				skin : 'layui-layer-rim', //加上边框
				area : [ '620px', '440px' ], //宽高
				content : kContent
			});

		}
		/*用户-批量删除*/
		function datadel() {
			layer.confirm('确认要删除吗？', function(index) {
				var option = $(":checked");
				var checkedId = "";
				var boo=true;
				//拼接除全选框外,所有选中的ID,
				for (var i = 0, len = option.length; i < len; i++) {
					if (boo) {
						if (option[i].id=='allSelect') {
							boo=true;
						}else {
							boo=false;
							checkedId += option[i].value;
						}
					}else{
						checkedId += ","+option[i].value;
					}
				}
				 $.ajax({
					  type: "post",
					  url:"users.action",
					  data : {
					  "checkedId":checkedId
					  },
					  dataType:"json",
					  success : function(map) {
						 parent.layer.msg(map.message,{icon: 1,time:1000});
						 setTimeout("serachFromSubmit()", 1000);
					  },error:function(code){ 
					         parent.layer.msg('删除失败!',{icon: 2,time:1000});
					  } 
					});
			});
		}
		    
	</script>
	<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>