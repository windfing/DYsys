<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE HTML>
<html>
<head>
<!-- 引入公共部分meta.jsp -->
<jsp:include page="meta.jsp"></jsp:include>

<title>修改专家 - 专家管理</title>

</head>
<body>
<article class="cl pd-20">
	<form  class="form form-horizontal" action="${pageContext.request.contextPath}/Doctor.action?op=updatedoctor" method="post" enctype="multipart/form-data" >
		<!-- 医生账号 -->
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>专家：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<input type="text" class="input-text" value="${requestScope.doctor.dName}" readonly="readonly" id="dName" name="dName">
			</div>
		</div>
		<!-- 初始密码 -->
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>初始密码：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<input type="password" class="input-text" value="${requestScope.doctor.dPassword}" autocomplete="off" placeholder="密码" id="dPassword" name="dPassword">
			</div>
		</div>
		<!-- 确认密码 -->
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>确认密码：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<input type="password" class="input-text" autocomplete="off" value="${requestScope.doctor.dPassword}"  placeholder="确认新密码" id="dPassword2" name="dPassword2">
			</div>
		</div>
		<!-- 性别 -->
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>性别：</label>
			<div class="formControls col-xs-8 col-sm-9 skin-minimal">
				<div class="radio-box">
					<input name="dSex" type="radio" id="dSex1" value="男" checked="">
					<label for="sex-1">男</label>
				</div>
				<div class="radio-box">
					<input type="radio" id="dSex2" name="dSex" value="女">
					<label for="sex-2">女</label>
				</div>
			</div>
		</div>
		<!-- 出生日期 -->
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>出生日期：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<input type="date" class="input-text" id="dBirthday" name="dBirthday" value="${requestScope.doctor.dBirthday}">
			</div>
		</div>
		<!-- 描述 -->
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>描述：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<textarea id="dDesc" name="dDesc" cols="" rows="" class="textarea"  placeholder="个人职业简介" dragonfly="true" >${requestScope.doctor.dDesc}</textarea>
				<p class="textarea-numberbar"><em class="textarea-length">0</em>/1000</p>
			</div>
		</div>
		<!-- 文件上传 -->
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>照片：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<input type="file"  placeholder="专家图片" value="${requestScope.doctor.dUrl}" id="dUrl" name="dUrl">
			</div>
		</div>
		<!-- 状态 -->
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>状态：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<select name="dStatus" id="dStatus">
				<option value="0">启用</option>
				<option value="1">禁用</option>
			    </select>
			</div>
		</div>
		<!-- 提交信息 -->
		<div class="row cl">
			<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
				<input class="btn btn-primary radius" type="submit" value="&nbsp;&nbsp;提交&nbsp;&nbsp;" id="sub">
			</div>
		</div>
	</form>
</article>

<!-- 导入公共部分footer.jsp 这是jquery.min.js、layer.js、H-ui.js、H-ui.admin.page.js -->
	<jsp:include page="footer.jsp"></jsp:include>

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="${pageContext.request.contextPath}/admin/lib/jquery.validation/1.14.0/jquery.validate.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/admin/lib/jquery.validation/1.14.0/additional-methods.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/admin/lib/jquery.validation/1.14.0/messages_zh.js"></script> 
<script type="text/javascript">
$(function(){

	//密码失焦校验
	$("#dPassword2").blur(function(){
		var p1 = $("#dPassword").val();		
		var p2 = $("#dPassword2").val();
		if(p2!=p1){
			layer.tips('密码不一致', '#dPassword2');
		} 
		
	});
	
    //提交修改
	 /* $("#sub").click(function(){
		$.get("${pageContext.request.contextPath}/Doctor.action?op=updatedoctor", 
				{"dName":$("#dName").val(),
			    "dPassword":$("#dPassword").val(),
				"dSex":$("input[type='radio']:checked").val(),
				"dBirthday":$("#dBirthday").val(),
				"dDesc":$("#dDesc").val(),
				"dUrl":$("#dUrl").val(),
				"dStatus":$("#dStatus").val()},
				function(data, status,
					xhr) {
						
						if(data){
							alert("修改成功！");
							//刷新父类页面
							location.href="${pageContext.request.contextPath}/Doctor.action?op=query";
					
						}else{
							alert("修改失败！");
						}		

		});
	});  */
	

});

</script>
<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>