<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!--_meta 作为公共模版分离出去-->
<!DOCTYPE HTML>
<html>
<head>
<!-- 引入公共部分meta.jsp -->
<jsp:include page="meta.jsp"></jsp:include>

<title>服务列表 - 服务管理</title>
<style type="text/css">
.sContent {
	overflow: hidden;
	text-overflow: ellipsis;
	display: -webkit-box;
	-webkit-box-orient: vertical;
	-webkit-line-clamp: 3;
	height: 56px;
}
</style>

</head>
<body>
	<!-- 导入公共部分header.jsp -->
	<jsp:include page="header.jsp"></jsp:include>

	<!-- 导入公共部分left.jsp -->
	<jsp:include page="left.jsp"></jsp:include>

	<section class="Hui-article-box">
		<nav class="breadcrumb">
			<i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span>
			服务中心 <span class="c-gray en">&gt;</span> 服务列表<a
				class="btn btn-success radius r"
				style="line-height: 1.6em; margin-top: 3px"
				href="javascript:location.replace(location.href);" title="刷新"><i
				class="Hui-iconfont">&#xe68f;</i></a>
		</nav>
		<div class="Hui-article">
			<article class="cl pd-20">
				
				<div class="cl pd-5 bg-1 bk-gray mt-20">
					<span class="l"><a href="javascript:;"
						onclick="member_add('添加服务','${pageContext.request.contextPath}/admin/service-add2.jsp','','510')"
						class="btn btn-primary radius"><i class="Hui-iconfont">&#xe600;</i>
							添加服务</a>
				</div>
				<div class="mt-20">
					<table
						class="table table-border table-bordered table-hover table-bg table-sort">
						<thead>
							<tr class="text-c">
								<th width="25"><input type="checkbox" name="" value=""></th>
								<th width="80">服务编号</th>
								<th width="100">服务类型</th>
								<th width="100">服务内容</th>
								<th width="100">图片路径</th>
								<th width="90">状态</th>
								<th width="90">医生编号</th>
								<th width="100">操作</th>
							</tr>
						</thead>
						<tbody>

							<c:forEach items="${list}" var="list">
								<tr class="text-c">
									<td><input type="checkbox" value="1" name=""></td>
									<td>${list.sId}</td>
									<td>${list.sTitle}</td>
									<td class="sContent">${list.sContent}</td>
									<td>${list.sUrl}</td>
									<td class="td-status" id="status"><c:if test="${list.sStatus==1}">
											<span class="label label-success radius">已启用</span>
										</c:if> <c:if test="${list.sStatus==0}">
											<span class="label label-defaunt radius">已停用</span>
										</c:if></td>
									<td>${list.dId}</td>
									<td class="td-manage"><c:if test="${list.sStatus==1}">
											<a style="text-decoration: none"
												onClick="member_stop(this,'10001')" href="javascript:;"
												title="停用"><i class="Hui-iconfont">&#xe631;</i> </a>
										</c:if> <c:if test="${list.sStatus==0}">
											<a style="text-decoration: none"
												onClick="member_start(this,'10001')" href="javascript:;"
												title="启用"><i class="Hui-iconfont">&#xe6e1;</i> </a>
										</c:if> <a title="编辑" href="javascript:;"
										onclick="member_edit('编辑','${pageContext.request.contextPath}/admin/service-update.jsp','${list.sId}','${list.sTitle}','${list.sContent}','${list.sUrl}','${list.sStatus}','${list.dId}','','510')"
										class="ml-5" style="text-decoration: none"><i
											class="Hui-iconfont">&#xe6df;</i> </a> <a title="删除"
										href="javascript:;" onclick="member_del(this,${list.sId})"
										class="ml-5" style="text-decoration: none"><i
											class="Hui-iconfont">&#xe6e2;</i> </a></td>
								</tr>

							</c:forEach>
						</tbody>
					</table>
				</div>
			</article>
		</div>
	</section>

	<!-- 导入公共部分footer.jsp 这是jquery.min.js、layer.js、H-ui.js、H-ui.admin.page.js -->
	<jsp:include page="footer.jsp"></jsp:include>

	<!--请在下方写此页面业务相关的脚本-->
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/admin/lib/My97DatePicker/4.8/WdatePicker.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/admin/lib/datatables/1.10.0/jquery.dataTables.min.js"></script>
	<script type=text/javascript " src=""${pageContext.request.contextPath}/admin/lib/laypage/1.2/laypage.js"></script>
	<script type="text/javascript">
	
		$(function() {
			$('.table-sort').dataTable({
				"aaSorting" : [ [ 1, "desc" ] ],//默认第几个排序
				"bStateSave" : true,//状态保存
				"aoColumnDefs" : [
				//{"bVisible": false, "aTargets": [ 3 ]} //控制列的隐藏显示
				{
					"orderable" : false,
					//"aTargets" : [ 0, 8, 9 ]
				} // 制定列不参与排序
				]
			});
			$('.table-sort tbody').on('click', 'tr', function() {
				if ($(this).hasClass('selected')) {
					$(this).removeClass('selected');
				} else {
					table.$('tr.selected').removeClass('selected');
					$(this).addClass('selected');
				}
			});
		});
		
		/*服务-添加*/
		function member_add(title, url, w, h) {
			//layer_show(title, url, w, h);
			location.href="${pageContext.request.contextPath}/admin/service-add2.jsp"
			
		}
		/*用户-查看*/
		function member_show(title, url, id, w, h) {
			layer_show(title, url, w, h);
		}
		
		/*服务-停用*/
		function member_stop(obj, id) {
			//获取要变更的那行的sId
			var sId=$(obj).parents("tr").find("td").eq(1).html();
			layer.confirm('确认要停用吗？',{
	            btn: ["确定","取消"] //按钮
	        },function() {
				$(obj).parents("tr").find(".td-manage").prepend('<a style="text-decoration:none" onClick="member_start(this,id)" href="javascript:;" title="启用"><i class="Hui-iconfont">&#xe6e1;</i></a>');
				$(obj).parents("tr").find(".td-status").html('<span class="label label-defaunt radius">已停用</span>');
				$(obj).remove();
				$.ajax({
					type:"get",
					url:"Service.action",
					data:{
						op:"stop",
						sId:sId
					},
					success:function(data){
						//判断，如果信息是已停用，就提示提示信息，加载页面
						if(data=="已停用"){
							layer.msg(data, {
								icon : 5,
								time : 1000
							},function(){
								location.reload();
							});
						}
					}
				});	
				layer.closeAll("dialog");
			});	
		}

		/*服务-启用*/
		function member_start(obj, id) {
			//获取要变更的那行的sId
			var sId = $(obj).parents("tr").find("td").eq(1).html();
			layer.confirm('确认要启用吗？',
					{
	            btn: ["确定","取消"] //按钮
	        },function() {
				$(obj).parents("tr").find(".td-manage").prepend('<a style="text-decoration:none" onClick="member_stop(this,id)" href="javascript:;" title="停用"><i class="Hui-iconfont">&#xe631;</i></a>');
				$(obj).parents("tr").find(".td-status").html('<span class="label label-success radius">已启用</span>');
				$(obj).remove();
				$.ajax({
					type:"get",
					url:"Service.action",
					data:{
						op:"start",
						sId:sId
					},
					success:function(data){
						//判断，如果信息已启用，就提示信息，重载页面
						if(data=="已启用"){
							layer.msg(data,{
								icon:6,
								time:1000
							},function(){
								location.reload();
							});
						}
					}
				});
				layer.closeAll("dialog");
			});
		}
		/*用户-编辑*/
		function member_edit(title, url,sId,sTitle,sContent,sUrl,sStatus,dId, w, h) {
			//layer_show(title, url, w, h);
			location.href="${pageContext.request.contextPath}/Service.action?op=updateService&sId="+sId+"&sTitle="+sTitle+"&sContent="+sContent+"&sUrl="+sUrl+"&sStatus="+sStatus+"&dId="+dId;
			
		}
		/*用户-删除*/
		function member_del(obj, id) {
			layer.confirm('确认要删除吗？', function(index) {
				$(obj).parents("tr").remove();
				layer.msg('已删除!', {
					icon : 1,
					time : 1000
				});
				location.href="${pageContext.request.contextPath}/Service.action?op=deleteService&sId="+id;
			});
			
			
		}
		
	
	</script>
	<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>