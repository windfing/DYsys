<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE HTML>
<html>
<head>
<!-- 引入公共部分meta.jsp -->
<jsp:include page="meta.jsp"></jsp:include>

<title>添加用户 - H-ui.admin v3.0</title>

</head>
<body>
	<article class="cl pd-20">
		<form class="form form-horizontal" id="form-member-add">

			<!-- 用户名 -->
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3"><span
					class="c-red">*</span>用户名：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<input type="text" class="input-text" placeholder="请输入用户名"
						id="uName" name="uName">
				</div>
			</div>

			<!-- 密码 -->
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3"><span
					class="c-red">*</span>密码：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<input type="password" class="input-text" placeholder="请输入密码"
						id="uPassword" name="uPassword">
				</div>
			</div>

			<!-- 性别 -->
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3"><span
					class="c-red">*</span>性别：</label>
				<div class="formControls col-xs-8 col-sm-9 skin-minimal">
					<div class="radio-box">
						<input type="radio" id="uSex-1" name="uSex" value="男" checked>
						<label for="uSex-1">男</label>
					</div>
					<div class="radio-box">
						<input type="radio" id="uSex-2" name="uSex" value="女"> <label
							for="uSex-2">女</label>
					</div>
				</div>
			</div>

			<!-- 手机号 -->
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3"><span
					class="c-red">*</span>手机：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<input type="text" class="input-text" placeholder="请输入手机号码"
						id="uPhone" name="uPhone">
				</div>
			</div>

			<!-- 生日 -->
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3"><span
					class="c-red">*</span>生日：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<input type="date" class="input-text" placeholder="" id="uBirthday"
						name="uBirthday">
				</div>
			</div>


			<!-- <div class="row cl">
			<label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>邮箱：</label>
			<div class="formControls col-xs-8 col-sm-9">
				<input type="text" class="input-text" placeholder="@" name="email" id="email">
			</div>
		</div>
		<div class="row cl">
			<label class="form-label col-xs-4 col-sm-3">附件：</label>
			<div class="formControls col-xs-8 col-sm-9"> <span class="btn-upload form-group">
				<input class="input-text upload-url" type="text" name="uploadfile" id="uploadfile" readonly nullmsg="请添加附件！" style="width:200px">
				<a href="javascript:void();" class="btn btn-primary radius upload-btn"><i class="Hui-iconfont">&#xe642;</i> 浏览文件</a>
				<input type="file" multiple name="file-2" class="input-file">
				</span> </div>
		</div> -->

			<!-- 地址 -->
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3">地址：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<textarea name="uAddress" id="uAddress" cols="" rows=""
						class="textarea" placeholder="请说输入地址..."></textarea>
					<p class="textarea-numberbar">
						<em class="textarea-length">0</em>/50
					</p>
				</div>
			</div>

			<!-- 状态 -->
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3">状态：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<span class="select-box"> <select class="select" size="1"
						name="uStatus" id="uStatus">
							<option value="0">启用</option>
							<option value="1">停用</option>
					</select>
					</span>
				</div>
			</div>
			
			<!-- 提交 -->
			<div class="row cl">
				<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
					<input class="btn btn-primary radius" type="button"
						value="&nbsp;&nbsp;提交&nbsp;&nbsp;" id="submit">
				</div>
			</div>
		</form>
	</article>

	<!-- 导入公共部分footer.jsp 这是jquery.min.js、layer.js、H-ui.js、H-ui.admin.page.js -->
	<jsp:include page="footer.jsp"></jsp:include>

	<!--请在下方写此页面业务相关的脚本-->
	<script type="text/javascript"
		src="lib/My97DatePicker/4.8/WdatePicker.js"></script>
	<script type="text/javascript"
		src="lib/jquery.validation/1.14.0/jquery.validate.js"></script>
	<script type="text/javascript"
		src="lib/jquery.validation/1.14.0/messages_zh.js"></script>
	<script type="text/javascript">
		$(function() {
			/* $('.skin-minimal input').iCheck({
				checkboxClass: 'icheckbox-blue',
				radioClass: 'iradio-blue',
				increaseArea: '20%'
			});
			
			$("#form-member-add").validate({
				rules:{
					username:{
						required:true,
						minlength:2,
						maxlength:16
					},
					sex:{
						required:true,
					},
					mobile:{
						required:true,
						isMobile:true,
					},
					email:{
						required:true,
						email:true,
					},
					uploadfile:{
						required:true,
					},
					
				},
				onkeyup:false,
				focusCleanup:true,
				success:"valid",
				submitHandler:function(form){
					$(form).ajaxSubmit();
					var index = parent.layer.getFrameIndex(window.name);
					parent.$('.btn-refresh').click();
					parent.layer.close(index);
				}
			}); */

			$("#submit")
					.click(
							function() {
								$
										.get(
												"${pageContext.request.contextPath}/users.action?op=add",
												{
													"uName" : $("#uName").val(),
													"uPassword" : $(
															"#uPassword").val(),
													"uSex" : $(
															"input[type='radio']:checked")
															.val(),
													"uPhone" : $("#uPhone")
															.val(),
													"uBirthday" : $(
															"#uBirthday").val(),
													"uAddress" : $("#uAddress")
															.val(),
													"uStatus" : $("#uStatus")
															.val()
												}, function(data, status, xhr) {

													if (data == "添加成功") {
														layer.msg(data, {
															icon : 1,
															time : 1000

														}, function() {
															parent.location
																	.reload();
														});
													}

												});
							});
		});
	</script>
	<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>