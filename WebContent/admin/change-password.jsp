<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE HTML>
<html>
<head>
<!-- 引入公共部分meta.jsp -->
<jsp:include page="meta.jsp"></jsp:include>

<title>修改用户 - H-ui.admin v3.0</title>

</head>
<body>
	<article class="cl pd-20">
		<form class="form form-horizontal" id="form-member-add">

			<!-- 原密码 -->
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3"><span
					class="c-red">*</span>原密码：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<input type="password" class="input-text" placeholder="请输入密码"
						id="uPassword" name="uPassword">
				</div>
			</div>

			<!-- 新密码 -->
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3"><span
					class="c-red">*</span>新密码：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<input type="password" class="input-text" placeholder="请输入密码"
						id="uPassword" name="uPassword">
				</div>
			</div>
			
			<!-- 确认密码 -->
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3"><span
					class="c-red">*</span>确认密码：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<input type="password" class="input-text" placeholder="请输入密码"
						id="uPassword" name="uPassword">
				</div>
			</div>

			
		</form>
	</article>

	<!-- 导入公共部分footer.jsp 这是jquery.min.js、layer.js、H-ui.js、H-ui.admin.page.js -->
	<jsp:include page="footer.jsp"></jsp:include>

	<!--请在下方写此页面业务相关的脚本-->
	<script type="text/javascript"
		src="lib/My97DatePicker/4.8/WdatePicker.js"></script>
	<script type="text/javascript"
		src="lib/jquery.validation/1.14.0/jquery.validate.js"></script>
	<script type="text/javascript"
		src="lib/jquery.validation/1.14.0/messages_zh.js"></script>
	<script type="text/javascript">
		$(function() {
			/* $('.skin-minimal input').iCheck({
				checkboxClass: 'icheckbox-blue',
				radioClass: 'iradio-blue',
				increaseArea: '20%'
			});
			
			$("#form-member-add").validate({
				rules:{
					username:{
						required:true,
						minlength:2,
						maxlength:16
					},
					sex:{
						required:true,
					},
					mobile:{
						required:true,
						isMobile:true,
					},
					email:{
						required:true,
						email:true,
					},
					uploadfile:{
						required:true,
					},
					
				},
				onkeyup:false,
				focusCleanup:true,
				success:"valid",
				submitHandler:function(form){
					$(form).ajaxSubmit();
					var index = parent.layer.getFrameIndex(window.name);
					parent.$('.btn-refresh').click();
					parent.layer.close(index);
				}
			}); */

			$("#submit")
					.click(
							function() {
								$
										.get(
												"${pageContext.request.contextPath}/users.action?op=add",
												{
													"uName" : $("#uName").val(),
													"uPassword" : $(
															"#uPassword").val(),
													"uSex" : $(
															"input[type='radio']:checked")
															.val(),
													"uPhone" : $("#uPhone")
															.val(),
													"uBirthday" : $(
															"#uBirthday").val(),
													"uAddress" : $("#uAddress")
															.val(),
													"uStatus" : $("#uStatus")
															.val()
												}, function(data, status, xhr) {

													if (data == "修改密码成功") {
														layer.msg(data, {
															icon : 1,
															time : 1000

														}, function() {
															parent.location
																	.reload();
														});
													}

												});
							});
		});
	</script>
	<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>