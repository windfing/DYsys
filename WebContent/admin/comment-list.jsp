<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE HTML>
<html>
<head>
<!-- 引入公共部分meta.jsp -->
<jsp:include page="meta.jsp"></jsp:include>

<title>评论列表</title>
<style type="text/css">
 .hiddtext{
 	overflow:hidden; 
 	text-overflow:ellipsis; 
 	display: -webkit-box;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 3;
    height:56px;
 }
 a:link{
 	text-decoration: none;
 }
</style>
</head>
<body>
	<!-- 导入公共部分header.jsp -->
	<jsp:include page="header.jsp"></jsp:include>
	<!-- 导入公共部分left.jsp -->
	<jsp:include page="left.jsp"></jsp:include>
	<section class="Hui-article-box">
		<nav class="breadcrumb">
			<i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span>
			评论管理 <span class="c-gray en">&gt;</span> 评论列表 <a
				class="btn btn-success radius r"
				style="line-height: 1.6em; margin-top: 3px"
				href="javascript:location.replace(location.href);" title="刷新"><i
				class="Hui-iconfont">&#xe68f;</i></a>
		</nav>
		<div class="Hui-article">
			<article class="cl pd-20">
				<!-- <div class="cl pd-5 bg-1 bk-gray mt-20">
					<span class="l"><a href="javascript:;" onclick="datadel()"
						class="btn btn-danger radius"><i class="Hui-iconfont">&#xe6e2;</i>
							批量删除</a> </span>
				</div> -->
				<div class="table-responsive">
					<div class="mt-20">
						<table
							class="table table-border table-bordered table-hover table-bg table-sort table-striped display">
							<thead>
								<tr class="text-c">
									<th width="25"><input type="checkbox" name="" value=""></th>
									<th width="60">ID</th>
									<th width="100">用户名</th>
									<th width="400">评论标题</th>
									<th width="400">评论内容</th>
									<th width="150">评论时间</th>
									<th width="70">操作</th>

								</tr>
							</thead>
							<tbody>

								<c:forEach items="${list}" var="a">
									<!--判断如果医生名字为空的话 则评论显示的是用户的评论信息  -->
									<c:if test="${empty a.dName}">
										<tr class="text-c">
											<td width="25"><input type="checkbox" name="" value=""></td>
											<td >${a.kdId}</td>
											<td>${a.uName}(用户)</td>
											<td>${a.kTitle}</td>
											<td class="text-l hiddtext" ><a href="javascript:;" onclick="member_open(this,'1')" title="详细信息">${a.kdContent}</a></td>
											<td>${a.kdTime}</td>
											<td class="td-manage">
												<a title="删除" href="javascript:;"
												onclick="member_del(this,'1')" class="ml-5"
												style="text-decoration: none"><i class="Hui-iconfont">&#xe6e2;</i></a></td>
										</tr>
									</c:if>

									<!--判断如果用户名字为空的话 则评论显示的是医生的评论信息  -->
									<c:if test="${empty a.uName}">
										<tr class="text-c">
											<td width="25"><input type="checkbox" name="" value=""></td>
											<td>${a.kdId}</td>
											<td>${a.dName}(医生)</td>
											<td>${a.kTitle}</td>
											<td class="text-l hiddtext"><a href="javascript:;" onclick="member_open(this,'1')" title="详细信息">${a.kdContent}</a></td>
											<td>${a.kdTime}</td>
											<td class="td-manage">
												<a title="删除" href="javascript:;"
												onclick="member_del(this,'1')" class="ml-5"
												style="text-decoration: none"><i class="Hui-iconfont">&#xe6e2;</i></a></td>
										</tr>
									</c:if>
								</c:forEach>
							</tbody>


						</table>
					</div>
			</article>
		</div>
	</section>

	<!-- 导入公共部分footer.jsp 这是jquery.min.js、layer.js、H-ui.js、H-ui.admin.page.js -->
	<jsp:include page="footer.jsp"></jsp:include>

	<!--请在下方写此页面业务相关的脚本-->
	<script type="text/javascript" src="${pageContext.request.contextPath}/admin/lib/My97DatePicker/4.8/WdatePicker.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/admin/lib/datatables/1.10.0/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/admin/lib/laypage/1.2/laypage.js"></script>
		
	<script type="text/javascript">
		$(function() {
			//分页
			 var table = $('.table-sort').dataTable({
				"aaSorting" : [ [ 1, "desc" ] ],//默认第几个排序
				"bStateSave" : true,//状态保存
				"aoColumnDefs" : [
				//{"bVisible": false, "aTargets": [ 3 ]} //控制列的隐藏显示
				{
					"orderable" : false,
					"aTargets" : [ 0, 2, 4 ]
				} // 制定列不参与排序
				]
			}); 
		 }); 


		function member_del(obj, id) {
			//获取要删除的那个id
			var kdId = $(obj).parents("tr").find("td").eq(1).html();
			layer.confirm('确认要删除吗？', function() {
				
				$.ajax({
					type:"get",
					url:"KnowledgedisServlet.action",
					data:{
						op : "delkd",
						kdId : kdId
					},
					success:function(data) {
						//如果信息是删除成功，就提示信息，重新加载页面
						if (data == "删除成功") {
							layer.msg(data, {
								icon : 1,
								time : 1000

							}, function() {
								location.reload();
							});
						}
					}
					
				});

			});

		}
		//点击内容弹窗显示所有
		function member_open(obj, id) {
			//获取点击的的那个内容 
			var kContent = $(obj).parents("tr").find("td").eq(4).text();
			layer.open({
				  type: 1,
				  skin: 'layui-layer-rim', //加上边框
				  area: ['620px', '440px'], //宽高
				  content: kContent
				});

		}
	</script>
	<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>