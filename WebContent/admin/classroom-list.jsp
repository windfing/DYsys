<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE HTML>
<html>
<head>
<!-- 引入公共部分meta.jsp -->
<jsp:include page="meta.jsp"></jsp:include>

<title>课堂列表 - 课堂管理 - H-ui.admin v3.0</title>


</head>
<body>
<!-- 导入公共部分header.jsp -->
	<jsp:include page="header.jsp"></jsp:include>

<!-- 导入公共部分left.jsp -->
	<jsp:include page="left.jsp"></jsp:include> 
<%-- 	<%@ include file="left.jsp" %> --%>
<section class="Hui-article-box">
	<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页
		<span class="c-gray en">&gt;</span>
		课堂管理
		<span class="c-gray en">&gt;</span>
		课堂列表
		<a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a>
	</nav>
	<div class="Hui-article">
		<article class="cl pd-20">
			
			<div class="cl pd-5 bg-1 bk-gray mt-20">
				<span class="l">
				<a class="btn btn-primary radius" data-title="添加课堂" _href="classroom-add.jsp" onclick="article_add('添加课堂','classroom-add.jsp')" href="javascript:;"><i class="Hui-iconfont">&#xe600;</i> 添加课堂</a>
				</span>
			</div>
			<div class="mt-20">
				<table id="knowldegeForm" class="table table-border table-bordered table-bg table-hover table-sort">
					<thead>
						<tr class="text-c">
							<th width="80">vId</th>
							<th>标题</th>
							<th width="500">路径</th>
							<th width="120">操作</th>
						</tr>
					</thead>
					
				</table>
			</div>
		</article>
	</div>
</section>

<!-- 导入公共部分footer.jsp 这是jquery.min.js、layer.js、H-ui.js、H-ui.admin.page.js -->
	<jsp:include page="footer.jsp"></jsp:include>


<!--请在下方写此页面业务相关的脚本-->
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/admin/lib/My97DatePicker/4.8/WdatePicker.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/admin/lib/datatables/1.10.0/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/admin/lib/laypage/1.2/laypage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/admin/static/h-ui/js/H-ui.js"></script>


<!-- layui -->
<link rel="stylesheet"href="${pageContext.request.contextPath}/user/layui/css/layui.css"media="all"/>
<script src="${pageContext.request.contextPath}/user/layui/layui.js" charset="utf-8"></script>
<script type="text/javascript">




$(document).ready(function () {
	var table =$("#knowldegeForm").DataTable({
		//ajax :路径  文件路径或者servlet的路径
		 ajax : "${pageContext.request.contextPath}/Video.action?op=showVideo",
		columns : [ {
			data : "vId"
		}, {
			data : "vTitle"
		}, {
			data : "vSrc"
		} ,{
			"defaultContent":"<a style='text-decoration:none' class='ml-5 modify' title='编辑'><i class='Hui-iconfont'>&#xe6df;</i></a> <a style='text-decoration:none' class='ml-5 del'  title='删除'><i class='Hui-iconfont'>&#xe6e2;</i></a>"
		}]
	});
	$('#knowldegeForm').on('click','.del',function() {
		var data = table.row($(this).parents('tr')).data();
		layui.use(['laypage','layer' ],function() {
			var laypage = layui.laypage, layer = layui.layer;
		layer.confirm('是否要删除？',{
			  btn: ['是','否'] //按钮
			}, function(){
				layer.msg('正在执行操作', {icon: 1});
				$.get("${pageContext.request.contextPath}/Video.action?op=deleteVideo","vId="+ data.vId,function(data,status) {
					layer.msg(data.msg,{icon : 1,time : 2000	},
					function() {location.reload();
					});
						});
			}, function(){
			  layer.msg('正在取消操作', {
			    time: 1000, 
			  });
			});
		});
		});
});


/*课堂-添加*/
function article_add(title,url,w,h){
	var index = layer.open({
		type: 2,
		title: title,
		content: url
	});
	layer.full(index);
}


</script>
<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>