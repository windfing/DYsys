<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE HTML>
<html>
<head>
<!-- 引入公共部分meta.jsp -->
<jsp:include page="meta.jsp"></jsp:include>
<%-- <%@include file="meta.jsp" %> --%>
<style type="text/css">
.dian{
overflow:hidden; 
 	text-overflow:ellipsis; 
 	display: -webkit-box;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 3;
    height:56px;
}
</style>
<title>知识列表 - 知识管理 - H-ui.admin v3.0</title>

</head>
<body>
<!-- 导入公共部分header.jsp -->
	<jsp:include page="header.jsp"></jsp:include>
<%-- <%@include file="header.jsp" %> --%>
<!-- 导入公共部分left.jsp -->
	<jsp:include page="left.jsp"></jsp:include>
<%-- <%@ include file="left.jsp" %> --%>
<section class="Hui-article-box">
	<nav class="breadcrumb"><i class="Hui-iconfont">&#xe67f;</i> 首页
		<span class="c-gray en">&gt;</span>
		知识管理
		<span class="c-gray en">&gt;</span>
		知识列表
		<a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px" href="javascript:location.replace(location.href);" title="刷新" ><i class="Hui-iconfont">&#xe68f;</i></a>
	</nav>
	<div class="Hui-article">
		<article class="cl pd-20">
		
			<div class="cl pd-5 bg-1 bk-gray mt-20">
				<span class="l">
				
				<a class="btn btn-primary radius" data-title="添加知识" _href="article-add.jsp" onclick="article_add('添加知识','article-add.jsp')" href="admin/article-add.jsp"><i class="Hui-iconfont">&#xe600;</i> 添加知识</a>
				</span>
			
			</div>
			<div class="mt-20">
				<table id="knowledgeForm"class="table table-border table-bordered table-bg table-hover table-sort">
					<thead>
						<tr class="text-c">
							<th width="25"><input type="checkbox" name="" value=""></th>
							<th width="80">ID</th>
							<th width="120">标题</th>
							<th width="600">内容</th>
							<th width="120">发布时间</th>
							<th width="75">发布人</th>
							<th width="60">发布状态</th>
							<th width="120">操作</th>
						</tr>
						<tbody>
					<c:forEach items="${list}" var="e">
						<tr class="text-c">
							<td><input type="checkbox" value="" name=""></td>
							<td>${e.kId}</td>
							<td class="text-l"><uclass="text-primary">${e.kTitle}</u></td>
							<td class="dian">${e.kContent}</td>
							<td>${e.kTime}</td>
							<td>${e.issuer}</td>
							<td><c:if test="${e.kStatus==1}">隐藏</c:if>
									<c:if test="${e.kStatus==0}">显示</c:if></td>
							<td class="f-14 td-manage"><a style="text-decoration:none" onClick="article_stop(this,'10001')" href="javascript:;" title="下架"><i class="Hui-iconfont">&#xe6de;</i></a>
						</tr>
						</c:forEach>
						</tbody>
					</thead>
					
				</table>
			</div>
		</article>
	</div>
</section>

<!-- 导入公共部分footer.jsp 这是jquery.min.js、layer.js、H-ui.js、H-ui.admin.page.js -->
	<jsp:include page="footer.jsp"></jsp:include>
<%-- <%@ include file="footer.jsp" %> --%>

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="${pageContext.request.contextPath}/admin/lib/My97DatePicker/4.8/WdatePicker.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/admin/lib/datatables/1.10.0/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/admin/lib/laypage/1.2/laypage.js"></script>
<script type="text/javascript">


$('.table-sort').dataTable({
	"aaSorting": [[ 1, "desc" ]],//默认第几个排序
	"bStateSave": true,//状态保存
	"aoColumnDefs": [
		//{"bVisible": false, "aTargets": [ 3 ]} //控制列的隐藏显示
		{"orderable":false,"aTargets":[0,7]}// 不参与排序的列
	]

});

/*知识-添加*/
function article_add(title,url,w,h){
	var index = layer.open({
		type: 2,
		title: title,
		content: url
	});
	layer.full(index);
}
/*知识-编辑*/
function article_edit(title,url,id,w,h){
	var index = layer.open({
		type: 2,
		title: title,
		content: url
	});
	layer.full(index);
}
/*知识-删除*/
// function article_del(obj,id){
// 	layer.confirm('确认要删除吗？',function(index){
// 		$.ajax({
// 			type: 'POST',
// 			url: '',
// 			dataType: 'json',
// 			success: function(data){
// 				$(obj).parents("tr").remove();
// 				layer.msg('已删除!',{icon:1,time:1000});
// 			},
// 			error:function(data) {
// 				console.log(data.msg);
// 			},
// 		});		
// 	});
// }

/*知识-审核*/
// function article_shenhe(obj,id){
// 	layer.confirm('审核文章？', {
// 		btn: ['通过','不通过','取消'], 
// 		shade: false,
// 		closeBtn: 0
// 	},
// 	function(){
// 		$(obj).parents("tr").find(".td-manage").prepend('<a class="c-primary" onClick="article_start(this,id)" href="javascript:;" title="申请上线">申请上线</a>');
// 		$(obj).parents("tr").find(".td-status").html('<span class="label label-success radius">已发布</span>');
// 		$(obj).remove();
// 		layer.msg('已发布', {icon:6,time:1000});
// 	},
// 	function(){
// 		$(obj).parents("tr").find(".td-manage").prepend('<a class="c-primary" onClick="article_shenqing(this,id)" href="javascript:;" title="申请上线">申请上线</a>');
// 		$(obj).parents("tr").find(".td-status").html('<span class="label label-danger radius">未通过</span>');
// 		$(obj).remove();
//     	layer.msg('未通过', {icon:5,time:1000});
// 	});	
// }
/*知识-下架*/
function article_stop(obj,id){
	var kId=$(obj).parents("tr").find("td").eq(1).html();
	//$.trim(kStatus) 去掉获取值的空格
	var kStatus=$.trim($(obj).parents("tr").find("td").eq(6).text());

	 $.get("Knowledge.action?op=deletek",{"kId":kId,"kStatus":kStatus},function(data){
		 layer.msg(data,{icon: 5,time:1000},function(){
			 window.location.reload(); 
		 });
  	   
     });

};

// /*知识-发布*/
// function article_start(obj,id){
// 	layer.confirm('确认要发布吗？',function(index){
// 		$(obj).parents("tr").find(".td-manage").prepend('<a style="text-decoration:none" onClick="article_stop(this,id)" href="javascript:;" title="下架"><i class="Hui-iconfont">&#xe6de;</i></a>');
// 		$(obj).parents("tr").find(".td-status").html('<span class="label label-success radius">已发布</span>');
// 		$(obj).remove();
// 		layer.msg('已发布!',{icon: 6,time:1000});
// 	});
// }
// /*知识-申请上线*/
// function article_shenqing(obj,id){
// 	$(obj).parents("tr").find(".td-status").html('<span class="label label-default radius">待审核</span>');
// 	$(obj).parents("tr").find(".td-manage").html("");
// 	layer.msg('已提交申请，耐心等待审核!', {icon: 1,time:2000});
// }
</script>
<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>