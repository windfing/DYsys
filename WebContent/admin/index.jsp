<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE HTML>
<html>

<head>
<!-- 引入公共部分meta.jsp -->
<jsp:include page="meta.jsp"></jsp:include>

<title>中医养生系统首页</title>

</head>

<body>
	
	<!-- 导入公共部分header.jsp -->
	<jsp:include page="header.jsp"></jsp:include>

	<!-- 导入公共部分left.jsp -->
	<jsp:include page="left.jsp"></jsp:include>

	<section class="Hui-article-box">
		<nav class="breadcrumb">
			<i class="Hui-iconfont"></i> <a href="/" class="maincolor">首页</a> <span
				class="c-999 en">&gt;</span> <span class="c-666">我的桌面</span> <a
				class="btn btn-success radius r"
				style="line-height: 1.6em; margin-top: 3px"
				href="javascript:location.replace(location.href);" title="刷新"><i
				class="Hui-iconfont">&#xe68f;</i></a>
		</nav>
		<div class="Hui-article">
			<article class="cl pd-20">
				<p class="f-20 text-success">
					欢迎使用admin登录
				</p>
<!-- 				<p>登录次数：18</p> -->
<!-- 				<p>上次登录IP：222.35.131.79.1 上次登录时间：2014-6-14 11:19:55</p> -->
				<table class="table table-border table-bordered table-bg">
					<thead>
						<tr>
							<th colspan="7" scope="col">DY队</th>
						</tr>
						<tr class="text-c">
							<th>成员</th>
							<th>性别</th>
							<th>电话</th>
							<th>生日</th>
						</tr>
					</thead>
					<tbody>
						<tr class="text-c">
							<td>黄志鹏</td>
							<td>男</td>
							<td>110</td>
							<td>1996-10-01</td>
						</tr>
						<tr class="text-c">
							<td>蒋丽娟</td>
							<td>女</td>
							<td>119</td>
							<td>1992-05-01</td>
						</tr>
						<tr class="text-c">
							<td>薛佳鑫</td>
							<td>女</td>
							<td>120</td>
							<td>1995-01-01</td>
						</tr>
						<tr class="text-c">
							<td>高文乾</td>
							<td>男</td>
							<td>114</td>
							<td>1994-12-25</td>
						</tr>
						<tr class="text-c">
							<td>黄伟鸿</td>
							<td>男</td>
							<td>10086</td>
							<td>1998-04-01</td>
						</tr>
						<tr class="text-c">
							<td>陈超群</td>
							<td>男</td>
							<td>10000</td>
							<td>1998-05-20</td>
						</tr>
					</tbody>
				</table>
				
			</article>
			<footer class="footer">
				
			</footer>
		</div>
	</section>

	<!-- 导入公共部分footer.jsp 这是jquery.min.js、layer.js、H-ui.js、H-ui.admin.page.js -->
	<jsp:include page="footer.jsp"></jsp:include>

	<!--请在下方写此页面业务相关的脚本-->
	<script type="text/javascript">
	
	</script>
	<!--/请在上方写此页面业务相关的脚本-->

	
</body>

</html>