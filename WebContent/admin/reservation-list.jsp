<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!--_meta 作为公共模版分离出去-->
<!DOCTYPE HTML>
<html>
<head>
<!-- 引入公共部分meta.jsp -->
<jsp:include page="meta.jsp"></jsp:include>

<title>预约列表 - 预约服务管理</title>
<!-- dataTables.min.css -->
<link
	href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"
	rel="stylesheet">
</head>
<body>
	<!-- 导入公共部分header.jsp -->
	<jsp:include page="header.jsp"></jsp:include>

	<!-- 导入公共部分left.jsp -->
		<jsp:include page="left.jsp"></jsp:include> 

	<section class="Hui-article-box">
		<nav class="breadcrumb">
			<i class="Hui-iconfont">&#xe67f;</i> 首页 <span class="c-gray en">&gt;</span>
			预约中心 <span class="c-gray en">&gt;</span> 预约列表<a
				class="btn btn-success radius r"
				style="line-height: 1.6em; margin-top: 3px"
				href="javascript:location.replace(location.href);" title="刷新"><i
				class="Hui-iconfont">&#xe68f;</i></a>
		</nav>
		<div class="Hui-article">
			<article class="cl pd-20">

				<div class="mt-20">
					<table id="show" class="table table-border table-bordered table-bg table-hover table-sort">
						<thead>
							<tr class="text-c">
								<th width="80">用户名</th>
								<th width="100">下单时间</th>
								<th width="100">预约时间</th>
								<th width="90">预约的服务</th>
								<th width="90">医生名</th>
								<th width="70">状态</th>
							</tr>
						</thead>
					</table>
				</div>
			</article>
		</div>
	</section>

	<!-- 导入公共部分footer.jsp 这是jquery.min.js、layer.js、H-ui.js、H-ui.admin.page.js -->
	<jsp:include page="footer.jsp"></jsp:include>

	<!--请在下方写此页面业务相关的脚本-->
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/admin/lib/My97DatePicker/4.8/WdatePicker.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/admin/lib/datatables/1.10.0/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/admin/lib/laypage/1.2/laypage.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/admin/static/h-ui/js/H-ui.js"></script>

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
	<!--/请在上方写此页面业务相关的脚本-->
	<script type="text/javascript">
		$(document).ready(function () {
			var table =$("#show").DataTable({
				//ajax :路径  文件路径或者servlet的路径
				 ajax : "${pageContext.request.contextPath}/Appointment.action",
				columns : [ {
					data : "uName"
				}, {
					data : "aCurrentTime"
				}, {
					data : "aTime"
				} ,{
					data:"sTitle"
				},{
					data:"dName"
				},{
					data:"aStatus"
				}]
			});
	});
	</script>
	
</body>
</html>