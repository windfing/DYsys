<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<!DOCTYPE HTML>
<html>
<head>
<!-- 引入公共部分meta.jsp -->
<jsp:include page="meta.jsp"></jsp:include>

<title>添加服务 - H-ui.admin v3.0</title>

</head>
<body>
	<article class="cl pd-20">
		<form class="form form-horizontal" action="${pageContext.request.contextPath}/Service.action?op=uploadImg" method="post" enctype="multipart/form-data" onsubmit="checkAll()">
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3"><span
					class="c-red">*</span>服务项目：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<input type="text" class="input-text" value="" placeholder=""
						id="sTitle" name="sTitle">
				</div>
			</div>
			
			
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3"><span
					class="c-red">*</span>服务内容：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<textarea name="sContent" id="sContent" cols="" rows="" class="textarea"
						placeholder="说点什么...最少输入10个字符"></textarea>
					<p class="textarea-numberbar">
						<em class="textarea-length">0</em>/100
					</p>
				</div>
			</div>
			
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3"><span
					class="c-red">*</span>图片上传：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<input type="file" class="input-text" value="" placeholder=""
						id="sPhoto" name="sPhoto">
				</div>
			</div>
			
			<div class="row cl">
			    <label class="form-label col-xs-4 col-sm-3"><span
					class="c-red">*</span>状态：</label>
			    <div class="formControls col-xs-8 col-sm-9">			        
					<select name="sStatus" id="sStatus">
					    <option value="1">启动</option>
					    <option value="0">禁用</option>
				    </select>
				</div>
				
			</div>
			
			<!-- <div class="row cl">
				<label class="form-label col-xs-4 col-sm-3"><span
					class="c-red">*</span>状态：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<input type="text" class="input-text" value="" placeholder=""
						id="sStatus" name="sStatus">
				</div>
			</div> -->
			
			
			<div class="row cl">
				<label class="form-label col-xs-4 col-sm-3"><span
					class="c-red">*</span>专家：</label>
				<div class="formControls col-xs-8 col-sm-9">
					<input type="text" class="input-text" placeholder="专家编号" name="dId"
						id="dId">
				</div>
			</div>
			
			<div class="row cl">
				<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
					<input type="submit" class="btn btn-primary radius" id="btnAjax"
						value="&nbsp;&nbsp;提交&nbsp;&nbsp;">
				</div>
			</div>
		</form>
	</article>

	<!-- 导入公共部分footer.jsp 这是jquery.min.js、layer.js、H-ui.js、H-ui.admin.page.js -->
	<jsp:include page="footer.jsp"></jsp:include>

	<!--请在下方写此页面业务相关的脚本-->
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/admin/lib/My97DatePicker/4.8/WdatePicker.js"></script>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/admin/lib/jquery.validation/1.14.0/jquery.validate.js"></script>
	<%-- <script type="text/javascript"
		src="${pageContext.request.contextPath}/admin/lib/jquery.validation/1.14.0/validate-methods.js"></script> --%>
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/admin/lib/jquery.validation/1.14.0/messages_zh.js"></script>
			
	<script type="text/javascript">
		$(function() {
			$('.skin-minimal input').iCheck({
				checkboxClass : 'icheckbox-blue',
				radioClass : 'iradio-blue',
				increaseArea : '20%'
			});

			$("#form-member-add").validate({
				rules : {
					sTitle : { 
						required : true,
						minlength : 2,
						maxlength : 16
					},
					sContent : {
						/* minlength : 10, */
						required : true,
					},
					sStatus : {
						required : true,
					},
					dId : {
						required : true,
					},

				},
				onkeyup : false,
				focusCleanup : true,
				success : "valid",
				submitHandler : function(form) {
					$(form).ajaxSubmit();
					var index = parent.layer.getFrameIndex(window.name);
					parent.$('.btn-refresh').click();
					parent.layer.close(index);
				}
			});
			
		});
		function checkAll(){
			location.href="${pageContext.request.contextPath}/Service.action?op=refresh";
			//parent.location.reload();
			//location.href="${pageContext.request.contextPath}/Service.action?op=refresh";
		}
	</script>
	<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>