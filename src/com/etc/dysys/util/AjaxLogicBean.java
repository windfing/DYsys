package com.etc.dysys.util;

import java.util.List;

public class AjaxLogicBean<T> {
	private List<T> data;

	public List<T> getData() {
		return data;
	}

	public void setData(List<T> data) {
		this.data = data;
	}

	@Override
	public String toString() {
		return "AjaxLogicBean [data=" + data + "]";
	}
	

}
