package com.etc.dysys.entity;

/**
 * 自定义实体类 [服务]
 * 
 * @author 黄伟鸿
 * 
 */

public class Service {

	// 服务的ID
	private int sId;
	// 服务的标题
	private String sTitle;
	// 服务的内容
	private String sContent;
	// 路径
	private String sUrl;
	// 服务的状态
	private int sStatus;
	// 医生的ID
	private int dId;

	public Service() {
		// TODO Auto-generated constructor stub
	}

	public String getsUrl() {
		return sUrl;
	}

	public void setsUrl(String sUrl) {
		this.sUrl = sUrl;
	}

	@Override
	public String toString() {
		return "Service [sId=" + sId + ", sTitle=" + sTitle + ", sContent=" + sContent + ", sUrl=" + sUrl + ", sStatus="
				+ sStatus + ", dId=" + dId + "]";
	}

	public int getsId() {
		return sId;
	}

	public void setsId(int sId) {
		this.sId = sId;
	}

	public String getsTitle() {
		return sTitle;
	}

	public void setsTitle(String sTitle) {
		this.sTitle = sTitle;
	}

	public String getsContent() {
		return sContent;
	}

	public void setsContent(String sContent) {
		this.sContent = sContent;
	}

	public int getsStatus() {
		return sStatus;
	}

	public void setsStatus(int sStatus) {
		this.sStatus = sStatus;
	}

	public int getdId() {
		return dId;
	}

	public void setdId(int dId) {
		this.dId = dId;
	}

	public Service(int sId, String sTitle, String sContent, int sStatus, int dId) {
		super();
		this.sId = sId;
		this.sTitle = sTitle;
		this.sContent = sContent;
		this.sStatus = sStatus;
		this.dId = dId;
	}

	public Service(String sTitle, String sContent, int sStatus, int dId) {
		this.sTitle = sTitle;
		this.sContent = sContent;
		this.sStatus = sStatus;
		this.dId = dId;
	}

	public Service(int sId, String sTitle, String sContent, String sUrl, int sStatus, int dId) {
		this.sId = sId;
		this.sTitle = sTitle;
		this.sContent = sContent;
		this.sUrl = sUrl;
		this.sStatus = sStatus;
		this.dId = dId;
	}

	public Service(String sTitle, String sContent, String sUrl, int sStatus, int dId) {
		this.sTitle = sTitle;
		this.sContent = sContent;
		this.sUrl = sUrl;
		this.sStatus = sStatus;
		this.dId = dId;
	}

}
