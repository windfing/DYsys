package com.etc.dysys.entity;
/**
 * 视图的实体类
 * @author 蒋丽娟
 *
 */
public class KnowledgeView {
	// 知识的ID
		private int kId;
		// 知识的标题
		private String kTitle;
		// 知识的内容
		private String kContent;
		// 知识上传的时间
		private String kTime;
		// 用户的ID
		private String issuer;
		// 知识的状态
		private int kStatus;
		public KnowledgeView(int kId, String kTitle, String kContent, String kTime, String issuer, int kStatus) {
			super();
			this.kId = kId;
			this.kTitle = kTitle;
			this.kContent = kContent;
			this.kTime = kTime;
			this.issuer = issuer;
			this.kStatus = kStatus;
		}
		@Override
		public String toString() {
			return "KnowledgeView [kId=" + kId + ", kTitle=" + kTitle + ", kContent=" + kContent + ", kTime=" + kTime
					+ ", issuer=" + issuer + ", kStatus=" + kStatus + "]";
		}
		public int getkId() {
			return kId;
		}
		public void setkId(int kId) {
			this.kId = kId;
		}
		public String getkTitle() {
			return kTitle;
		}
		public void setkTitle(String kTitle) {
			this.kTitle = kTitle;
		}
		public String getkContent() {
			return kContent;
		}
		public void setkContent(String kContent) {
			this.kContent = kContent;
		}
		public String getkTime() {
			kTime=this.kTime.replace(".0","");
			return kTime;
		}
		public void setkTime(String kTime) {
			
			this.kTime = kTime;
		}
		public String getIssuer() {
			return issuer;
		}
		public void setIssuer(String issuer) {
			this.issuer = issuer;
		}
		public int getkStatus() {
			return kStatus;
		}
		public void setkStatus(int kStatus) {
			this.kStatus = kStatus;
		}
		public KnowledgeView() {
			super();
		}
		

}
