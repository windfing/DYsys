package com.etc.dysys.entity;
/**
 * 预约服务的视图查询实体类
 * @author 黄志鹏
 *
 */
public class AppointmentView {
	//用户Id
 private int uId;
 //用户名
 private String uName;
 //预约下单的时间
 private String aCurrentTime;
 //预约的时间
 private String aTime;
 //服务的状态
 private int aStatus;
 //服务的名称
 private  String sTitle;
 //预约的医生名
 private String  dName;
 //预约的医生Id
 private int dId;
 //预约的服务Id
 private int sId;
 //预约的Id
 private int aId;
 
 

public AppointmentView(int uId, String uName, String aCurrentTime, String aTime, int aStatus, String sTitle,
		String dName, int dId, int sId, int aId) {
	super();
	this.uId = uId;
	this.uName = uName;
	this.aCurrentTime = aCurrentTime;
	this.aTime = aTime;
	this.aStatus = aStatus;
	this.sTitle = sTitle;
	this.dName = dName;
	this.dId = dId;
	this.sId = sId;
	this.aId = aId;
}
@Override
public String toString() {
	return "AppointmentView [uId=" + uId + ", uName=" + uName + ", aCurrentTime=" + aCurrentTime + ", aTime=" + aTime
			+ ", aStatus=" + aStatus + ", sTitle=" + sTitle + ", dName=" + dName + ", dId=" + dId + ", sId=" + sId
			+ ", aId=" + aId + "]";
}
public AppointmentView() {
	// TODO Auto-generated constructor stub
}
 public int getaId() {
	return aId;
}

public void setaId(int aId) {
	this.aId = aId;
}

 

public int getuId() {
	return uId;
}
public void setuId(int uId) {
	this.uId = uId;
}
public String getuName() {
	return uName;
}
public void setuName(String uName) {
	this.uName = uName;
}
public String getaCurrentTime() {
	return aCurrentTime;
}
public void setaCurrentTime(String aCurrentTime) {
	this.aCurrentTime = aCurrentTime;
}
public String getaTime() {
	return aTime;
}
public void setaTime(String aTime) {
	this.aTime = aTime;
}
public int getaStatus() {
	return aStatus;
}
public void setaStatus(int aStatus) {
	this.aStatus = aStatus;
}
public String getsTitle() {
	return sTitle;
}
public void setsTitle(String sTitle) {
	this.sTitle = sTitle;
}
public String getdName() {
	return dName;
}
public void setdName(String dName) {
	this.dName = dName;
}
public int getdId() {
	return dId;
}
public void setdId(int dId) {
	this.dId = dId;
}
public int getsId() {
	return sId;
}
public void setsId(int sId) {
	this.sId = sId;
} 
 
 
}
