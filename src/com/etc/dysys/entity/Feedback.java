package com.etc.dysys.entity;

/**
 * 自定义实体类 [反馈]
 * 
 * @author 黄伟鸿
 * 
 */

public class Feedback {

	// 反馈的ID
	private int fId;
	// 反馈的内容
	private String fContent;
	// 反馈的时间
	private String fCurrentTime;
	// 服务的ID
	private int sId;

	@Override
	public String toString() {
		return "feedback [fId=" + fId + ", fContent=" + fContent + ", fCurrentTime=" + fCurrentTime + ", sId=" + sId
				+ "]";
	}
public Feedback() {
	// TODO Auto-generated constructor stub
}
	public int getfId() {
		return fId;
	}

	public void setfId(int fId) {
		this.fId = fId;
	}

	public String getfContent() {
		return fContent;
	}

	public void setfContent(String fContent) {
		this.fContent = fContent;
	}

	public String getfCurrentTime() {
		return fCurrentTime;
	}

	public void setfCurrentTime(String fCurrentTime) {
		this.fCurrentTime = fCurrentTime;
	}

	public int getsId() {
		return sId;
	}

	public void setsId(int sId) {
		this.sId = sId;
	}

	public Feedback(int fId, String fContent, String fCurrentTime, int sId) {
		super();
		this.fId = fId;
		this.fContent = fContent;
		this.fCurrentTime = fCurrentTime;
		this.sId = sId;
	}

}
