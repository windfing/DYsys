package com.etc.dysys.entity;
/**
 * 知识，知识评论，医生，用户联合实体类
 * @author 薛佳鑫
 *
 */

public class KdisKnowUserDoc {
	
	private int kdId;
	private String kdContent;
	private String kdTime;
	private String kTitle;
	private String uName;
	private String dName;
	public int getKdId() {
		return kdId;
	}
	public void setKdId(int kdId) {
		this.kdId = kdId;
	}
	public String getKdContent() {
		return kdContent;
	}
	public void setKdContent(String kdContent) {
		this.kdContent = kdContent;
	}
	public String getKdTime() {
		return kdTime;
	}
	public void setKdTime(String kdTime) {
		this.kdTime = kdTime;
	}
	public String getkTitle() {
		return kTitle;
	}
	public void setkTitle(String kTitle) {
		this.kTitle = kTitle;
	}
	public String getuName() {
		return uName;
	}
	public void setuName(String uName) {
		this.uName = uName;
	}
	public String getdName() {
		return dName;
	}
	public void setdName(String dName) {
		this.dName = dName;
	}
	public KdisKnowUserDoc(int kdId, String kdContent, String kdTime, String kTitle, String uName, String dName) {
		super();
		this.kdId = kdId;
		this.kdContent = kdContent;
		this.kdTime = kdTime;
		this.kTitle = kTitle;
		this.uName = uName;
		this.dName = dName;
	}
	public KdisKnowUserDoc() {
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "KdisKnowUserDoc [kdId=" + kdId + ", kdContent=" + kdContent + ", kdTime=" + kdTime + ", kTitle="
				+ kTitle + ", uName=" + uName + ", dName=" + dName + "]";
	}
	

}
