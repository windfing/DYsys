package com.etc.dysys.entity;

/**
 * 自定义实体类 [知识评论]
 * 
 * @author 黄伟鸿
 * 
 */

public class Knowledgedis {

	// 知识评论的ID
	private int kdId;
	// 知识评论的内容
	private String kdContent;
	// 知识评论的时间
	private String kdTime;
	// 用户的ID
	private int uId;
	// 医生的ID
	private int dId;
	private int kId;

	public Knowledgedis() {
		// TODO Auto-generated constructor stub
	}

	public int getKdId() {
		return kdId;
	}

	public void setKdId(int kdId) {
		this.kdId = kdId;
	}

	public String getKdContent() {
		return kdContent;
	}

	public void setKdContent(String kdContent) {
		this.kdContent = kdContent;
	}

	public String getKdTime() {
		return kdTime;
	}

	public void setKdTime(String kdTime) {
		this.kdTime = kdTime;
	}

	public int getuId() {
		return uId;
	}

	public void setuId(int uId) {
		this.uId = uId;
	}

	public int getdId() {
		return dId;
	}

	public void setdId(int dId) {
		this.dId = dId;
	}

	public int getkId() {
		return kId;
	}

	public void setkId(int kId) {
		this.kId = kId;
	}

	@Override
	public String toString() {
		return "Knowledgedis [kdId=" + kdId + ", kdContent=" + kdContent + ", kdTime=" + kdTime + ", uId=" + uId
				+ ", dId=" + dId + ", kId=" + kId + "]";
	}

	public Knowledgedis(int kdId, String kdContent, String kdTime, int uId, int dId, int kId) {
		super();
		this.kdId = kdId;
		this.kdContent = kdContent;
		this.kdTime = kdTime;
		this.uId = uId;
		this.dId = dId;
		this.kId = kId;
	}

	public Knowledgedis(String kdContent, int uId, int kId) {
		super();
		this.kdContent = kdContent;
		this.uId = uId;
		this.kId = kId;
	}
	
	
	
	
	

}
