package com.etc.dysys.entity;

/**
 * 自定义实体类 [视频]
 * 
 * @author 黄伟鸿
 * 
 */

public class Video {

	// 视频的ID
	private int vId;
	// 视频的路径s
	private String vSrc;
	// 视频的标题
	private String vTitle;
public Video() {
	// TODO Auto-generated constructor stub
}
	@Override
	public String toString() {
		return "video [vId=" + vId + ", vSrc=" + vSrc + ", vTitle=" + vTitle + "]";
	}

	public int getvId() {
		return vId;
	}

	public void setvId(int vId) {
		this.vId = vId;
	}

	public String getvSrc() {
		return vSrc;
	}

	public void setvSrc(String vSrc) {
		this.vSrc = vSrc;
	}

	public String getvTitle() {
		return vTitle;
	}

	public void setvTitle(String vTitle) {
		this.vTitle = vTitle;
	}

	public Video(int vId, String vSrc, String vTitle) {
		super();
		this.vId = vId;
		this.vSrc = vSrc;
		this.vTitle = vTitle;
	}

}
