package com.etc.dysys.entity;

public class KnowledegeIndex {
	// 知识的ID
		private int kId;
		// 知识的标题
		private String kTitle;
		// 知识的内容
		private String kContent;
		public KnowledegeIndex() {
			// TODO Auto-generated constructor stub
		}
		public KnowledegeIndex(int kId, String kTitle, String kContent, String kTime) {
			super();
			this.kId = kId;
			this.kTitle = kTitle;
			this.kContent = kContent;
			this.kTime = kTime;
		}
		@Override
		public String toString() {
			return "KnowledegeIndex [kId=" + kId + ", kTitle=" + kTitle + ", kContent=" + kContent + ", kTime=" + kTime
					+ "]";
		}
		public int getkId() {
			return kId;
		}
		public void setkId(int kId) {
			this.kId = kId;
		}
		public String getkTitle() {
			return kTitle;
		}
		public void setkTitle(String kTitle) {
			this.kTitle = kTitle;
		}
		public String getkContent() {
			return kContent;
		}
		public void setkContent(String kContent) {
			this.kContent = kContent;
		}
		public String getkTime() {
			kTime=this.kTime.replace(".0","");
			return kTime;
		}
		public void setkTime(String kTime) {
			this.kTime = kTime;
		}
		// 知识上传的时间
		private String kTime;

}
