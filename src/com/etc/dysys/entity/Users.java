package com.etc.dysys.entity;

/**
 * 自定义实体类 [用户]
 * 
 * @author 黄伟鸿
 * 
 */

public class Users {

	// 用户的ID
	private int uId;
	// 用户名
	private String uName;
	// 用户的密码
	private String uPassword;
	// 用户的性别
	private String uSex;
	// 用户的手机号码
	private String uPhone;
	// 用户的生日
	private String uBirthday;
	// 用户的地址
	private String uAddress;
	// 用户的状态
	private int uStatus;

	public Users() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Users [uId=" + uId + ", uName=" + uName + ", uPassword=" + uPassword + ", uSex=" + uSex + ", uPhone="
				+ uPhone + ", uBirthday=" + uBirthday + ", uAddress=" + uAddress + ", uStatus=" + uStatus + "]";
	}

	public int getuId() {
		return uId;
	}

	public void setuId(int uId) {
		this.uId = uId;
	}

	public String getuName() {
		return uName;
	}

	public void setuName(String uName) {
		this.uName = uName;
	}

	public String getuPassword() {
		return uPassword;
	}

	public void setuPassword(String uPassword) {
		this.uPassword = uPassword;
	}

	public String getuSex() {
		return uSex;
	}

	public void setuSex(String uSex) {
		this.uSex = uSex;
	}

	public String getuPhone() {
		return uPhone;
	}

	public void setuPhone(String uPhone) {
		this.uPhone = uPhone;
	}

	public String getuBirthday() {
		return uBirthday;
	}

	public void setuBirthday(String uBirthday) {
		this.uBirthday = uBirthday;
	}

	public String getuAddress() {
		return uAddress;
	}

	public void setuAddress(String uAddress) {
		this.uAddress = uAddress;
	}

	public int getuStatus() {
		return uStatus;
	}

	public void setuStatus(int uStatus) {
		this.uStatus = uStatus;
	}

	public Users(int uId, String uName, String uPassword, String uSex, String uPhone, String uBirthday, String uAddress,
			int uStatus) {
		super();
		this.uId = uId;
		this.uName = uName;
		this.uPassword = uPassword;
		this.uSex = uSex;
		this.uPhone = uPhone;
		this.uBirthday = uBirthday;
		this.uAddress = uAddress;
		this.uStatus = uStatus;
	}

	public Users(String uName, String uPassword, String uSex, String uPhone, String uBirthday, String uAddress,
			int uStatus) {
		super();
		this.uName = uName;
		this.uPassword = uPassword;
		this.uSex = uSex;
		this.uPhone = uPhone;
		this.uBirthday = uBirthday;
		this.uAddress = uAddress;
		this.uStatus = uStatus;
	}

	public Users(String uSex, String uPhone, String uBirthday, String uAddress) {
		super();
		this.uSex = uSex;
		this.uPhone = uPhone;
		this.uBirthday = uBirthday;
		this.uAddress = uAddress;

	}

}
