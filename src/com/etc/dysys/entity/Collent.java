package com.etc.dysys.entity;

/**
 * 自定义实体类 [销量]
 * 
 * @author 黄伟鸿
 *
 */

public class Collent {

	// 销量的ID
	private int cId;
	// 知识的ID
	private int kId;

	@Override
	public String toString() {
		return "Collent [cId=" + cId + ", kId=" + kId + "]";
	}

	public int getCId() {
		return cId;
	}

	public void setCid(int cId) {
		this.cId = cId;
	}

	public int getKId() {
		return kId;
	}

	public void setKId(int kId) {
		this.kId = kId;
	}

	public Collent(int cId, int kId) {
		super();
		this.cId = cId;
		this.kId = kId;
	}

	public Collent() {
		// TODO Auto-generated constructor stub
	}
}
