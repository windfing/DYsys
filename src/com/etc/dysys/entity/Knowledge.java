package com.etc.dysys.entity;

/**
 * 自定义实体类 [知识]
 * 
 * @author 黄伟鸿
 * 
 */

public class Knowledge {

	// 知识的ID
	private int kId;
	// 知识的标题
	private String kTitle;
	// 知识的内容
	private String kContent;
	// 知识上传的时间
	private String kTime;
	// 用户的ID
	private int uId;
	// 医生的ID
	private int dId;
	// 知识的状态
	private int kStatus;

	@Override
	public String toString() {
		return "knowledge [kId=" + kId + ", kTitle=" + kTitle + ", kContent=" + kContent + ", kTime=" + kTime + ", uId="
				+ uId + ", dId=" + dId + ", kStatus=" + kStatus + "]";
	}
	
	public int getkId() {
		return kId;
	}
	public void setkId(int kId) {
		this.kId = kId;
	}
	public String getkTitle() {
		return kTitle;
	}
	public void setkTitle(String kTitle) {
		this.kTitle = kTitle;
	}
	public String getkContent() {
		return kContent;
	}
	public void setkContent(String kContent) {
		this.kContent = kContent;
	}
	public String getkTime() {
		kTime=this.kTime.replace(".0","");
		return kTime;
	}
	public void setkTime(String kTime) {
		
		this.kTime = kTime;
	}
	public int getuId() {
		return uId;
	}
	public void setuId(int uId) {
		this.uId = uId;
	}
	public int getdId() {
		return dId;
	}
	public void setdId(int dId) {
		this.dId = dId;
	}
	public int getkStatus() {
		return kStatus;
	}
	public void setkStatus(int kStatus) {
		this.kStatus = kStatus;
	}
	public Knowledge() {
		// TODO Auto-generated constructor stub
	}
	public Knowledge(int kId, String kTitle, String kContent, String kTime, int uId, int dId, int kStatus) {
		super();
		this.kId = kId;
		this.kTitle = kTitle;
		this.kContent = kContent;
		this.kTime = kTime;
		this.uId = uId;
		this.dId = dId;
		this.kStatus = kStatus;
	}

	
}
