package com.etc.dysys.entity;

/**
 * 自定义实体类 [医生]
 * 
 * @author 黄伟鸿
 * 
 */

public class Doctor {
	// 医生的ID
	private int dId;
	// 医生名
	private String dName;
	// 医生的密码
	private String dPassword;
	// 医生的性别
	private String dSex;
	// 医生的生日
	private String dBirthday;
	// 医生的介绍
	private String dDesc;
	// 医生图片路径
	private String dUrl;
	// 医生的状态
	private int dStatus;


	@Override
	public String toString() {
		return "Doctor [dId=" + dId + ", dName=" + dName + ", dPassword=" + dPassword + ", dSex=" + dSex
				+ ", dBirthday=" + dBirthday + ", dDesc=" + dDesc + ", dUrl=" + dUrl + ", dStatus=" + dStatus + "]";
	}

	public int getdId() {
		return dId;
	}

	public void setdId(int dId) {
		this.dId = dId;
	}

	public String getdName() {
		return dName;
	}

	public void setdName(String dName) {
		this.dName = dName;
	}

	public String getdPassword() {
		return dPassword;
	}

	public void setdPassword(String dPassword) {
		this.dPassword = dPassword;
	}

	public String getdSex() {
		return dSex;
	}

	public void setdSex(String dSex) {
		this.dSex = dSex;
	}

	public String getdBirthday() {
		return dBirthday;
	}

	public void setdBirthday(String dBirthday) {
		this.dBirthday = dBirthday;
	}

	public String getdDesc() {
		return dDesc;
	}

	public void setdDesc(String dDesc) {
		this.dDesc = dDesc;
	}
	

	public String getdUrl() {
		return dUrl;
	}


	public void setdUrl(String dUrl) {
		this.dUrl = dUrl;
	}

	public int getdStatus() {
		return dStatus;
	}

	public void setdStatus(int dStatus) {
		this.dStatus = dStatus;
	}

	

	/**
	 * @param dId
	 * @param dName
	 * @param dPassword
	 * @param dSex
	 * @param dBirthday
	 * @param dDesc
	 * @param dUrl
	 * @param dStatus
	 */
	public Doctor(int dId, String dName, String dPassword, String dSex, String dBirthday, String dDesc, String dUrl,
			int dStatus) {
		super();
		this.dId = dId;
		this.dName = dName;
		this.dPassword = dPassword;
		this.dSex = dSex;
		this.dBirthday = dBirthday;
		this.dDesc = dDesc;
		this.dUrl = dUrl;
		this.dStatus = dStatus;
	}

	

	/**
	 * @param dName
	 * @param dPassword
	 * @param dSex
	 * @param dBirthday
	 * @param dDesc
	 * @param dUrl
	 * @param dStatus
	 */
	public Doctor(String dName, String dPassword, String dSex, String dBirthday, String dDesc, String dUrl,
			int dStatus) {
		super();
		this.dName = dName;
		this.dPassword = dPassword;
		this.dSex = dSex;
		this.dBirthday = dBirthday;
		this.dDesc = dDesc;
		this.dUrl = dUrl;
		this.dStatus = dStatus;
	}


	/**
	 * @param dPassword
	 * @param dSex
	 * @param dBirthday
	 * @param dDesc
	 * @param dUrl
	 * @param dStatus
	 */
	public Doctor(String dPassword, String dSex, String dBirthday, String dDesc, String dUrl, int dStatus) {
		super();
		this.dPassword = dPassword;
		this.dSex = dSex;
		this.dBirthday = dBirthday;
		this.dDesc = dDesc;
		this.dUrl = dUrl;
		this.dStatus = dStatus;
	}

	public Doctor() {
		// TODO Auto-generated constructor stub
	}
	

	
	
	
	
}
