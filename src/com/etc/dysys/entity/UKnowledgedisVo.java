package com.etc.dysys.entity;
/**
 * 前台评论展示的实体
 * @author 薛佳鑫
 *
 */ 
public class UKnowledgedisVo {
	private int kId;
	private String kdContent;
	private String kdTime;
	private String uName;
	private String dName;
	
	   
	
	public int getkId() {
		return kId;
	}


	public void setkId(int kId) {
		this.kId = kId;
	}


	public String getKdContent() {
		return kdContent;
	}


	public void setKdContent(String kdContent) {
		this.kdContent = kdContent;
	}


	public String getKdTime() {
		return kdTime;
	}


	public void setKdTime(String kdTime) {
		this.kdTime = kdTime;
	}


	public String getuName() {
		return uName;
	}


	public void setuName(String uName) {
		this.uName = uName;
	}


	public String getdName() {
		return dName;
	}


	public void setdName(String dName) {
		this.dName = dName;
	}


	


	@Override
	public String toString() {
		return "UKnowledgedisVo [kId=" + kId + ", kdContent=" + kdContent + ", kdTime=" + kdTime + ", uName=" + uName
				+ ", dName=" + dName + "]";
	}


	public UKnowledgedisVo() {
		// TODO Auto-generated constructor stub
	}

}
