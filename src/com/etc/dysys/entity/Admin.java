/**
 * 
 */
package com.etc.dysys.entity;

/**
 * @author ccq
 *
 * date: 2019年4月2日  上午9:22:36
 */
public class Admin {
	private int adminId;
	private String adminName;
	private String adminPwd;
	
	@Override
	public String toString() {
		return "Admin [adminId=" + adminId + ", adminName=" + adminName + ", adminPwd=" + adminPwd + "]";
	}
	

	public int getAdminId() {
		return adminId;
	}


	public void setAdminId(int adminId) {
		this.adminId = adminId;
	}


	public String getAdminName() {
		return adminName;
	}


	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}


	public String getAdminPwd() {
		return adminPwd;
	}


	public void setAdminPwd(String adminPwd) {
		this.adminPwd = adminPwd;
	}


	/**
	 * @param adminId
	 * @param adminName
	 * @param adminPwd
	 */
	public Admin(int adminId, String adminName, String adminPwd) {
		super();
		this.adminId = adminId;
		this.adminName = adminName;
		this.adminPwd = adminPwd;
	}

	/**
	 * @param adminName
	 * @param adminPwd
	 */
	public Admin(String adminName, String adminPwd) {
		super();
		this.adminName = adminName;
		this.adminPwd = adminPwd;
	}
	
	public Admin() {
		// TODO Auto-generated constructor stub
	}

}
