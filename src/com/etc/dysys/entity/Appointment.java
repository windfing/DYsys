package com.etc.dysys.entity;

/**
 * 自定义实体类 [预约]
 * 
 * @author 黄伟鸿
 *
 */

public class Appointment {

	// 预约ID
	private int aId;
	// 预约时的当前时间
	private String aCurrentTime;
	// 预约养生的时间
	private String aTime;
	// 用户的ID
	private int uId;
	private int dId;
	// 服务的ID
	private int sId;
	// 服务的状态
	private int aStatus;

	@Override
	public String toString() {
		return "Appointment [aId=" + aId + ", aCurrentTime=" + aCurrentTime + ", aTime=" + aTime + ", uId=" + uId
				+ ", sId=" + sId + ", aStatus=" + aStatus + "]";
	}

	public int getdId() {
		return dId;
	}

	public void setdId(int dId) {
		this.dId = dId;
	}

	public int getaStatus() {
		return aStatus;
	}

	public void setaStatus(int aStatus) {
		this.aStatus = aStatus;
	}

	public int getaId() {
		return aId;
	}

	public void setaId(int aId) {
		this.aId = aId;
	}

	public String getaCurrentTime() {
		return aCurrentTime;
	}

	public void setaCurrentTime(String aCurrentTime) {
		this.aCurrentTime = aCurrentTime;
	}

	public String getaTime() {
		return aTime;
	}

	public void setaTime(String aTime) {
		this.aTime = aTime;
	}

	public int getuId() {
		return uId;
	}

	public void setuId(int uId) {
		this.uId = uId;
	}

	public int getsId() {
		return sId;
	}

	public void setsId(int sId) {
		this.sId = sId;
	}

	public Appointment() {
		// TODO Auto-generated constructor stub
	}

	public Appointment(int aId, String aCurrentTime, String aTime, int uId, int sId, int aStatus) {
		super();
		this.aId = aId;
		this.aCurrentTime = aCurrentTime;
		this.aTime = aTime;
		this.uId = uId;
		this.sId = sId;
		this.aStatus = aStatus;
	}

	public Appointment(String aCurrentTime, String aTime, int uId, int sId, int aStatus) {
		this.aCurrentTime = aCurrentTime;
		this.aTime = aTime;
		this.uId = uId;
		this.sId = sId;
		this.aStatus = aStatus;
	}

	public Appointment(String aCurrentTime, String aTime, int uId, int dId, int sId, int aStatus) {
		this.aCurrentTime = aCurrentTime;
		this.aTime = aTime;
		this.uId = uId;
		this.dId = dId;
		this.sId = sId;
		this.aStatus = aStatus;
	}

}
