package com.etc.dysys.admin.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jdt.internal.compiler.lookup.UnresolvedAnnotationBinding;

import com.etc.dysys.admin.service.UsersService;
import com.etc.dysys.admin.service.impl.UsersServiceImpl;
import com.etc.dysys.entity.Doctor;
import com.etc.dysys.entity.Users;

/**
 * Servlet implementation class UsersServlet
 */
@WebServlet("/users.action")
public class UsersServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private UsersService us = new UsersServiceImpl();

	/**
	 * Default constructor.
	 */
	public UsersServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		String op = "query";

		if (request.getParameter("op") != null) {
			op = request.getParameter("op");
		}

		if (op.equals("query")) {
			doQuery(request, response);
		} else if (op.equals("del")) {
			doDelete(request, response);
		} else if (op.equals("stop")) {
			doStop(request, response);
		} else if (op.equals("start")) {
			doStart(request, response);
		} else if (op.equals("add")) {
			doAdd(request, response);
		} else if (op.equals("sel")) {
			doSelect(request, response);
		} else if (op.equals("update")) {
			doUpdate(request, response);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

	/**
	 * 查询所有用户
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doQuery(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 获取集合（Users）
		List<Users> list = us.getUsers();
		request.setAttribute("list", list);
		request.getRequestDispatcher("admin/member-list.jsp").forward(request, response);

	}

	/**
	 * 根据ID删除用户
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doDelete(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		int uId = Integer.parseInt(request.getParameter("uId"));
		boolean flag = us.delUsersById(uId);
		PrintWriter out = response.getWriter();
		String msg = "删除失败";
		if (flag) {
			out.print("删除成功");
		}
		out.close();
	}

	/**
	 * 用户的停用
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doStop(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		int uId = Integer.parseInt(request.getParameter("uId"));
		boolean flag = us.stopUsersById(uId);
		PrintWriter out = response.getWriter();
		String msg = "已启用";
		if (flag) {
			out.print("已停用");
		}
		out.close();
	}

	/**
	 * 用户的启用
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doStart(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		int uId = Integer.parseInt(request.getParameter("uId"));
		boolean flag = us.startUsersById(uId);
		PrintWriter out = response.getWriter();
		String msg = "已停用";
		if (flag) {
			out.print("已启用");
		}
		out.close();
	}

	/**
	 * 用户的增加
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doAdd(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String uName = request.getParameter("uName");
		String uPassword = request.getParameter("uPassword");
		String uSex = request.getParameter("uSex");
		String uPhone = request.getParameter("uPhone");
		String uBirthday = request.getParameter("uBirthday");
		String uAddress = request.getParameter("uAddress");
		int uStatus = Integer.parseInt(request.getParameter("uStatus"));

		Users user = new Users(uName, uPassword, uSex, uPhone, uBirthday, uAddress, uStatus);

		boolean flag = us.addUsers(user);

		String msg = "添加成功";
		if (!flag) {
			msg = "添加失败";
		}

		PrintWriter out = response.getWriter();

		out.print(msg);
		out.close();

	}

	/**
	 * 根据ID查找用户
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doSelect(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		int uId = Integer.parseInt(request.getParameter("uId"));
		Users user = us.selUsersById(uId);
		request.setAttribute("user", user);
		request.getRequestDispatcher("admin/member-update.jsp").forward(request, response);

	}

	/**
	 * 用户的修改
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doUpdate(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String uName = request.getParameter("uName");
		String uSex = request.getParameter("uSex");
		String uPhone = request.getParameter("uPhone");
		String uBirthday = request.getParameter("uBirthday");
		String uAddress = request.getParameter("uAddress");
		// 创建一个user对象
		Users user = new Users(uSex, uPhone, uBirthday, uAddress);
		boolean flag = us.updateUsersByName(user, uName);
		String msg = "修改成功";
		if (!flag) {
			msg = "修改失败";
		}

		PrintWriter out = response.getWriter();

		out.print(msg);
		out.close();
	}

}
