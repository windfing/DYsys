package com.etc.dysys.admin.controller;

/**
 * 蒋丽娟
 */
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.etc.dysys.admin.service.KnowledgeService;
import com.etc.dysys.admin.service.impl.KnowledgeServiceImpl;
import com.etc.dysys.entity.KnowledgeView;

/**
 * Servlet implementation class KnowledgeServlet
 */
@WebServlet("/Knowledge.action")
public class KnowledgeServlet extends HttpServlet {
	KnowledgeService ks = new KnowledgeServiceImpl();
	private static final long serialVersionUID = 1L;

       
  
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
	String op="";
if(request.getParameter("op")!=null) {
	op=request.getParameter("op");
}
if("knowledgePage".equals(op)) {
	doShow(request,response);
}else if("addK".equals(op)) {
	doAddk(request,response);
}else if("deletek".equals(op)) {
	doDeletek(request,response);
}
		
	}
	protected void doDeletek(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int kId=Integer.parseInt(request.getParameter("kId"));
		
	String Status=request.getParameter("kStatus");
		PrintWriter out=response.getWriter();
		if("显示".equals(Status)) {
		int	kStatus=1;
			boolean flag=ks.deleteKnowledge(kStatus, kId);
			if(flag) {
				out.print("下架成功");
			}else {	
			out.print("下架失败");
			}
		}else {
			int kStatus=0;
			boolean flag=ks.deleteKnowledge(kStatus, kId);
			if(flag) {
				out.print("上架成功");
			}else {
			out.print("上架失败");
			}
		}
		
		
		
		out.close();
}
	protected void doAddk(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String kTitle=request.getParameter("title");
		String kContent=request.getParameter("content");
		boolean flag=ks.addKnowledge(kTitle, kContent);
		PrintWriter out=response.getWriter();
		if(flag) {
			
			out.print("增加成功");
		}else {
			
		out.print("增加失败");
		}
		out.close();
	}
	protected void doShow(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			List<KnowledgeView> list=ks.allKnowledge();
			request.setAttribute("list",list);
			request.getRequestDispatcher("admin/article-list.jsp").forward(request, response);
		}


	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
