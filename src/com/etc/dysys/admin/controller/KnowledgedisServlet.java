package com.etc.dysys.admin.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.etc.dysys.admin.service.KnowledgedisService;
import com.etc.dysys.admin.service.impl.KnowledgedisServiceImpl;
import com.etc.dysys.entity.KdisKnowUserDoc;
import com.etc.dysys.util.AjaxLogicBean;
import com.google.gson.Gson;

/**
 * Servlet implementation class KnowledgedisServlet
 */

/**
 * 知识评论的servlet
 * @author 薛佳鑫
 *
 */
@WebServlet("/KnowledgedisServlet.action")
public class KnowledgedisServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private KnowledgedisService kds=new KnowledgedisServiceImpl();
    /**
     * @see HttpServlet#HttpServlet()
     */
    public KnowledgedisServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		/*response.getWriter().append("Served at: ").append(request.getContextPath());*/
		doPost(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		String op="querykd";
		if (request.getParameter("op") != null) {
			op = request.getParameter("op");
		}
		
		if(op.equals("querykd")) {
			
			doQuery(request, response);
		}else if(op.equals("delkd")) {
			doDel(request, response);
		}
		
		
		
	}
	protected void doQuery(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		AjaxLogicBean<KdisKnowUserDoc> alb=kds.getKnowledgedis();
		List<KdisKnowUserDoc> list=alb.getData();
		request.setAttribute("list", list);
		request.getRequestDispatcher("admin/comment-list.jsp").forward(request, response);
		
	}
	
	protected void doDel(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int  kdId=Integer.parseInt(request.getParameter("kdId"));
		boolean flag=kds.delKnowledgedisById(kdId);
		PrintWriter out=response.getWriter();
		String msg="删除失败";
		if(flag) {
			out.print("删除成功");
		}
		out.close();
		
		
	}

}
