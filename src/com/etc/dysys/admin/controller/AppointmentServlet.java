package com.etc.dysys.admin.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.etc.dysys.admin.service.AppointmentService;
import com.etc.dysys.admin.service.impl.AppointmentServiceImpl;
import com.etc.dysys.entity.AppointmentView;
import com.etc.dysys.util.AjaxLogicBean;
import com.google.gson.Gson;

/**
 * 预约统计的后台数据显示
 * Servlet implementation class AppointmentServlet
 */
@WebServlet("/Appointment.action")
public class AppointmentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
     private AppointmentService appointmentService = new AppointmentServiceImpl();
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setCharacterEncoding("utf-8");
		List<AppointmentView> list = appointmentService.findAllAppointment();
		AjaxLogicBean<AppointmentView> pd = new AjaxLogicBean<>();
		pd.setData(list);
		response.setContentType("application/json");
		Gson gson = new Gson();
		String pdStr= gson.toJson(pd);
		PrintWriter out = response.getWriter();
		out.print(pdStr);
		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
