package com.etc.dysys.admin.dao;

import java.util.List;

import com.etc.dysys.entity.Video;

/**
 * 后台视频操作的DAo接口
 * 
 * @author 黄志鹏
 *
 */
public interface VideoDao {
	/**
	 * 保存视频信息的方法
	 * 
	 * @param video
	 * @return
	 */
	public int saveVideo(Video video);
	/**
	 * 查询所有视频信息的方法
	 * @return
	 */
	public List<Video> getAll();
	/**
	 * 根据视频Id删除视频的方法
	 * @param vId
	 * @return
	 */
	public int deleteVideo(int vId);

}
