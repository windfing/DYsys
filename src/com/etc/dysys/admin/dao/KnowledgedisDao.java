package com.etc.dysys.admin.dao;

import java.util.List;

import com.etc.dysys.entity.KdisKnowUserDoc;
/**
 * 知识评论DAO
 * @author 薛佳鑫
 *
 */
public interface KnowledgedisDao {
	/**
	 * 查询所有的知识评论
	 * @return 评论集合
	 */
	
	public List<KdisKnowUserDoc> getKnowledgedis();
	
	/**
	 * 根据id删除评论
	 * @param kdId  知识评论编号
	 * @return  布尔值
	 */
	public boolean delKnowledgedisById(int kdId);
}
