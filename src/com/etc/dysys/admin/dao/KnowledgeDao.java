package com.etc.dysys.admin.dao;

import java.util.List;

import com.etc.dysys.entity.Knowledge;
import com.etc.dysys.entity.KnowledgeView;
import com.etc.dysys.util.PageData;
/**
 * 
 * @author 蒋丽娟
 *
 */
public interface KnowledgeDao {
	
	public List<KnowledgeView> allKnowledge();
	public boolean addKnowledge(String kTitle,String  kContent);
	public boolean deleteKnowledge(int kStatus,int kId);

}
