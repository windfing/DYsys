/**
 * 
 */
package com.etc.dysys.admin.dao;

import java.util.List;

import com.etc.dysys.entity.Doctor;

/**
 * @author ccq
 *
 * date: 2019年3月28日  上午10:30:58
 */
public interface DoctorDao {
	/**
	 * 查找所有医生
	 * @return
	 */
	public List<Doctor> getAllDoctor();
	/**
	 * 增加医生
	 * @param doctor
	 * @return
	 */
	public boolean addDoctor(Doctor doctor);
	/**
	 * 移除医生
	 * @param doctor
	 * @return
	 */
	public boolean deleteDoctor(int dId);
	/**
	 * 修改医生状态
	 * @param dId
	 * @return
	 */
	public boolean updateDoctorStatus(int dStatus,int dId);
	/**
	 * 根据医生姓名dName，修改医生信息
	 * @param doctor
	 * @return
	 */
	public boolean updateDoctorByName(Doctor doctor,String dName);
	/**
	 * 查找单个医生
	 * @param dId
	 * @return
	 */
	public Doctor findDoctorById(int dId);
	

}
