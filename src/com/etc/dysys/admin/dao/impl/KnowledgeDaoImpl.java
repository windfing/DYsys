package com.etc.dysys.admin.dao.impl;

import java.util.List;

import com.etc.dysys.admin.dao.KnowledgeDao;
import com.etc.dysys.entity.Knowledge;
import com.etc.dysys.entity.KnowledgeView;
import com.etc.dysys.util.DBUtil;
import com.etc.dysys.util.PageData;
/**
 * 
 * @author 蒋丽娟
 *
 */
public class KnowledgeDaoImpl implements KnowledgeDao {

	@Override
	public List<KnowledgeView> allKnowledge() {
		// TODO Auto-generated method stub
		String sql="SELECT  * from knowledgeview";
		List< KnowledgeView> pd=(List<KnowledgeView>) DBUtil.select(sql, KnowledgeView.class);
		return pd;
	}

	@Override
	public boolean addKnowledge(String kTitle,String  kContent) {
		// TODO Auto-generated method stub
		String sql="INSERT INTO knowledge (kTitle, kContent, kTime, kStatus) VALUES (?,?,now(), 0)";
		int i=DBUtil.execute(sql, kTitle,kContent);
		return i>0;
	}

	@Override
	public boolean deleteKnowledge(int kStatus,int kId) {
		// TODO Auto-generated method stub
		String sql="update knowledge set kStatus=? where kId=?";
		int i=DBUtil.execute(sql, kStatus,kId);
		return i>0;
	}

}
