/**
 * 
 */
package com.etc.dysys.admin.dao.impl;

import java.util.List;

import com.etc.dysys.admin.dao.DoctorDao;
import com.etc.dysys.entity.Doctor;
import com.etc.dysys.util.DBUtil;

/**
 * @author ccq
 *
 * date: 2019年3月28日  上午10:33:16
 */
public class DoctorDaoImpl implements DoctorDao{
    
	/**
	 * 查找所有医生
	 */
	@Override
	public List<Doctor> getAllDoctor() {
		String sql = "select * from doctor";
		List<Doctor> list = (List<Doctor>) DBUtil.select(sql, Doctor.class);
		return list;
	}

	/**
	 * 增加医生
	 */
	@Override
	public boolean addDoctor(Doctor doctor) {
		String sql = "insert into doctor(dName,dPassword,dSex,dBirthday,dDesc,dUrl,dStatus) values(?,?,?,?,?,?,?)";
		int n = DBUtil.execute(sql, doctor.getdName(),doctor.getdPassword(),doctor.getdSex(),doctor.getdBirthday(),doctor.getdDesc(),doctor.getdUrl(),doctor.getdStatus());
		return n>0;
	}

	/**
	 * 移除医生
	 */
	@Override
	public boolean deleteDoctor(int dId) {
		// TODO Auto-generated method stub
		String sql = "delete from doctor where dId=?";
		int n = DBUtil.execute(sql, dId);
		return n>0;
	}
	/**
	 * 修改医生状态
	 */
	@Override
	public boolean updateDoctorStatus(int dStatus,int dId) {
		String sql = "update doctor set dStatus=? where dId=?";
		int n = DBUtil.execute(sql,dStatus,dId);
		return n>0;
	}
    
	/**
	 * 根据医生姓名dName，修改医生信息
	 */
	@Override
	public boolean updateDoctorByName(Doctor doctor,String dName) {
		// TODO Auto-generated method stub
		String sql = "update doctor set dPassword=?,dSex=?,dBirthday=?,dDesc=?,dUrl=?,dStatus=? where dName=?";
		int n = DBUtil.execute(sql, doctor.getdPassword(),doctor.getdSex(),doctor.getdBirthday(),doctor.getdDesc(),doctor.getdUrl(),doctor.getdStatus(),dName);
		return n>0;
	}

	/**
	 * 查找单个医生
	 */
	@Override
	public Doctor findDoctorById(int dId) {
		String sql = "select * from doctor where dId=?";
		List<Doctor> list = (List<Doctor>) DBUtil.select(sql, Doctor.class, dId);
		return list.get(0);
	}

	
	
	

}
