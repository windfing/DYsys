/**
 * 
 */
package com.etc.dysys.admin.dao.impl;

import java.util.List;

import com.etc.dysys.admin.dao.AdminDao;
import com.etc.dysys.entity.Admin;
import com.etc.dysys.util.DBUtil;

/**
 * @author ccq
 *
 * date: 2019年4月2日  上午9:25:53
 */
public class AdminDaoImpl implements AdminDao{

	
	@Override
	public Admin findAdmin(Admin admin) {
		String sql = "select * from admin where adminName=? and adminPwd=?";
		List<Admin> list = (List<Admin>) DBUtil.select(sql, Admin.class,admin.getAdminName(),admin.getAdminPwd());
		if(list.size()>0) {
			return list.get(0);
		}
		return null;
	}

}
