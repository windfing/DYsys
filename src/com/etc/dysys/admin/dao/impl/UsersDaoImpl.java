package com.etc.dysys.admin.dao.impl;

import java.util.List;

import org.apache.catalina.User;

import com.etc.dysys.admin.dao.UsersDao;
import com.etc.dysys.entity.Doctor;
import com.etc.dysys.entity.Users;
import com.etc.dysys.util.DBUtil;

/**
 * 
 * UsersDaoImpl UsersDao的实现类
 * 
 * @author 黄伟鸿
 *
 */
public class UsersDaoImpl implements UsersDao {

	/**
	 * 用户的增加
	 */
	public boolean addUsers(Users user) {
		String sql = "INSERT INTO `dysysdb`.`users`(`uName`, `uPassword`, `uSex`,`uPhone`, `uBirthday`, `uAddress`, `uStatus`) VALUES (?,?,?,?,?,?,?)";
		return DBUtil.execute(sql, user.getuName(), user.getuPassword(), user.getuSex(), user.getuPhone(),
				user.getuBirthday(), user.getuAddress(), user.getuStatus()) > 0;
	}

	/**
	 * 用户的查询
	 */
	@Override
	public List<Users> getUsers() {
		// TODO Auto-generated method stub
		String sql = "select * from users";
		List<Users> list = (List<Users>) DBUtil.select(sql, Users.class);
		return list;
	}

	/**
	 * 删除用户
	 */
	@Override
	public boolean delUsersById(int uId) {
		// TODO Auto-generated method stub
		String sql = "delete  from users where uId = ?";
		return DBUtil.execute(sql, uId) > 0;
	}

	/**
	 * 用户的停用
	 */
	@Override
	public boolean stopUsersById(int uId) {
		// TODO Auto-generated method stub
		String sql = "update users set uStatus=1 where uId=?";
		return DBUtil.execute(sql, uId) > 0;
	}

	/**
	 * 用户的启用
	 */
	@Override
	public boolean startUsersById(int uId) {
		// TODO Auto-generated method stub
		String sql = "update users set uStatus=0 where uId=?";
		return DBUtil.execute(sql, uId) > 0;
	}

	/**
	 * 用户的修改
	 */
	@Override
	public boolean updateUsersByName(Users user, String uName) {
		// TODO Auto-generated method stub
		String sql = "update users set uSex=?,uPhone=?,uBirthday=?,uAddress=? where uName=?";
		return DBUtil.execute(sql, user.getuSex(), user.getuPhone(), user.getuBirthday(), user.getuAddress(),
				 uName) > 0;
	}

	/**
	 * 根据ID查询用户
	 */
	@Override
	public Users selUsersById(int uId) {
		// TODO Auto-generated method stub
		String sql = "select * from users where uId=?";
		List<Users> list = (List<Users>) DBUtil.select(sql, Users.class, uId);
		return list.get(0);
	}


}
