package com.etc.dysys.admin.dao.impl;

import java.util.List;

import com.etc.dysys.admin.dao.AppointmentDao;
import com.etc.dysys.entity.AppointmentView;
import com.etc.dysys.util.DBUtil;
/**
 * 后台AppointmentDao实现类
 * @author 黄志鹏
 *
 */
public class AppointmentDaoImpl implements AppointmentDao{

	/**
	 * 查询所有预约服务的方法
	 */
	@Override
	public List<AppointmentView> findAllAppointment() {
		// TODO Auto-generated method stub
		String sql ="select * from appointmentview";
		List<AppointmentView> list = (List<AppointmentView>) DBUtil.select(sql, AppointmentView.class);
		return list;
	}

}
