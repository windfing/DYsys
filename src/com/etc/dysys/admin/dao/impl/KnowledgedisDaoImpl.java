package com.etc.dysys.admin.dao.impl;

import java.util.List;

import com.etc.dysys.admin.dao.KnowledgedisDao;
import com.etc.dysys.entity.KdisKnowUserDoc;
import com.etc.dysys.util.DBUtil;
/**
 * 知识评论 dao的实现层
 * @author 薛佳鑫
 *
 */
public class KnowledgedisDaoImpl implements KnowledgedisDao {
	
	@Override
	public List<KdisKnowUserDoc> getKnowledgedis() {
		// TODO Auto-generated method stub
		String sql="select kdId,kdContent,kTitle,DATE_FORMAT(kdTime, '%Y-%m-%d %k:%i:%s') kdTime,uName,dName from kdview";
		List<KdisKnowUserDoc> list=(List<KdisKnowUserDoc>) DBUtil.select(sql, KdisKnowUserDoc.class);
		return list;
	}

	@Override
	public boolean delKnowledgedisById(int kdId) {
		// TODO Auto-generated method stub
		String sql="delete from knowledgedis where kdId=? ";
		return DBUtil.execute(sql, kdId)>0;
	}

}
