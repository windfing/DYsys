package com.etc.dysys.admin.dao.impl;

import java.util.List;

import com.etc.dysys.admin.dao.ServiceDao;
import com.etc.dysys.entity.Service;
import com.etc.dysys.util.DBUtil;

/**
 * 
 * @author 高文乾
 *
 */
public class ServiceDaoImpl implements ServiceDao {

	@Override
	public List<Service> findAllServices() {
		String sql = "select * from service";
		List<Service> list = (List<Service>) DBUtil.select(sql, Service.class);
		return list;
	}

	/**
	 * 添加服务
	 */
	@Override
	public boolean addService(String sTitle, String sContent, int sStatus, int dId) {
		// TODO Auto-generated method stub
		String sql = "insert into service(sTitle,sContent,sStatus,dId) values(?,?,?,?)";
		return DBUtil.execute(sql, sTitle, sContent, sStatus, dId) > 0;
	}

	/**
	 * 删除方法
	 */
	@Override
	public boolean delServiceById(int sId) {
		// TODO Auto-generated method stub
		String sql = "delete from service where sId=?";
		return DBUtil.execute(sql, sId) > 0;
	}

	@Override
	public boolean updateService(int sId, String sTitle, String sContent, int sStatus, int dId) {
		// TODO Auto-generated method stub
		String sql = "update service set sTitle=?,sContent=?,sStatus=?,dId=? where sId=?";
		return DBUtil.execute(sql, sTitle, sContent, sStatus, dId, sId) > 0;
	}

	@Override
	public boolean stopService(int sId) {
		// TODO Auto-generated method stub
		String sql = "update service set sStatus=0 where sId=?";
		return DBUtil.execute(sql, sId) > 0;
	}

	@Override
	public boolean startService(int sId) {
		// TODO Auto-generated method stub
		String sql = "update service set sStatus=1 where sId=?";
		return DBUtil.execute(sql, sId) > 0;
	}

	@Override
	public List<Service> getAllServices() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean updateService02(int sId, String sTitle, String sContent, String sUrl, int sStatus, int dId) {
		// TODO Auto-generated method stub
		String sql = "update service set sTitle=?,sContent=?,sUrl=?,sStatus=?,dId=? where sId=?";
		return DBUtil.execute(sql, sTitle, sContent, sUrl, sStatus, dId, sId) > 0;
	}

	@Override
	public boolean addService02(Service service) {
		// TODO Auto-generated method stub
		String sql = "insert into service(sTitle,sContent,sUrl,sStatus,dId) values(?,?,?,?,?)";
		int n = DBUtil.execute(sql, service.getsTitle(), service.getsContent(), service.getsUrl(), service.getsStatus(),
				service.getdId());
		return n > 0;
	}

}
