package com.etc.dysys.admin.dao.impl;

import java.util.List;

import com.etc.dysys.admin.dao.VideoDao;
import com.etc.dysys.entity.Video;
import com.etc.dysys.util.DBUtil;
/**
 * 后台视频操作的Dao实现类
 * @author Administrator
 *
 */
public class VideoDaoImpl implements VideoDao {
	
	/**
	 * 保存视频信息的方法
	 */
	@Override
	public int saveVideo(Video video) {
		// TODO Auto-generated method stub
		String sql ="insert into video(vSrc,vTitle) values(?,?)";
		int n = DBUtil.execute(sql, video.getvSrc(),video.getvTitle());
		return n;
	}
	/**
	 * 查询所有视频信息的方法
	 */
	@Override
	public List<Video> getAll() {
		// TODO Auto-generated method stub
		String sql ="select * from video";
		List<Video> list = (List<Video>) DBUtil.select(sql, Video.class);
		return list;
	}
	/**
	 * 根据视频Id删除视频的方法
	 */
	@Override
	public int deleteVideo(int vId) {
		// TODO Auto-generated method stub
		String sql ="delete from video where vId=?";
		int n = DBUtil.execute(sql, vId);
		return n;
	}

}
