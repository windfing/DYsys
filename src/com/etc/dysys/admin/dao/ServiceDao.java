package com.etc.dysys.admin.dao;

import java.util.List;

import com.etc.dysys.entity.Service;

/**
 * 
 * 
 * @author 高文乾
 *
 */
public interface ServiceDao {
	/**
	 * 得到所有Service数据
	 * 
	 * @return
	 */
	public List<Service> findAllServices();

	/**
	 * 得到所有service数据，数据库二次更改
	 * 
	 * @return
	 */
	public List<Service> getAllServices();

	/**
	 * 添加服务
	 * 
	 * @param sTitle
	 * @param sContent
	 * @param sStatus
	 * @param dId
	 * @return
	 */
	public boolean addService(String sTitle, String sContent, int sStatus, int dId);

	/**
	 * 删除service的方法
	 * 
	 * @param sId
	 * @return
	 */
	public boolean delServiceById(int sId);

	/**
	 * 修改编辑
	 * 
	 * @param sId
	 * @param sTitle
	 * @param sContent
	 * @param sStatus
	 * @param dId
	 * @return
	 */
	public boolean updateService(int sId, String sTitle, String sContent, int sStatus, int dId);

	/**
	 * 停用状态
	 * 
	 * @param sId
	 * @return
	 */
	public boolean stopService(int sId);

	/**
	 * 启用状态
	 * 
	 * @param sId
	 * @return
	 */
	public boolean startService(int sId);

	/**
	 * 修改service数据（修改版）
	 * 
	 * @param sId
	 * @param sTitle
	 * @param sContent
	 * @param sUrl
	 * @param sStatus
	 * @param dId
	 * @return
	 */
	public boolean updateService02(int sId, String sTitle, String sContent, String sUrl, int sStatus, int dId);

	/**
	 * 添加service数据（修改版）
	 * 
	 * @param service
	 * @return
	 */
	public boolean addService02(Service service);

}
