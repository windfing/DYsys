/**
 * 
 */
package com.etc.dysys.admin.dao;

import com.etc.dysys.entity.Admin;

/**
 * @author ccq
 *
 * date: 2019年4月2日  上午9:25:18
 */
public interface AdminDao {
	
	/**
	 * 查询单个admin
	 * @return
	 */
	public Admin findAdmin(Admin admin);

}
