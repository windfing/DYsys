package com.etc.dysys.admin.dao;

import java.util.List;

import com.etc.dysys.entity.AppointmentView;

/**
 * 后台预约服务查询的Dao接口
 * @author 黄志鹏
 *
 */
public interface AppointmentDao {
	
	/**
	 * 查询所有服务的方法
	 * @return
	 */
	public List<AppointmentView> findAllAppointment();

}
