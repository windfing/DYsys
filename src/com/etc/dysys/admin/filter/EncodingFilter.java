package com.etc.dysys.admin.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 编码的过滤器
 * 
 * @author 黄伟鸿
 *
 */
@WebFilter(value = { "/users.action", "/Doctor.action", "/KnowledgedisServlet.action", "/Knowledge.action",
		"/Service.action", "/Video.action", "/Admin.action" })
public class EncodingFilter implements Filter {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		// 设置请求编码
		request.setCharacterEncoding("utf-8");
		// 响应编码
		response.setCharacterEncoding("utf-8");

		// "放行"
		chain.doFilter(request, response);
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub

	}

}
