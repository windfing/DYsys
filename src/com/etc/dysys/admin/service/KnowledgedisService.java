package com.etc.dysys.admin.service;

import com.etc.dysys.entity.KdisKnowUserDoc;
import com.etc.dysys.util.AjaxLogicBean;
/**
 * 知识评论 service
 * @author 薛佳鑫
 *
 */

public interface KnowledgedisService {
	/**
	 * 查询所有知识评论
	 * @return
	 */
	public  AjaxLogicBean<KdisKnowUserDoc> getKnowledgedis();
	
	/**
	 * 删除评论
	 * @param kdId  知识评论编号
	 * @return 布尔值
	 */
	public boolean delKnowledgedisById(int kdId);

}
