package com.etc.dysys.admin.service;

import java.util.List;

import com.etc.dysys.entity.Users;

/**
 * 
 * @author 黄伟鸿
 *
 */
public interface UsersService {

	/**
	 * 增加用户
	 * 
	 * @param user
	 * @return
	 */
	public boolean addUsers(Users user);

	/**
	 * 查詢用戶
	 * 
	 * @return
	 */
	public List<Users> getUsers();

	/**
	 * 删除用户
	 * 
	 * @param uId
	 * @return
	 */
	public boolean delUsersById(int uId);

	/**
	 * 用户的停用
	 * 
	 * @param uId
	 * @param uStatus
	 * @return
	 */
	public boolean stopUsersById(int uId);

	/**
	 * 用户的启用
	 * 
	 * @param uId
	 * @param uStatus
	 * @return
	 */
	public boolean startUsersById(int uId);

	/**
	 * 用户的修改
	 * 
	 * @param user
	 * @param uName
	 * @return
	 */
	public boolean updateUsersByName(Users user, String uName);

	/**
	 * 根据ID查找用户
	 * 
	 * @param uId
	 * @return
	 */
	public Users selUsersById(int uId);

}
