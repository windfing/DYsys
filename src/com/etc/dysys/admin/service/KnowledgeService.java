package com.etc.dysys.admin.service;

import java.util.List;

import com.etc.dysys.entity.Knowledge;
import com.etc.dysys.entity.KnowledgeView;
import com.etc.dysys.util.PageData;
/**
 * 知识管理的曾删改
 * @author 蒋丽娟
 *
 */
public interface KnowledgeService {
	/**
	 * 
	 * @return 返回所有的文章
	 */
	public List<KnowledgeView> allKnowledge();
	/**
	 * 
	 * @param kTitle 文章的标题
	 * @param kContent 文章的内容
	 * @return 返回是否增加成功
	 */
	public boolean addKnowledge(String kTitle,String  kContent);
	/**
	 * 
	 * @param kStatus 文章的状态
	 * @param kId 文章的ID
	 * @return 返回状态是否修改成功
	 */
	public boolean deleteKnowledge(int kStatus,int kId);


}
