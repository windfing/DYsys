package com.etc.dysys.admin.service;

import java.util.List;

import com.etc.dysys.entity.AppointmentView;

/**
 * 后台预约服务的service接口
 * 
 * @author 黄志鹏
 *
 */
public interface AppointmentService {
	/**
	 * 查询所有服务的预约信息的方法
	 * @return
	 */
	public List<AppointmentView> findAllAppointment();
}
