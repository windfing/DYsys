/**
 * 
 */
package com.etc.dysys.admin.service.impl;

import java.util.List;

import com.etc.dysys.admin.dao.AdminDao;
import com.etc.dysys.admin.dao.impl.AdminDaoImpl;
import com.etc.dysys.admin.service.AdminService;
import com.etc.dysys.entity.Admin;

/**
 * @author ccq
 *
 * date: 2019年4月2日  上午9:27:39
 */
public class AdminServiceImpl implements AdminService{

	private AdminDao ad = new AdminDaoImpl();
	
	@Override
	public Admin findAdmin(Admin admin) {

		return ad.findAdmin(admin);
	}

}
