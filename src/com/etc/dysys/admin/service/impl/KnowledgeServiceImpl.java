package com.etc.dysys.admin.service.impl;

import java.util.List;

import com.etc.dysys.admin.dao.KnowledgeDao;
import com.etc.dysys.admin.dao.impl.KnowledgeDaoImpl;
import com.etc.dysys.admin.service.KnowledgeService;
import com.etc.dysys.entity.Knowledge;
import com.etc.dysys.entity.KnowledgeView;
import com.etc.dysys.util.PageData;
/**
 * 知识管理service的实现类
 * @author 蒋丽娟
 *
 */
public class KnowledgeServiceImpl implements KnowledgeService {
	// 得到knowledgeDao的对象
	KnowledgeDao kd = new KnowledgeDaoImpl();

	@Override
	public List<KnowledgeView> allKnowledge() {
		// TODO Auto-generated method stub
		List<KnowledgeView> list = kd.allKnowledge();
		if (list.size() > 0) {

			return list;
		}
		return null;

	}

	@Override
	public boolean addKnowledge(String kTitle, String kContent) {
		// TODO Auto-generated method stub
		boolean flag=kd.addKnowledge(kTitle, kContent);
		
		return flag;
	}

	@Override
	public boolean deleteKnowledge(int kStatus, int kId) {
		// TODO Auto-generated method stub
		boolean flag=kd.deleteKnowledge(kStatus, kId);
		return flag;
	}

}
