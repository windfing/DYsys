package com.etc.dysys.admin.service.impl;

import java.util.List;

import com.etc.dysys.admin.dao.UsersDao;
import com.etc.dysys.admin.dao.impl.UsersDaoImpl;
import com.etc.dysys.admin.service.UsersService;
import com.etc.dysys.entity.Users;
import com.etc.dysys.util.MD5Util;

/**
 * UsersServiceImpl UsersService的实现类
 * 
 * @author 黄伟鸿
 *
 */
public class UsersServiceImpl implements UsersService {

	private UsersDao ud = new UsersDaoImpl();

	/**
	 * 用户的增加
	 */
	@Override
	public boolean addUsers(Users user) {
		// TODO Auto-generated method stub

		if (user == null) {
			return false;
		} else {
			user.setuPassword(MD5Util.getEncodeByMd5(user.getuPassword()));
			System.out.println("UserService: " + user);
			return ud.addUsers(user);
		}
	}

	/**
	 * 用户的查询
	 */
	@Override
	public List<Users> getUsers() {
		// TODO Auto-generated method stub
		return ud.getUsers();
	}

	/**
	 * 用户的删除
	 */
	@Override
	public boolean delUsersById(int uId) {
		// TODO Auto-generated method stub
		return ud.delUsersById(uId);
	}

	/**
	 * 用户的停用
	 */
	@Override
	public boolean stopUsersById(int uId) {
		// TODO Auto-generated method stub
		return ud.stopUsersById(uId);
	}

	/**
	 * 用户的启用
	 */
	@Override
	public boolean startUsersById(int uId) {
		// TODO Auto-generated method stub
		return ud.startUsersById(uId);
	}

	/**
	 * 用户的修改
	 */
	@Override
	public boolean updateUsersByName(Users user, String uName) {
		// TODO Auto-generated method stub
		return ud.updateUsersByName(user, uName);
	}

	/**
	 * 根据ID查找用户
	 */
	@Override
	public Users selUsersById(int uId) {
		// TODO Auto-generated method stub
		return ud.selUsersById(uId);
	}


}
