package com.etc.dysys.admin.service.impl;

import java.util.List;

import com.etc.dysys.admin.dao.AppointmentDao;
import com.etc.dysys.admin.dao.impl.AppointmentDaoImpl;
import com.etc.dysys.admin.service.AppointmentService;
import com.etc.dysys.entity.AppointmentView;
/**
 * 后台预约服务的service的实现类
 * @author 黄志鹏
 *
 */
public class AppointmentServiceImpl implements AppointmentService {
	private AppointmentDao ad= new AppointmentDaoImpl();
	/**
	 * 查询所有的预约服务信息
	 */
	@Override
	public List<AppointmentView> findAllAppointment() {
		// TODO Auto-generated method stub
		List<AppointmentView> list = ad.findAllAppointment();
		return list;
	}

}
