package com.etc.dysys.admin.service.impl;

import java.util.List;

import com.etc.dysys.admin.dao.DoctorDao;
import com.etc.dysys.admin.dao.impl.DoctorDaoImpl;
import com.etc.dysys.admin.service.DoctorService;
import com.etc.dysys.entity.Doctor;
import com.etc.dysys.util.MD5Util;

public class DoctorServiceImpl implements DoctorService {

	private DoctorDao dd = new DoctorDaoImpl();
	
	@Override
	public List<Doctor> getAllDoctor() {
		return dd.getAllDoctor();
	}


	@Override
	public boolean addDoctor(Doctor doctor) {
		if(doctor==null) {
			return false;
		}else {
			doctor.setdPassword(MD5Util.getEncodeByMd5(doctor.getdPassword()));
			return dd.addDoctor(doctor);
		}
		
	}

	
	@Override
	public boolean deleteDoctor(int dId) {
		// TODO Auto-generated method stub
		return dd.deleteDoctor(dId);
	}
	
	@Override
	public boolean updateDoctorStatus(int dStatus, int dId) {
		
		return dd.updateDoctorStatus(dStatus, dId);
	}

	
	@Override
	public boolean updateDoctorByName(Doctor doctor,String dName) {
		if(doctor==null) {
			return false;
		}else {
			doctor.setdPassword(MD5Util.getEncodeByMd5(doctor.getdPassword()));
			return dd.updateDoctorByName(doctor,dName);
		}		
	}


	@Override
	public Doctor findDoctorById(int dId) {
		// TODO Auto-generated method stub
		return dd.findDoctorById(dId);
	}

	

}
