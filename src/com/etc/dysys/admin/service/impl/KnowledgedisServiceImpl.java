package com.etc.dysys.admin.service.impl;

import java.util.List;

import com.etc.dysys.admin.dao.KnowledgedisDao;
import com.etc.dysys.admin.dao.impl.KnowledgedisDaoImpl;
import com.etc.dysys.admin.service.KnowledgedisService;
import com.etc.dysys.entity.KdisKnowUserDoc;
import com.etc.dysys.util.AjaxLogicBean;
/**
 * 知识评论的service的实现
 * @author 薛佳鑫
 *
 */
public class KnowledgedisServiceImpl implements KnowledgedisService{
	private KnowledgedisDao kd=new KnowledgedisDaoImpl();
	@Override
	public AjaxLogicBean<KdisKnowUserDoc> getKnowledgedis() {
		// TODO Auto-generated method stub
		List<KdisKnowUserDoc> list=kd.getKnowledgedis();
		AjaxLogicBean<KdisKnowUserDoc> alb=new AjaxLogicBean<>();
		alb.setData(list);
		return alb;
	}
	@Override
	public boolean delKnowledgedisById(int kdId) {
		// TODO Auto-generated method stub
		return kd.delKnowledgedisById(kdId);
	}

}
