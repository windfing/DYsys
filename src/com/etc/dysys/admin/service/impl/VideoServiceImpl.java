package com.etc.dysys.admin.service.impl;

import java.util.List;

import com.etc.dysys.admin.dao.VideoDao;
import com.etc.dysys.admin.dao.impl.VideoDaoImpl;
import com.etc.dysys.admin.service.VideoService;
import com.etc.dysys.entity.Video;
import com.etc.dysys.util.AjaxLogicBean;

/**
 * 后台视频操作的service实现类
 * @author 黄志鹏
 *
 */
public class VideoServiceImpl implements VideoService {
	
	private VideoDao videoDao = new VideoDaoImpl();
	/**
	 * 保存视频信息的方法
	 */
	@Override
	public boolean saveVideo(Video video) {
		// TODO Auto-generated method stub
		int n =videoDao.saveVideo(video);
		return n>0;
	}
	/**
	 * 后台查询所有视频信息的方法
	 */
	@Override
	public AjaxLogicBean<Video> getAll() {
		// TODO Auto-generated method stub
		List<Video> list = videoDao.getAll();
		AjaxLogicBean<Video> pd = new AjaxLogicBean<>();
		pd.setData(list);
		return pd;
	}
	/**
	 * 根据vId删除视频的方法
	 */
	@Override
	public boolean deleteVideo(int vId) {
		// TODO Auto-generated method stub
		int  n = videoDao.deleteVideo(vId);
		return n>0;
	}

}
