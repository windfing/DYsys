package com.etc.dysys.admin.service.impl;

import java.util.List;

import com.etc.dysys.admin.dao.ServiceDao;
import com.etc.dysys.admin.dao.impl.ServiceDaoImpl;
import com.etc.dysys.admin.service.ServiceService;
import com.etc.dysys.entity.Service;

public class ServiceServiceImpl implements ServiceService {

	ServiceDao sd = new ServiceDaoImpl();

	@Override
	public List<Service> findAllServices() {
		// TODO Auto-generated method stub
		return sd.findAllServices();
	}

	@Override
	public boolean addService(String sTitle, String sContent, int sStatus, int dId) {
		// TODO Auto-generated method stub
		return sd.addService(sTitle, sContent, sStatus, dId);
	}

	@Override
	public boolean delServiceById(int sId) {
		// TODO Auto-generated method stub
		return sd.delServiceById(sId);
	}

	@Override
	public boolean updateService(int sId, String sTitle, String sContent, int sStatus, int dId) {
		// TODO Auto-generated method stub
		return sd.updateService(sId, sTitle, sContent, sStatus, dId);
	}

	@Override
	public boolean stopService(int sId) {
		// TODO Auto-generated method stub
		return sd.stopService(sId);
	}

	@Override
	public boolean startService(int sId) {
		// TODO Auto-generated method stub
		return sd.startService(sId);
	}

	@Override
	public boolean updateService02(int sId, String sTitle, String sContent, String sUrl, int sStatus, int dId) {
		// TODO Auto-generated method stub
		return sd.updateService02(sId, sTitle, sContent, sUrl, sStatus, dId);
	}

	@Override
	public boolean addService02(Service service) {
		// TODO Auto-generated method stub
		return sd.addService02(service);
	}

}
