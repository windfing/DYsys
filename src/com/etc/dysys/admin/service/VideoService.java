package com.etc.dysys.admin.service;

import com.etc.dysys.entity.Video;
import com.etc.dysys.util.AjaxLogicBean;

/**
 * 后台视频管理操作的service接口
 * @author 黄志鹏
 *
 */
public interface VideoService {

	/**
	 * 保存视频信息的方法
	 * @param video
	 * @return
	 */
	public boolean saveVideo(Video video);
	/**
	 * 后台查询所有视频信息的方法
	 * @return
	 */
	public AjaxLogicBean<Video> getAll();
	/**
	 * 根据vId删除video的方法
	 * @param vId
	 * @return
	 */
	public boolean deleteVideo(int vId);

}
