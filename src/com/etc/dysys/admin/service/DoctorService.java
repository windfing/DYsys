/**
 * 
 */
package com.etc.dysys.admin.service;

import java.util.List;

import com.etc.dysys.entity.Doctor;

/**
 * @author ccq
 *
 * date: 2019年3月28日  上午10:34:01
 */
public interface DoctorService {
	
	public List<Doctor> getAllDoctor();
	
	public boolean addDoctor(Doctor doctor);
	
	public boolean deleteDoctor(int dId);
	
	public boolean updateDoctorStatus(int dStatus,int dId);
	
	public boolean updateDoctorByName(Doctor doctor,String dName);
	
	public Doctor findDoctorById(int dId);

}
