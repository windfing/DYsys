/**
 * 
 */
package com.etc.dysys.admin.service;

import com.etc.dysys.entity.Admin;

/**
 * @author ccq
 *
 * date: 2019年4月2日  上午9:26:49
 */
public interface AdminService {
	
	public Admin findAdmin(Admin admin);

}
