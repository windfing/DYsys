package com.etc.dysys.user.service;

import com.etc.dysys.entity.Appointment;
import com.etc.dysys.entity.AppointmentView;
import com.etc.dysys.entity.Doctor;
import com.etc.dysys.entity.Service;
import com.etc.dysys.util.AjaxLogicBean;

/**
 * 前台预约信息操作的service接口
 * 
 * @author 黄志鹏
 *
 */
public interface UAppointmentService {
	/**
	 * 用户查询已完成的预约信息的方法
	 * 
	 * @param uId
	 *            查询的用户Id
	 * @return
	 */
	public AjaxLogicBean<AppointmentView> getUserAppointmentSuccessByuId(int uId);

	/**
	 * 医生查询被预约的信息的方法
	 * 
	 * @param dId
	 * @return
	 */
	public AjaxLogicBean<AppointmentView> getDoctorAppointment(int dId);

	/**
	 * 用户查询未完成的预约信息的方法
	 * 
	 * @param uId
	 * @return
	 */
	public AjaxLogicBean<AppointmentView> getUserAppointmentUnfinishedByuId(int uId);

	/**
	 * 用户通过取消为完成服务的方法
	 * 
	 * @param aId
	 *            服务订单Id
	 * @return
	 */
	public boolean userDeleteAppointmentByaId(int aId);

	/**
	 * 医生完成服务的方法
	 * 
	 * @param aId
	 * @return
	 */
	public boolean doctorCompleteAppointmentByaId(int aId);

	/**
	 * 医生查询自己已完成的服务的方法
	 * 
	 * @param dId
	 * @return
	 */
	public AjaxLogicBean<AppointmentView> getDoctorShowsuccess(int dId);

	/**
	 * 预约服务
	 * 
	 * @param appointment
	 * @return
	 */
	public boolean addAppointment(Appointment appointment);

	/**
	 * 得到相应的Service
	 * 
	 * @param sTitle
	 * @return
	 */
	public Service getServiceByTitle(String sTitle);

	/**
	 * 得到相应的doctor
	 * 
	 * @param sTitle
	 * @return
	 */
	public Doctor getDoctorByName(String dName);

}
