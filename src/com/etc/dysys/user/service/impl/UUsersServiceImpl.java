package com.etc.dysys.user.service.impl;

import com.etc.dysys.entity.Users;
import com.etc.dysys.user.dao.UUsersDao;
import com.etc.dysys.user.dao.impl.UUsersDaoImpl;
import com.etc.dysys.user.service.UUsersService;
import com.etc.dysys.util.MD5Util;

/**
 * 前台user操作的service类
 * 
 * @author 黄志鹏
 *
 */
public class UUsersServiceImpl implements UUsersService {
	private UUsersDao uUserDao = new UUsersDaoImpl();

	/**
	 * 保存用户（注册）的方法
	 */
	@Override
	public boolean saveUser(Users users) {
		// 判断用户名是否已经存在
		Users users2 = uUserDao.checkuName(users.getuName());
		// 判断用户的手机号是否已经存在
		Users users3 = uUserDao.checkPhone(users.getuPhone());
		if (users2 == null && users3 == null) {
			users.setuPassword(MD5Util.getEncodeByMd5(users.getuPassword()));
			int n = uUserDao.saveUser(users);
			return n > 0;
		}
		return false;
	}

	/**
	 * 注册表单Ajax异步校验手机号码是否存在的方法
	 */
	@Override
	public boolean checkPhone(String uPhone) {
		// TODO Auto-generated method stub
		Users users = uUserDao.checkPhone(uPhone);
		if (users != null) {
			return true;
		}
		return false;
	}

	/**
	 * 根据手机号跟密码查询用户的方法（即用户登录的方法）
	 */
	@Override
	public Users getUserByuPhoneanduPassword(String uPhone, String uPassword) {
		// TODO Auto-generated method stub
		String iuPassword = MD5Util.getEncodeByMd5(uPassword);
		Users users = uUserDao.getUserByuPhoneanduPassword(uPhone, iuPassword);
		return users;
	}

	/**
	 * 根据uId查询用户信息
	 */
	@Override
	public Users getUserByuId(int uId) {
		// TODO Auto-generated method stub
		Users users = uUserDao.getUserByuId(uId);
		return users;
	}

	/**
	 * 修改用户信息的方法
	 */
	@Override
	public boolean updateUsers(Users users) {
		if(users.getuPassword().equals("")) {
			Users users2 = uUserDao.checkuName(users.getuName());
			System.out.println(users2);
			String s = users2.getuPassword();
			users.setuPassword(s);
			int n = uUserDao.updateUsers(users);
			return n > 0;
		}else {
		users.setuPassword(MD5Util.getEncodeByMd5(users.getuPassword()));
		int n = uUserDao.updateUsers(users);
		return n > 0;
	}
	}
	/**
	 * Ajax异步校验用户名是否存在的方法
	 */
	@Override
	public boolean checkuName(String uName) {
		// TODO Auto-generated method stub

		// TODO Auto-generated method stub
		Users users = uUserDao.checkuName(uName);
		if (users != null) {
			return true;
		}
		return false;

	}

}
