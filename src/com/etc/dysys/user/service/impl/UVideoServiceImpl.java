package com.etc.dysys.user.service.impl;

import java.util.List;

import com.etc.dysys.entity.Video;
import com.etc.dysys.user.dao.UVideoDao;
import com.etc.dysys.user.dao.impl.UVideoDaoImpl;
import com.etc.dysys.user.service.UVideoService;
import com.etc.dysys.util.AjaxLogicBean;

public class UVideoServiceImpl implements UVideoService {
	private UVideoDao uvd = new UVideoDaoImpl();

	@Override
	public AjaxLogicBean<Video> getFourVideos() {
		// TODO Auto-generated method stub
		List<Video> list = uvd.getFourVideos();
		if (list.size() > 0) {
			AjaxLogicBean<Video> alb = new AjaxLogicBean<>();
			alb.setData(list);
			return alb;
		} else {
			return null;
		}
	}

}
