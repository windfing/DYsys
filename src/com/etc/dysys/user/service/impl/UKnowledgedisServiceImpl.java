package com.etc.dysys.user.service.impl;

import java.util.List;

import com.etc.dysys.entity.KdisKnowUserDoc;
import com.etc.dysys.entity.Knowledgedis;
import com.etc.dysys.entity.UKnowledgedisVo;
import com.etc.dysys.user.dao.UKnowledgedisDao;
import com.etc.dysys.user.dao.impl.UKnowledgedisDaoImpl;
import com.etc.dysys.user.service.UKnowledgedisService;
import com.etc.dysys.util.AjaxLogicBean;

public class UKnowledgedisServiceImpl implements UKnowledgedisService {
	
	private UKnowledgedisDao ukd=new UKnowledgedisDaoImpl();
	@Override
	public boolean addKnowledgedis(Knowledgedis kd) {
		// TODO Auto-generated method stub
		return ukd.addKnowledgedis(kd);
	}
	@Override
	public AjaxLogicBean<UKnowledgedisVo> getUKnowledgedis(int kId) {
		// TODO Auto-generated method stub
		List<UKnowledgedisVo> list=ukd.getUKnowledgedis(kId);
		AjaxLogicBean<UKnowledgedisVo> alb=new AjaxLogicBean<>();
		alb.setData(list);
		return alb;
	}
	@Override
	public int  getUKnowledgedisCount(int kId) {
		// TODO Auto-generated method stub
		return ukd.getUKnowledgedisCount(kId);
		
	}

}
