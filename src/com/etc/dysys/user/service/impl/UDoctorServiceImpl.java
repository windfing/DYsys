package com.etc.dysys.user.service.impl;

import java.util.List;

import com.etc.dysys.entity.Doctor;
import com.etc.dysys.user.dao.UDoctorDao;
import com.etc.dysys.user.dao.impl.UDoctorDaoImpl;
import com.etc.dysys.user.service.UDoctorService;
import com.etc.dysys.util.AjaxLogicBean;
import com.etc.dysys.util.MD5Util;
/**
 * 前台医生操作的service实现类
 * @author 黄志鹏
 *
 */
public class UDoctorServiceImpl implements UDoctorService {
	private UDoctorDao uDoctorDao = new UDoctorDaoImpl();
	/**
	 * 前台医生登录的方法
	 */
	@Override
	public Doctor getDoctorByNameandPassword(String account, String password) {
		// TODO Auto-generated method stub
		String ipassword= MD5Util.getEncodeByMd5(password);
		Doctor doctor =uDoctorDao.getDoctorByNameandPassword(account,ipassword);
		return doctor;
	}

	/**
	 * 根据医生Id查询医生对象的方法
	 */
	@Override
	public Doctor getDoctorBydId(int dId) {
		// TODO Auto-generated method stub
		Doctor doctor=uDoctorDao.getDoctorBydId(dId);
		return doctor;
	}
	@Override
	public AjaxLogicBean<Doctor> getDoctors() {
		// TODO Auto-generated method stub
		List<Doctor> list=uDoctorDao.getDoctors();
		AjaxLogicBean<Doctor> alb=new AjaxLogicBean<>();
		alb.setData(list);
		return alb;

	}

	@Override
	public AjaxLogicBean<Doctor> getDoctorTeam() {
		// TODO Auto-generated method stub
		List<Doctor> list=uDoctorDao.getDoctorTeam();
		AjaxLogicBean<Doctor> alb=new AjaxLogicBean<>();
		alb.setData(list);
		return alb;
	}

}
