package com.etc.dysys.user.service.impl;

import java.util.List;

import com.etc.dysys.entity.KnowledegeIndex;
import com.etc.dysys.entity.Knowledge;
import com.etc.dysys.entity.KnowledgeView;
import com.etc.dysys.user.dao.ArticleDao;
import com.etc.dysys.user.dao.impl.ArticleDaoImpl;
import com.etc.dysys.user.service.ArticleService;
import com.etc.dysys.util.AjaxLogicBean;
import com.etc.dysys.util.PageData;
/**
 * 文章知识Service的实现类
 * @author 蒋丽娟
 *
 */
public class ArticleServiceImpl implements ArticleService {
	ArticleDao ad=new ArticleDaoImpl();

	@Override
	public PageData<KnowledgeView> allArticle(int page, int pageSize,String content) {
		// TODO Auto-generated method stub
		PageData<KnowledgeView> pd=ad.allArticle(page, pageSize,content);
	     List<KnowledgeView> list=pd.getData();
	     if(pd.getData().size()>0) {
	    	 return pd;
	     }else
		return null;
	}

	@Override
	public PageData<Knowledge> personUserA(int page, int pageSize, int uId) {
		PageData<Knowledge> pd=ad.personUserA(page, pageSize, uId);
		   if(pd.getData().size()>0) {
		    	 return pd;
		     }else
			return null;
		
	}

	@Override
	public PageData<Knowledge> personDoctorA(int page, int pageSize, int dId) {
		PageData<Knowledge> pd=ad.personDoctorA(page, pageSize, dId);
		   if(pd.getData().size()>0) {
		    	 return pd;
		     }else
			return null;
		}

	@Override
	public boolean addUser(String kTitle, String kContent, int uId) {
		boolean flag=ad.addUser(kTitle, kContent, uId);
		return flag;
	}

	@Override
	public boolean addDoctor(String kTitle, String kContent, int dId) {
		boolean flag=ad.addDoctor(kTitle, kContent, dId);
		return flag;
	}

	@Override
	public boolean delete(int kId) {
		// TODO Auto-generated method stub
		boolean flag=ad.delete(kId);
		return flag;
	}

	@Override
	public KnowledgeView show(int kId) {
		KnowledgeView k=ad.show(kId);
		if(k!=null) {
			return k;
			}
		return null;
	}

	@Override
	public List<KnowledegeIndex> index() {
		// TODO Auto-generated method stub
		List<KnowledegeIndex> list=ad.index();
	
		if(list.size()>0)
			return list;
		return null;
	}

	
}
