package com.etc.dysys.user.service.impl;

import java.util.List;

import com.etc.dysys.entity.Appointment;
import com.etc.dysys.entity.AppointmentView;
import com.etc.dysys.entity.Doctor;
import com.etc.dysys.entity.Service;
import com.etc.dysys.user.dao.UAppointmentDao;
import com.etc.dysys.user.dao.impl.UAppointmentDaoImpl;
import com.etc.dysys.user.service.UAppointmentService;
import com.etc.dysys.util.AjaxLogicBean;

/**
 * 前台预约操作的Service实现类
 * 
 * @author 黄志鹏
 *
 */
public class UAppointmentServiceImpl implements UAppointmentService {
	private UAppointmentDao uAppointmentDao = new UAppointmentDaoImpl();

	/**
	 * 用户查询已完成的预约信息的方法
	 */
	@Override
	public AjaxLogicBean<AppointmentView> getUserAppointmentSuccessByuId(int uId) {
		// TODO Auto-generated method stub
		List<AppointmentView> list = uAppointmentDao.getUserAppointmentSuccessByuId(uId);
		for (int i = 0; i < list.size(); i++) {
			String[] arr = list.get(i).getaCurrentTime().split("\\.");
			list.get(i).setaCurrentTime(arr[0]);
		}
		AjaxLogicBean<AppointmentView> data = new AjaxLogicBean<>();
		data.setData(list);
		return data;
	}

	/**
	 * 医生查询自己被预约的信息的方法
	 */
	@Override
	public AjaxLogicBean<AppointmentView> getDoctorAppointment(int dId) {
		// TODO Auto-generated method stub
		List<AppointmentView> list = uAppointmentDao.getDoctorAppointmentBydId(dId);
		for (int i = 0; i < list.size(); i++) {
			String[] arr = list.get(i).getaCurrentTime().split("\\.");
			list.get(i).setaCurrentTime(arr[0]);
		}
		AjaxLogicBean<AppointmentView> data = new AjaxLogicBean<>();
		data.setData(list);
		return data;
	}

	/**
	 * 用户查询自己未完成的预约的信息的方法
	 */
	@Override
	public AjaxLogicBean<AppointmentView> getUserAppointmentUnfinishedByuId(int uId) {
		// TODO Auto-generated method stub
		List<AppointmentView> list = uAppointmentDao.getUserAppointmentUnfinishedByuId(uId);
		for (int i = 0; i < list.size(); i++) {
			String[] arr = list.get(i).getaCurrentTime().split("\\.");
			list.get(i).setaCurrentTime(arr[0]);
		}
		AjaxLogicBean<AppointmentView> data = new AjaxLogicBean<>();
		data.setData(list);
		return data;
	}

	/**
	 * 用户取消为完成的预约的方法
	 */
	@Override
	public boolean userDeleteAppointmentByaId(int aId) {
		// TODO Auto-generated method stub
		int n = uAppointmentDao.userDeleteAppointmentByaId(aId);
		return n > 0;
	}

	/**
	 * 医生完成服务的方法
	 */
	@Override
	public boolean doctorCompleteAppointmentByaId(int aId) {
		// TODO Auto-generated method stub
		int n = uAppointmentDao.doctorCompleteAppointmentByaId(aId);
		return n > 0;
	}

	/**
	 * 医生查询自己已经完成的服务的方法
	 */
	@Override
	public AjaxLogicBean<AppointmentView> getDoctorShowsuccess(int dId) {
		List<AppointmentView> list = uAppointmentDao.getDoctorShowsuccessBydId(dId);
		for (int i = 0; i < list.size(); i++) {
			String[] arr = list.get(i).getaCurrentTime().split("\\.");
			list.get(i).setaCurrentTime(arr[0]);
		}
		AjaxLogicBean<AppointmentView> data = new AjaxLogicBean<>();
		data.setData(list);
		return data;
	}

	/**
	 * 预约服务
	 */
	@Override
	public boolean addAppointment(Appointment appointment) {
		// TODO Auto-generated method stub
		return uAppointmentDao.addAppointment(appointment);
	}

	@Override
	public Service getServiceByTitle(String sTitle) {
		// TODO Auto-generated method stub
		return uAppointmentDao.getServiceByTitle(sTitle);
	}

	@Override
	public Doctor getDoctorByName(String dName) {
		// TODO Auto-generated method stub
		return uAppointmentDao.getDoctorByName(dName);
	}

}
