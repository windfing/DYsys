package com.etc.dysys.user.service;

import com.etc.dysys.entity.Doctor;
import com.etc.dysys.util.AjaxLogicBean;

/**
 * 前台医生操作的service
 * @author 黄志鹏
 *
 */
public interface UDoctorService {
	/**
	 * 医生登录的方法
	 * @param account 账户名
	 * @param password 密码
	 * @return 查询到的医生对象
	 */
	public  Doctor getDoctorByNameandPassword(String account, String password);

	/**
	 * 根据医生Id获取医生对象的方法
	 * @param dId
	 * @return
	 */
	public Doctor getDoctorBydId(int dId);

	
	/**
	 * 获取5个医生
	 * @return
	 */
	public AjaxLogicBean<Doctor> getDoctors();
	
	
	/**
	 * 获取所有医生倒序
	 * @return
	 */
	public AjaxLogicBean<Doctor> getDoctorTeam();


}
