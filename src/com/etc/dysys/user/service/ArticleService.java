package com.etc.dysys.user.service;

import java.util.List;

import com.etc.dysys.entity.KnowledegeIndex;
import com.etc.dysys.entity.Knowledge;
import com.etc.dysys.entity.KnowledgeView;
import com.etc.dysys.util.AjaxLogicBean;
import com.etc.dysys.util.PageData;
/**
 * 文章知识的service的接口
 * @author 蒋丽娟
 *
 */
public interface ArticleService {
	/**
	 * 
	 * @param 第几页
	 * @param 一页显示多少条
	 * @return PageData
	 */
	public PageData<KnowledgeView> allArticle(int page,int pageSize,String content);
	/**
	 * 
	 * @param page 第几页
	 * @param pageSize 一页显示多少条
	 * @param uId 用户的ID
	 * @return 用户的文章列表
	 */
	public PageData<Knowledge> personUserA(int page,int pageSize,int uId);
	/**
	 * 
	 * @param page 第几页
	 * @param pageSize一页显示多少条
	 * @param dId 医生的id
	 * @return 医生的文章列表
	 */
	public PageData<Knowledge> personDoctorA(int page,int pageSize,int dId);
	/**
	 * 
	 * @param kTitle 文章的标题
	 * @param kContent 文章的内容
	 * @param uId 用户的id
	 * @return 如果增加成功，返回true,增加失败，返回false
	 */
	public boolean addUser(String kTitle,String  kContent,int uId);
	/**
	 * 
	 * @param kTitle 文章的标题
	 * @param kContent 文章的内容
	 * @param uId 医生的id
	 * @return 如果增加成功，返回true,增加失败，返回false
	 */
	public boolean addDoctor(String kTitle,String  kContent,int dId);
	/**
	 * 
	 * @param kId 文章的Id
	 * @return 如果删除成功，返回true,增加失败，返回false
	 */
	public boolean delete(int kId);
	/**
	 * 
	 * @param kId 文章的Id
	 * @return 返回这个Kid的文章内容，标题等
	 */
	public KnowledgeView show(int kId);
	/**
	 * 
	 * @return 首页展示的5条数据
	 */
	public List<KnowledegeIndex> index();


}
