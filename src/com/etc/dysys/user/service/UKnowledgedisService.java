package com.etc.dysys.user.service;

import java.util.List;

import com.etc.dysys.entity.KdisKnowUserDoc;
import com.etc.dysys.entity.Knowledgedis;
import com.etc.dysys.entity.UKnowledgedisVo;
import com.etc.dysys.util.AjaxLogicBean;

/**
 * 前台知识评论的service
 * @author 薛佳鑫
 *
 */
public interface UKnowledgedisService {
	/**
	 * 用户新增知识评论
	 * @param kd  知识评论对象
	 * @return 布尔值
	 */
	public boolean addKnowledgedis(Knowledgedis kd);
	
	
	
	/**
	 * 查询所有知识评论  按时间降序
	 * @return
	 */
	public  AjaxLogicBean<UKnowledgedisVo> getUKnowledgedis(int kId);
	
	/**
	 * 查询评论总数
	 * @param kId  文章id
	 * @return
	 */
	public int  getUKnowledgedisCount(int kId);

}
