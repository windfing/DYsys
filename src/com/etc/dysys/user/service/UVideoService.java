package com.etc.dysys.user.service;

import java.util.List;

import com.etc.dysys.entity.Video;
import com.etc.dysys.util.AjaxLogicBean;

public interface UVideoService {
	
	/**
	 * 查询最新4个视频
	 * @return json格式的对象
	 */
	public AjaxLogicBean<Video> getFourVideos();

}
