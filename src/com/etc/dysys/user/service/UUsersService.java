package com.etc.dysys.user.service;

import com.etc.dysys.entity.Users;

/**
 * 前台users操作的service接口
 * @author 黄志鹏
 *
 */
public interface UUsersService {
	/**
	 * 保存用户（注册）的方法
	 * @param users 注册的用户信息
	 * @return 是否成功
	 */
	public boolean saveUser(Users users);
	/**
	 * 注册表单Ajax异步校验手机号码是否存在的方法
	 * @param uPhone
	 * @return
	 */
	public boolean checkPhone(String uPhone);
	/**
	 * 根据手机号跟密码查询用户（即用户登录的方法）
	 * @param uPhone 用户的手机号
	 * @param uPassword 用户的密码
	 * @return 返回用户的信息
	 */
	public Users getUserByuPhoneanduPassword(String uPhone, String uPassword);
	/**
	 * 根据uId获取用户信息
	 * @param uId 用户的Id
	 * @return 返回获取的用户对象
	 */
	public Users getUserByuId(int uId);
	/**
	 * 用户修改个人信息的方法
	 * @param users 
	 * @return
	 */
	public boolean updateUsers(Users users);
	/**
	 * Ajax异步校验用户名是否存在的方法
	 * @param uName
	 * @return
	 */
	public boolean checkuName(String uName);

}
