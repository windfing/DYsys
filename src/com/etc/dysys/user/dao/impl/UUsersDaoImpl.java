package com.etc.dysys.user.dao.impl;

import java.util.List;

import com.etc.dysys.entity.Users;
import com.etc.dysys.user.dao.UUsersDao;
import com.etc.dysys.util.DBUtil;

/**
 * 前台user操作的DAO类
 * 
 * @author 黄志鹏
 *
 */
public class UUsersDaoImpl implements UUsersDao {
	/**
	 * 保存用户的方法
	 */
	@Override
	public int saveUser(Users users) {
		// TODO Auto-generated method stub
		String sql = "insert into users(uName,uPassword,uSex,uPhone,uBirthday,uAddress,uStatus) values(?,?,?,?,?,?,?)";
		int n = DBUtil.execute(sql, users.getuName(), users.getuPassword(), users.getuSex(), users.getuPhone(),
				users.getuBirthday(), users.getuAddress(), users.getuStatus());
		return n;
	}

	/**
	 * 注册表单异步校验手机号是否存在的方法（即根据手机号查询是否存在用户）
	 */
	@Override
	public Users checkPhone(String uPhone) {
		// TODO Auto-generated method stub
		String sql ="select * from users where uPhone=?";
		List<Users> list  = (List<Users>) DBUtil.select(sql, Users.class, uPhone);
		if(list.size()>0) {
			return list.get(0);
		}
		return null;
	}
	/**
	 * 根据手机号及密码查询用户信息的方法（即登录的方法）
	 */
	@Override
	public Users getUserByuPhoneanduPassword(String uPhone, String uPassword) {
		// TODO Auto-generated method stub
		String sql ="select * from users where (uPhone=? and uPassword =? and uStatus=0) or(uName=? and uPassword=? and uStatus=0)";
		List<Users> list = (List<Users>) DBUtil.select(sql, Users.class, uPhone,uPassword,uPhone,uPassword);
		if(list.size()>0) {
			return list.get(0);
		}
		return null;
	}
	/**
	 * 根据用户id查询用户
	 */
	@Override
	public Users getUserByuId(int uId) {
		// TODO Auto-generated method stub
		String sql ="select * from users where uId = ?";
		List<Users> list = (List<Users>) DBUtil.select(sql, Users.class, uId);
		if(list.size()>0) {
			return list.get(0);
		}
		return null;
	}
	/**
	 * 修改用户信息的方法
	 */
	@Override
	public int updateUsers(Users users) {
		// TODO Auto-generated method stub
		String sql ="update users set uPassword = ?,uSex=?, uBirthday=?,uAddress=? where uId=?";
		int n = DBUtil.execute(sql, users.getuPassword(),users.getuSex(),users.getuBirthday(),users.getuAddress(),users.getuId());
		return n;
	}
	/**
	 * 根据用户名查询用户名是否存在
	 */
	@Override
	public Users checkuName(String uName) {
		// TODO Auto-generated method stub
		String sql="select * from users where uName=?";
		List<Users> list  = (List<Users>) DBUtil.select(sql, Users.class, uName);
		if(list.size()>0) {
			return list.get(0);
		}
		return null;
	}

}
