package com.etc.dysys.user.dao.impl;

import java.util.List;

import javax.print.Doc;

import com.etc.dysys.entity.Doctor;
import com.etc.dysys.user.dao.UDoctorDao;
import com.etc.dysys.util.DBUtil;

/**
 * 前台医生操作的DAO实现类
 * 
 * @author 黄志鹏
 *
 */
public class UDoctorDaoImpl implements UDoctorDao {
	/**
	 * 医生登录的方法
	 */
	@Override
	public Doctor getDoctorByNameandPassword(String account, String ipassword) {
		// TODO Auto-generated method stub
		String sql ="select * from doctor where dName=? and dPassword =? and dStatus=0";
		List<Doctor> list = (List<Doctor>) DBUtil.select(sql, Doctor.class, account,ipassword);
		if(list.size()>0) {
			return list.get(0);
		}
		
		return null;
	}
	
	/**
	 * 根据医生Id获取医生对象的方法
	 */
	@Override
	public Doctor getDoctorBydId(int dId) {
		// TODO Auto-generated method stub
		String sql ="select * from doctor where dId=?";
		List<Doctor> list = (List<Doctor>) DBUtil.select(sql, Doctor.class, dId);
		if(list.size()>0) {
			return list.get(0);
		}
		return null;
	}
	
	
	/**
	 * 查询医生(首页5个医生)
	 */
	@Override
	public List<Doctor> getDoctors() {
		// TODO Auto-generated method stub
		String sql="select * from doctor where did BETWEEN 26 and 31";
		List<Doctor> list=(List<Doctor>) DBUtil.select(sql, Doctor.class);
		return list;
	}
	
	/**
	 * 查询所有医生(专家团队)
	 */
	@Override
	public List<Doctor> getDoctorTeam() {
		// TODO Auto-generated method stub
		String sql="select * from doctor ORDER BY dId DESC";
		List<Doctor> list=(List<Doctor>) DBUtil.select(sql, Doctor.class);
		return list;
	}

}
