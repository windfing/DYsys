package com.etc.dysys.user.dao.impl;

import java.util.List;

import com.etc.dysys.entity.Video;
import com.etc.dysys.user.dao.UVideoDao;
import com.etc.dysys.util.DBUtil;

public class UVideoDaoImpl implements UVideoDao{

	@Override
	public List<Video> getFourVideos() {
		// TODO Auto-generated method stub
		String sql="select * from video ORDER BY vid desc  LIMIT 4 ";
		List<Video> list =(List<Video>) DBUtil.select(sql, Video.class);
		return list;
	}

}
