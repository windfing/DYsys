package com.etc.dysys.user.dao.impl;

import java.util.List;

import com.etc.dysys.entity.KnowledegeIndex;
import com.etc.dysys.entity.Knowledge;
import com.etc.dysys.entity.KnowledgeView;
import com.etc.dysys.user.dao.ArticleDao;
import com.etc.dysys.util.DBUtil;
import com.etc.dysys.util.PageData;
/**
 * 文章知识的dao实现类
 * @author 蒋丽娟
 *
 */
public class ArticleDaoImpl implements ArticleDao {
	@Override
	public PageData<KnowledgeView> allArticle(int page,int pageSize,String content) {
		// TODO Auto-generated method stub
		String sql="select * from knowledgeview where (kTitle LIKE ? OR kContent LIKE ?)  AND kStatus=0 ORDER BY kTime DESC";
		PageData<KnowledgeView> pd=DBUtil.getPage(sql, page, pageSize, KnowledgeView.class,"%"+content+"%","%"+content+"%");
		return pd;
	}
/**
 * 用户发表的文章
 */
	@Override
	public PageData<Knowledge> personUserA(int page, int pageSize ,int uId) {
		// TODO Auto-generated method stub
		String sql="select * from knowledge where uId=? AND kStatus=0 ORDER BY kTime DESC";
		PageData<Knowledge> pd=DBUtil.getPage(sql, page, pageSize, Knowledge.class, uId);
		return pd;
	}
	/**
	 * 医生发表的文章
	 */
@Override
public PageData<Knowledge> personDoctorA(int page, int pageSize, int dId) {
	// TODO Auto-generated method stub
	String sql="select * from knowledge where dId=? AND kStatus=0 ORDER BY kTime DESC";
	PageData<Knowledge> pd=DBUtil.getPage(sql, page, pageSize, Knowledge.class, dId);
	return pd;
}
/**
 * 用户发表文章
 */
	@Override
	public boolean addUser(String kTitle,String  kContent,int uId) {
		String sql="INSERT INTO knowledge (kTitle, kContent, kTime,uId, kStatus) VALUES (?,?,now(),?,0)";
		int i=DBUtil.execute(sql, kTitle,kContent,uId);
		return i>0;
	}
	/**
	 * 医生发表的文章
	 */
	@Override
	public boolean addDoctor(String kTitle,String  kContent,int dId) {
		String sql="INSERT INTO knowledge (kTitle, kContent, kTime,dId, kStatus) VALUES (?,?,now(),?,0)";
		int i=DBUtil.execute(sql, kTitle,kContent,dId);
		return i>0;
	}
	/**
	 * 删除文章
	 */
	@Override
	public boolean delete(int kId) {
		// TODO Auto-generated method stub
		String sql="delete from knowledge where kId=?";
		int i=DBUtil.execute(sql, kId);
		return i>0;
	}
	/**
	 * 根据文章ID找到这个文章的内容
	 */
	@Override
	public KnowledgeView show(int kId) {
		String sql="select * from knowledgeView where kId=? ORDER BY kTime DESC";
		List<KnowledgeView> list= (List<KnowledgeView>) DBUtil.select(sql,KnowledgeView.class , kId);
		return list.get(0);
	}
	@Override
	public List<KnowledegeIndex> index() {
	String sql="select kId,kTitle,kContent,kTime from knowledge limit  5";

//		@SuppressWarnings("unchecked")
		List<KnowledegeIndex> list=(List<KnowledegeIndex>) DBUtil.select(sql,KnowledegeIndex.class);
		return list;
	}
	
	

}
