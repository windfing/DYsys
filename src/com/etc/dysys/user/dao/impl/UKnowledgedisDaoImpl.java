package com.etc.dysys.user.dao.impl;

import java.util.List;
import java.util.Objects;

import com.etc.dysys.entity.KdisKnowUserDoc;
import com.etc.dysys.entity.Knowledgedis;
import com.etc.dysys.entity.UKnowledgedisVo;
import com.etc.dysys.user.dao.UKnowledgedisDao;
import com.etc.dysys.util.DBUtil;
/**
 * 前台用户进行知识评论的dao的实现
 * @author 薛佳鑫
 *
 */
public class UKnowledgedisDaoImpl implements UKnowledgedisDao {

	@Override
	public boolean addKnowledgedis(Knowledgedis kd) {
		// TODO Auto-generated method stub
		String sql="";
		int i;
		//如果用户id为空，sql语句将uId置为null 如果医生id为空，sql语句将dId置为null,都为空则返回false
		if(kd.getuId()==0) {
			 sql="insert into knowledgedis (kId,kdContent,kdTime,uId,dId) values(?,?,now(),null,?)";
			 i=DBUtil.execute(sql, kd.getkId(),kd.getKdContent(),kd.getdId());
			 return i>0;
		}else if(kd.getdId()==0) {
			 sql="insert into knowledgedis (kId,kdContent,kdTime,uId,dId) values(?,?,now(),?,null)";
			 i=DBUtil.execute(sql, kd.getkId(),kd.getKdContent(),kd.getuId());
			 return i>0;
		}else{
			return false;
		}
		
	}

	@Override
	public List<UKnowledgedisVo> getUKnowledgedis(int kId) {
		// TODO Auto-generated method stub
		String sql="SELECT k.kId,kd.kdContent,  DATE_FORMAT(kd.kdTime, '%Y-%m-%d %k:%i:%s')kdTime,u.uName,d.dName  from  knowledgedis kd\r\n" + 
				"			LEFT JOIN knowledge k\r\n" + 
				"				on k.kId=kd.kId \r\n" + 
				"				LEFT JOIN   users u\r\n" + 
				"				on kd.uid=u.uId\r\n" + 
				"				LEFT JOIN  doctor d \r\n" + 
				"				on kd.did=d.did\r\n" + 
				"				where k.kId=?\r\n" + 
				"				ORDER BY kd.kdTime desc";
		List<UKnowledgedisVo> list=(List<UKnowledgedisVo>) DBUtil.select(sql, UKnowledgedisVo.class,kId);
		return list;
	}

	@Override
	public int  getUKnowledgedisCount(int kId) {
		// TODO Auto-generated method stub
		return getUKnowledgedis(kId).size();
	}

}
