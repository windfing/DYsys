package com.etc.dysys.user.dao.impl;

import java.util.List;

import com.etc.dysys.entity.Service;
import com.etc.dysys.user.dao.UServiceDao;
import com.etc.dysys.util.DBUtil;

public class UServiceDaoImpl implements UServiceDao {

	@Override
	public List<Service> getAllServices() {
		// TODO Auto-generated method stub
		String sql = "select * from service";
		return (List<Service>) DBUtil.select(sql, Service.class);
	}

}
