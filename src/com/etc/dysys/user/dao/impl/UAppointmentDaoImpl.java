package com.etc.dysys.user.dao.impl;

import java.util.List;

import com.etc.dysys.entity.Appointment;
import com.etc.dysys.entity.AppointmentView;
import com.etc.dysys.entity.Doctor;
import com.etc.dysys.entity.Service;
import com.etc.dysys.user.dao.UAppointmentDao;
import com.etc.dysys.util.DBUtil;

/**
 * 前台预约功能实现的DAO实现类
 * 
 * @author 黄志鹏
 *
 */
public class UAppointmentDaoImpl implements UAppointmentDao {
	/**
	 * 通过uId查询用户的预约信息的方法
	 */
	@Override
	public List<AppointmentView> getUserAppointmentSuccessByuId(int uId) {
		// TODO Auto-generated method stub
		String sql = "SELECT * FROM appointmentview where uId = ? and aStatus = 1 order by aCurrentTime desc";
		List<AppointmentView> list = (List<AppointmentView>) DBUtil.select(sql, AppointmentView.class, uId);
		return list;
	}

	/**
	 * 通过dId查询医生被预约信息的方法
	 */
	@Override
	public List<AppointmentView> getDoctorAppointmentBydId(int dId) {
		// TODO Auto-generated method stub
		String sql = "select * from appointmentview where dId=? and aStatus = 0 order by aCurrentTime desc";
		List<AppointmentView> list = (List<AppointmentView>) DBUtil.select(sql, AppointmentView.class, dId);
		return list;
	}

	/**
	 * 用户查询自己未完成的服务的方法
	 */
	@Override
	public List<AppointmentView> getUserAppointmentUnfinishedByuId(int uId) {
		// TODO Auto-generated method stub
		String sql = "SELECT * FROM appointmentview where uId = ? and aStatus = 0 order by aCurrentTime desc";
		List<AppointmentView> list = (List<AppointmentView>) DBUtil.select(sql, AppointmentView.class, uId);
		return list;
	}

	/**
	 * 用户取消未完成服务的方法
	 */
	@Override
	public int userDeleteAppointmentByaId(int aId) {
		// TODO Auto-generated method stub
		String sql = "DELETE FROM appointment WHERE aId = ?";
		int n = DBUtil.execute(sql, aId);
		return n;
	}

	/**
	 * 医生完成服务的方法
	 */
	@Override
	public int doctorCompleteAppointmentByaId(int aId) {
		// TODO Auto-generated method stub
		String sql = "update  appointment set aStatus= 1 where aId=?";
		int n = DBUtil.execute(sql, aId);
		return n;
	}

	/**
	 * 医生查询自己已经完成的服务的方法
	 */
	@Override
	public List<AppointmentView> getDoctorShowsuccessBydId(int dId) {
		// TODO Auto-generated method stub
		String sql = "select * from appointmentview where dId=? and aStatus = 1 order by aCurrentTime desc";
		List<AppointmentView> list = (List<AppointmentView>) DBUtil.select(sql, AppointmentView.class, dId);
		return list;
	}

	/**
	 * 用户预约服务
	 */
	@Override
	public boolean addAppointment(Appointment appointment) {
		// TODO Auto-generated method stub
		String sql = "insert into appointment(aCurrentTime,aTime,uId,sId,aStatus) values(now(),?,?,?,?)";
		return DBUtil.execute(sql, appointment.getaTime(), appointment.getuId(), appointment.getsId(),
				appointment.getaStatus()) > 0;
	}

	@Override
	public Service getServiceByTitle(String sTitle) {
		// TODO Auto-generated method stub
		String sql = "select * from service where sTitle=?";
		List<Service> list = (List<Service>) DBUtil.select(sql, Service.class, sTitle);
		return list.get(0);
	}

	@Override
	public Doctor getDoctorByName(String dName) {
		// TODO Auto-generated method stub
		String sql = "select * from doctor where dName=?";
		List<Doctor> list = (List<Doctor>) DBUtil.select(sql, Doctor.class, dName);
		return list.get(0);
	}

}
