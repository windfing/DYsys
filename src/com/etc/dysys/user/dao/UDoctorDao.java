package com.etc.dysys.user.dao;

import java.util.List;

import com.etc.dysys.entity.Doctor;

/**
 * 前台医生操作的DAO
 * @author 黄志鹏
 *
 */
public interface UDoctorDao {
/**
 * 医生登录的方法
 * @param account 医生的账户名
 * @param ipassword 经过加密后的密码
 * @return 查询到的医生对象
 */
	public Doctor getDoctorByNameandPassword(String account, String ipassword);

/**
 * 根据医生Id获取医生的方法
 * @param dId
 * @return
 */
public Doctor getDoctorBydId(int dId);

	
	
	/**
	 * 查询医生的方法(首页)
	 * @return  集合
	 */
	public List<Doctor> getDoctors();
	
	
	/**
	 * 查询所有医生的方法倒序(专家团队)
	 * @return 医生的集合
	 */
	public List<Doctor> getDoctorTeam();


}
