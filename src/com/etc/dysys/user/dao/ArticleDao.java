package com.etc.dysys.user.dao;

import java.util.List;

import com.etc.dysys.admin.dao.KnowledgeDao;
import com.etc.dysys.entity.KnowledegeIndex;
import com.etc.dysys.entity.Knowledge;
import com.etc.dysys.entity.KnowledgeView;
import com.etc.dysys.util.PageData;
/**
 * 文章知识的dao
 * @author 蒋丽娟
 *
 */
public interface ArticleDao {
	public PageData<KnowledgeView> allArticle(int page,int pageSize,String content);
	public PageData<Knowledge> personUserA(int page,int pageSize,int uId);
	public PageData<Knowledge> personDoctorA(int page,int pageSize,int dId);
	public boolean addUser(String kTitle,String  kContent,int uId);
	public boolean addDoctor(String kTitle,String  kContent,int dId);
	public boolean delete(int kId);
	public KnowledgeView show(int kId);
	public List<KnowledegeIndex> index();
	
	

}
