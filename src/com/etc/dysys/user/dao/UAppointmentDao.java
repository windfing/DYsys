package com.etc.dysys.user.dao;

import java.util.List;

import com.etc.dysys.entity.Appointment;
import com.etc.dysys.entity.AppointmentView;
import com.etc.dysys.entity.Doctor;
import com.etc.dysys.entity.Service;

/**
 * 前台预约操作的Dao接口
 * 
 * @author 黄志鹏
 *
 */
public interface UAppointmentDao {
	/**
	 * 通过uId查询用户已完成的预约信息的方法
	 * 
	 * @param uId
	 *            要查询的用户id
	 * @return
	 */
	public List<AppointmentView> getUserAppointmentSuccessByuId(int uId);

	/**
	 * 通过dId查询医生被预约的信息的方法
	 * 
	 * @param dId
	 *            要查询的医生Id
	 * @return
	 */
	public List<AppointmentView> getDoctorAppointmentBydId(int dId);

	/**
	 * 用户查自己未完成的服务的方法
	 * 
	 * @param uId
	 * @return
	 */
	public List<AppointmentView> getUserAppointmentUnfinishedByuId(int uId);

	/**
	 * 用户取消未完成的服务的方法
	 * 
	 * @param aId
	 * @return
	 */
	public int userDeleteAppointmentByaId(int aId);

	/**
	 * 医生完成服务的方法
	 * 
	 * @param aId
	 * @return
	 */
	public int doctorCompleteAppointmentByaId(int aId);

	/**
	 * 医生 查询自己已经完成的服务的方法
	 * 
	 * @param dId
	 * @return
	 */
	public List<AppointmentView> getDoctorShowsuccessBydId(int dId);

	/**
	 * 客户预约服务的方法
	 * 
	 * @param appointment
	 * @return
	 */
	public boolean addAppointment(Appointment appointment);

	/**
	 * 得到相应的Service
	 * 
	 * @param sTitle
	 * @return
	 */
	public Service getServiceByTitle(String sTitle);

	/**
	 * 得到相应的doctor
	 * 
	 * @param sTitle
	 * @return
	 */
	public Doctor getDoctorByName(String dName);

}
