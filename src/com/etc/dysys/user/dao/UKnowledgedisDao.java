package com.etc.dysys.user.dao;

import java.util.List;

import com.etc.dysys.entity.KdisKnowUserDoc;
import com.etc.dysys.entity.Knowledgedis;
import com.etc.dysys.entity.UKnowledgedisVo;

/**
 * 前台用户进行知识评论的dao
 * @author 薛佳鑫
 *
 */
public interface UKnowledgedisDao {
	/**
	 * 前台用户新增评论
	 * @param kd  知识评论对象
	 * @return    布尔值
	 */
	public boolean addKnowledgedis(Knowledgedis kd);
	
	
	/**
	 * 查询所有的知识评论  降序
	 * @return 评论集合
	 */
	
	public List<UKnowledgedisVo> getUKnowledgedis(int kId);
	
	/**
	 * 查询评论总数
	 * @param kId  文章的id
	 * @return 查询数
	 */
	public int  getUKnowledgedisCount(int kId);
	
	
	
}
