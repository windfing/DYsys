package com.etc.dysys.user.dao;

import java.util.List;

import com.etc.dysys.entity.Service;

/**
 * 
 * @author 高文乾
 *
 */
public interface UServiceDao {

	public List<Service> getAllServices();

}
