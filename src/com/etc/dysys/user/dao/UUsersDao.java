package com.etc.dysys.user.dao;

import com.etc.dysys.entity.Users;

/**
 * 前台user操作的DAO接口
 * @author 黄志鹏
 *
 */
public interface UUsersDao {
	/**
	 * 保存用户的持久层方法
	 * @param users 要保存的用户信息
	 * @return 操作的影响行数
	 */
	public int saveUser(Users users);
	/**
	 * 注册表单异步校验手机号是否存在的方法（即根据手机号查询是否存在用户）
	 * @return
	 */
	public Users checkPhone(String uPhone);
	/**
	 * 根据手机号及密码查询用户信息的方法（即登录的方法）
	 * @param uPhone 用户手机号
	 * @param uPassword 用户密码
	 * @return 查询到的用户信息
	 */
	public Users getUserByuPhoneanduPassword(String uPhone, String uPassword);
	/**
	 * 根据用户Id查询用户
	 * @param uId 用户的id
	 * @return
	 */
	public Users getUserByuId(int uId);
	/**
	 * 修改用户信息的方法
	 * @param users 
	 * @return
	 */
	public int updateUsers(Users users);
	/**
	 * 根据用户名查询用户名是否存在
	 * @param getuName
	 * @return
	 */
	public Users checkuName(String uName);

}
