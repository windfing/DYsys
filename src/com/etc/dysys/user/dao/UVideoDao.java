package com.etc.dysys.user.dao;

import java.util.List;

import com.etc.dysys.entity.Video;

public interface UVideoDao {
	/**
	 * 查询最新4个视频
	 * @return
	 */
	public List<Video> getFourVideos();

}
