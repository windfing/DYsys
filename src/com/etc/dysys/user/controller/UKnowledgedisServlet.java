package com.etc.dysys.user.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.etc.dysys.admin.service.KnowledgedisService;
import com.etc.dysys.admin.service.impl.KnowledgedisServiceImpl;
import com.etc.dysys.entity.Doctor;
import com.etc.dysys.entity.KdisKnowUserDoc;
import com.etc.dysys.entity.KnowledgeView;
import com.etc.dysys.entity.Knowledgedis;
import com.etc.dysys.entity.UKnowledgedisVo;
import com.etc.dysys.entity.Users;
import com.etc.dysys.user.service.ArticleService;
import com.etc.dysys.user.service.UKnowledgedisService;
import com.etc.dysys.user.service.impl.ArticleServiceImpl;
import com.etc.dysys.user.service.impl.UKnowledgedisServiceImpl;
import com.etc.dysys.util.AjaxLogicBean;
import com.google.gson.Gson;

/**
 * Servlet implementation class UKnowledgedisServlet
 */
/**
 * 前台用户知识评论servlet
 * @author 薛佳鑫
 *
 */
@WebServlet("/UKnowledgedisServlet.action")
public class UKnowledgedisServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	UKnowledgedisService ukds=new UKnowledgedisServiceImpl();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UKnowledgedisServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		String op="querykd";
		
		if(request.getParameter("op")!=null) {
			op=request.getParameter("op");
		}
		if(op.equals("querykd")) {
			doQuerykd(request,response);
		}else if(op.equals("addkd")) {
			doAddkd(request,response);
		}
	}
	
	/**
	 * 在前台展示知识评论
	 * @param request
	 * @param responses
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doQuerykd(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int kId=Integer.parseInt(request.getParameter("kId"));
		AjaxLogicBean<UKnowledgedisVo> alb=ukds.getUKnowledgedis(kId);
		ArticleService as=new ArticleServiceImpl();
		KnowledgeView k=as.show(kId);
		List<UKnowledgedisVo> list=alb.getData();
		request.setAttribute("kShow", k);
		//调用查询评论数的方法
		int count=ukds.getUKnowledgedisCount(kId);
		//存储到属性范围
		request.setAttribute("list", list);
		request.setAttribute("count", count);
		request.getRequestDispatcher("user/articleComment.jsp").forward(request, response);	
	}
	
	
	
	/**
	 * 用户对知识进行评论
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doAddkd(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		if(request.getParameter("kdContent")!=null) {
			String kdContent=request.getParameter("kdContent");
			int kId=Integer.parseInt(request.getParameter("kId"));
			
			Users users=(Users) request.getSession().getAttribute("users");
			Doctor doctor=(Doctor) request.getSession().getAttribute("doctor");
			Knowledgedis kd=new Knowledgedis();
			kd.setKdContent(kdContent);
			if(doctor!=null) {
				kd.setdId(doctor.getdId());
			}
			if(users!=null) {
				kd.setuId(users.getuId());
			}
			kd.setkId(kId);
			boolean flag=ukds.addKnowledgedis(kd);
			
			PrintWriter out=response.getWriter();
			if(flag) {
				out.print("评论成功");
			}else if(users==null&&doctor==null) {
				out.print("请先登录");
			}
			
			else {
				out.println("评论失败");
			}
			out.close();
		}
		
		
	}

}
