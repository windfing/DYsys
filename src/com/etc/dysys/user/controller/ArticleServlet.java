package com.etc.dysys.user.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.jasper.tagplugins.jstl.core.Out;
import org.omg.CORBA.INTERNAL;

import com.etc.dysys.entity.KnowledegeIndex;
import com.etc.dysys.entity.Knowledge;
import com.etc.dysys.entity.KnowledgeView;
import com.etc.dysys.user.service.ArticleService;
import com.etc.dysys.user.service.impl.ArticleServiceImpl;
import com.etc.dysys.util.AjaxLogicBean;
import com.etc.dysys.util.PageData;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;


/**
 * 知识文章管理的服务端
 * @author 蒋丽娟
 *
 */
@WebServlet("/ArticleServlet.action")
public class ArticleServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
  ArticleService as=new ArticleServiceImpl();

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		String op="";
		if(request.getParameter("op")!=null){
			op=request.getParameter("op");
		}
       if("allArticle".equals(op)) {
			doPage(request,response);
       }
       if("personArticle".equals(op)) {
    	   doPersonK(request,response);
       }
       if("addArticle".equals(op)) {
    	   
    	   doAdd(request,response);
       }
       if("deleteArticle".equals(op)) {
    	   delete(request,response);
       }
       if("articalIndex".equals(op)) {
    	   doIndex(request,response);
       }

	}
	
	protected void doIndex(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out=response.getWriter();
	response.setContentType("application/json");
		List<KnowledegeIndex> kd=as.index();
		AjaxLogicBean<KnowledegeIndex> ss=new AjaxLogicBean<KnowledegeIndex>();
		
		ss.setData(kd);
		Gson gson = new Gson();
		String pd=gson.toJson(ss);
		
		out.print(pd);
		out.close();
	}
	
	protected void delete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int kId=Integer.parseInt(request.getParameter("kId"));
		PrintWriter out=response.getWriter();
		boolean flag=as.delete(kId);
		if(flag) {
			out.print("删除成功");
		}else {
			out.print("删除失败");
		}
		out.close();
	
	}
	protected void doAdd(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String kTitle=request.getParameter("kTitle");
		String kContent=request.getParameter("kContent");
		
		PrintWriter out=response.getWriter();
		boolean flag=false;
		//登录的是医生，用户uId是空的
		
		if(request.getParameter("uId").equals("")) {
			int dId=Integer.parseInt(request.getParameter("dId"));
		    flag=as.addDoctor(kTitle, kContent, dId);
		}else if(request.getParameter("dId").equals("")) {//登录的是用户，医生dId是空的
			int uId=Integer.parseInt(request.getParameter("uId"));
			flag=as.addUser(kTitle, kContent, uId);
		}
		if(flag) {
			out.print("发表成功");
		}else {
			out.print("发表失败");
		}
		out.close();
		
	}
	
protected void doPersonK(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
			int page = 1;
			int pageSize=4;
			PageData<Knowledge> pd = null;
			if(request.getParameter("page")!=null) {
				page=Integer.parseInt(request.getParameter("page"));
			}
			if(request.getParameter("pageSize")!=null) {
				pageSize=Integer.parseInt(request.getParameter("pageSize"));
			}
			if(!request.getParameter("uId").equals("")) {
				int uId=Integer.parseInt(request.getParameter("uId"));
				pd=as.personUserA(page, pageSize, uId);	
			}
			if(!request.getParameter("dId").equals("")) { 
				int dId=Integer.parseInt(request.getParameter("dId"));
				pd=as.personDoctorA(page, pageSize, dId);
			}
			request.setAttribute("pd", pd);
			request.getRequestDispatcher("user/articlePerson.jsp").forward(request, response);
		}
	protected void doPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String content="";
			int page=1;
			int pageSize=4;
			if(request.getParameter("page")!=null) {
				page=Integer.parseInt(request.getParameter("page"));
			}
			if(request.getParameter("pageSize")!=null) {
				pageSize=Integer.parseInt(request.getParameter("pageSize"));
			}
			if(request.getParameter("content")!=null) {
				content=request.getParameter("content");
			}	
		PageData<KnowledgeView> pd=as.allArticle(page, pageSize, content);
		request.setAttribute("pd", pd);
		request.setAttribute("content", content);
			request.getRequestDispatcher("user/article.jsp").forward(request, response);
		}
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
