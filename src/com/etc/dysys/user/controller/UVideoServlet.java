package com.etc.dysys.user.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.etc.dysys.entity.Video;
import com.etc.dysys.user.service.UVideoService;
import com.etc.dysys.user.service.impl.UVideoServiceImpl;
import com.etc.dysys.util.AjaxLogicBean;
import com.google.gson.Gson;

/**
 * Servlet implementation class UVideoServlet
 */
@WebServlet("/UVideo.action")
public class UVideoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private UVideoService uvs=new UVideoServiceImpl(); 
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//设置请求 响应的编码格式
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		String op="";
		if(request.getParameter("op")!=null) {
			op= request.getParameter("op");
		} 
		
		if(op.equals("queryVideos")) {
			doQueryVideos(request, response);
		}
	}
	
	
	protected void doQueryVideos(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("application/json");
		AjaxLogicBean<Video> videos=uvs.getFourVideos();
		Gson gson=new Gson();
		String str=gson.toJson(videos);
		//System.out.println("str:"+str);
		PrintWriter out=response.getWriter();
		out.print(str);
		out.close();
		
	}
	

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
