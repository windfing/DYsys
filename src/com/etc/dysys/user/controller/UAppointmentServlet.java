package com.etc.dysys.user.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.etc.dysys.entity.Appointment;
import com.etc.dysys.entity.AppointmentView;
import com.etc.dysys.entity.Service;
import com.etc.dysys.user.service.UAppointmentService;
import com.etc.dysys.user.service.impl.UAppointmentServiceImpl;
import com.etc.dysys.util.AjaxLogicBean;
import com.etc.dysys.util.CommonMessage;
import com.google.gson.Gson;

/**
 * Servlet implementation class UAppointmentServlet
 */
@WebServlet("/UAppointment.action")
public class UAppointmentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UAppointmentService uAppointmentService = new UAppointmentServiceImpl();

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// 设置request的编码格式
		request.setCharacterEncoding("utf-8");
		// 设置响应编码
		response.setCharacterEncoding("utf-8");
		String op = "";
		if (request.getParameter("op") != null) {
			op = request.getParameter("op");
		}
		if (op.equals("userShowsuccess")) {
			doUserShowsuccess(request, response);
		} else if (op.equals("doctorAppointmentfinished")) {
			doDoctorAppointmentfinished(request, response);
		} else if (op.equals("userShowunfinished")) {
			doUserShowunfinished(request, response);
		} else if (op.equals("userCanceAppointment")) {
			doUserCanceAppointment(request, response);
		} else if (op.equals("doctorCompleteAppointment")) {
			doDoctorCompleteAppointment(request, response);
		} else if (op.equals("doctorShowsuccess")) {
			doDoctorShowsuccess(request, response);
		} else if (op.equals("addAppointment")) {
			doAddAppointment(request, response);
		} else if (op.equals("addAppAjax")) {
			doAddAppointment02(request, response);
		}
	}

	/**
	 * 步骤二：
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doAddAppointment02(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String uName = request.getParameter("uName");
		String aTime = request.getParameter("aTime");
		// String aDoctor = request.getParameter("aDoctor");
		String aService = request.getParameter("aService");
		int uId = Integer.parseInt(request.getParameter("uId"));
		// 设置返回格式
		response.setContentType("application/json");
		// Doctor doctor = uAppointmentService.getDoctorByName(aDoctor);
		Service service = uAppointmentService.getServiceByTitle(aService);
		// int dId = doctor.getdId();
		int sId = service.getsId();
		// System.out.println("输出结果：" + uId + "," + aTime + "," + dId + "," + sId);

		Appointment appointment = new Appointment("now()", aTime, uId, sId, 0);
		boolean flag = uAppointmentService.addAppointment(appointment);
		// System.out.println(flag);
		PrintWriter out = response.getWriter();
		CommonMessage comm = new CommonMessage();
		comm.setMsg(flag ? "预约成功" : "预约失败");
		/*
		 * String msg = "预约失败"; if (flag) { msg = "预约成功"; out.print(msg); }
		 */

		Gson gson = new Gson();
		String commStr = gson.toJson(comm);
		out.print(commStr);
		out.close();

	}

	/**
	 * 客户预约服务 步骤一：
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doAddAppointment(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int sId = Integer.parseInt(request.getParameter("sId"));
		int uId = Integer.parseInt(request.getParameter("uId"));
		String uName = request.getParameter("uName");
		// System.out.println("sId:" + sId + ",uId:" + uId + ",uName:" + uName);

		request.setAttribute("uName", uName);
		request.getRequestDispatcher("user/appointment.jsp").forward(request, response);

	}

	/**
	 * 医生查询自己被预约且已完成的服务的方法
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doDoctorShowsuccess(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int dId = Integer.parseInt(request.getParameter("dId"));
		// List<AppointmentView> list =uAppointmentService.getUserAppointment(uId);
		AjaxLogicBean<AppointmentView> data = uAppointmentService.getDoctorShowsuccess(dId);
		// 设置响应格式
		response.setContentType("application/json");
		Gson gson = new Gson();
		String pd = gson.toJson(data);
		PrintWriter out = response.getWriter();
		out.print(pd);
		out.close();

	}

	/**
	 * 医生完成服务的操作
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doDoctorCompleteAppointment(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/json");
		int aId = Integer.parseInt(request.getParameter("aId"));
		boolean flag = uAppointmentService.doctorCompleteAppointmentByaId(aId);
		CommonMessage comm = new CommonMessage();
		comm.setMsg(flag ? "操作成功" : "操作失败");
		Gson gson = new Gson();
		String commStr = gson.toJson(comm);
		PrintWriter out = response.getWriter();
		out.print(commStr);
		out.close();
	}

	/**
	 * 用户取消自己未完成的预约的信息的方法
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */

	protected void doUserCanceAppointment(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int aId = Integer.parseInt(request.getParameter("aId"));
		// List<AppointmentView> list =uAppointmentService.getUserAppointment(uId);
		boolean flag = uAppointmentService.userDeleteAppointmentByaId(aId);
		// 设置响应格式
		response.setContentType("application/json");
		Gson gson = new Gson();
		CommonMessage comm = new CommonMessage();
		comm.setMsg(flag ? "操作成功" : "操作失败");
		String commStr = gson.toJson(comm);
		PrintWriter out = response.getWriter();
		out.print(commStr);
		out.close();
	}

	/**
	 * 用户查询自己未完成的预约的信息的方法
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */

	protected void doUserShowunfinished(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int uId = Integer.parseInt(request.getParameter("uId"));
		// List<AppointmentView> list =uAppointmentService.getUserAppointment(uId);
		AjaxLogicBean<AppointmentView> data = uAppointmentService.getUserAppointmentUnfinishedByuId(uId);
		// 设置响应格式
		response.setContentType("application/json");
		Gson gson = new Gson();
		String pd = gson.toJson(data);
		PrintWriter out = response.getWriter();
		out.print(pd);
		out.close();
	}

	/**
	 * 医生查询自己被预约且未完成的服务的信息的方法
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */

	protected void doDoctorAppointmentfinished(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int dId = Integer.parseInt(request.getParameter("dId"));
		// List<AppointmentView> list =uAppointmentService.getUserAppointment(uId);
		AjaxLogicBean<AppointmentView> data = uAppointmentService.getDoctorAppointment(dId);
		// 设置响应格式
		response.setContentType("application/json");
		Gson gson = new Gson();
		String pd = gson.toJson(data);
		PrintWriter out = response.getWriter();
		out.print(pd);
		out.close();
	}

	/**
	 * 用户查询自己已完成的预约的信息的方法
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */

	protected void doUserShowsuccess(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int uId = Integer.parseInt(request.getParameter("uId"));
		// List<AppointmentView> list =uAppointmentService.getUserAppointment(uId);
		AjaxLogicBean<AppointmentView> data = uAppointmentService.getUserAppointmentSuccessByuId(uId);
		// 设置响应格式
		response.setContentType("application/json");
		Gson gson = new Gson();
		String pd = gson.toJson(data);
		PrintWriter out = response.getWriter();
		out.print(pd);
		out.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
