package com.etc.dysys.user.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.etc.dysys.entity.Service;
import com.etc.dysys.user.service.UServiceService;
import com.etc.dysys.user.service.UServiceServiceImpl;

/**
 * Servlet implementation class UServieceServlet
 */
@WebServlet("/UService.action")
public class UServieceServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	UServiceService uss = new UServiceServiceImpl();

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("utf-8");

		String op = "query";
		if (request.getParameter("op") != null) {
			op = request.getParameter("op");
		}

		if (op.equals("query")) {
			doQuery(request, response);
		}
	}

	/**
	 * 遍历所有数据
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doQuery(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<Service> list = uss.getAllServices();
		list.forEach(System.out::println);
		request.setAttribute("list", list);
		request.getRequestDispatcher("user/service.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
