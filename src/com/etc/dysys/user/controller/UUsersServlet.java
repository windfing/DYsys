package com.etc.dysys.user.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.etc.dysys.entity.Doctor;
import com.etc.dysys.entity.Users;
import com.etc.dysys.user.service.UUsersService;
import com.etc.dysys.user.service.impl.UUsersServiceImpl;
import com.etc.dysys.util.CommonMessage;
import com.google.gson.Gson;

/**
 * Servlet implementation class UUsersServlet
 */
@WebServlet("/UUsers.action")
public class UUsersServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UUsersService uUsersService = new UUsersServiceImpl();

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//设置request的编码格式
		request.setCharacterEncoding("utf-8");
		//设置response的编码格式
		response.setCharacterEncoding("utf-8");
		String op ="";
		if(request.getParameter("op")!=null) {
			op=request.getParameter("op");
		}
		if(op.equals("regist")) {
			doRegist(request,response);
		}else if(op.equals("checkPhone")) {
			doCheckPhone(request,response);
		}else if(op.equals("modify")) {
			doModify(request,response);
		}else if(op.equals("update")) {
			doUpdate(request,response);
		}else if(op.equals("personal")) {
			doPersonal(request,response);
		}else if(op.equals("checkuName")) {
			doCheckuName(request,response);
		}
		
	}
	
	
	
	/**
	 * 用户点击个人中心将信息显示在页面上
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doPersonal(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//获取用户id
		int uId = Integer.parseInt(request.getParameter("uId"));
		//通过uid获取用户对象
		Users users =uUsersService.getUserByuId(uId);
		request.setAttribute("users", users);
		request.getRequestDispatcher("/user/userPersonal.jsp").forward(request, response);;
	}
	
	
	
	/**
	 * 用户修改个人信息的方法
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doUpdate(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//获取页面传输的信息
	int uId = Integer.parseInt(request.getParameter("uId"));
	String uName = request.getParameter("uName");
	String uPassword = request.getParameter("uPassword");
	String uSex=request.getParameter("uSex");
//	String uPhone = request.getParameter("uPhone");
	String uBirthday = request.getParameter("uBirthday");
	String uAddress= request.getParameter("uAddress");
	Users users = new Users(uId, uName, uPassword, uSex, "", uBirthday, uAddress, 0);
	boolean flag = uUsersService.updateUsers(users);
	//设置响应的格式
	response.setContentType("application/json");
	//设置返回信息
	CommonMessage comm = new CommonMessage();
	comm.setMsg(flag?"修改成功":"修改失败");
	Gson gson = new Gson();
	String commStr = gson.toJson(comm);
	PrintWriter out = response.getWriter();
	out.print(commStr);
	out.close();
	}
	
	
	
	
	/**
	 * 用户修改自己的信息前获取用户信息界面显示
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doModify(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//获取用户id
		int uId = Integer.parseInt(request.getParameter("uId"));
		//通过uid获取用户对象
		Users users =uUsersService.getUserByuId(uId);
		request.setAttribute("users", users);
		request.getRequestDispatcher("/user/userModify.jsp").forward(request, response);;
	}
	
	
	
	
	/**
	 * 注册时Ajax校验手机号码是否存在的方法
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doCheckuName(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//设置response 的格式
		response.setContentType("application/json");
		//接收uPhone
		String uName =request.getParameter("uName");
		boolean flag = uUsersService.checkuName(uName);
		PrintWriter out = response.getWriter();
		String msg = "";
		if(flag) {
			//号码已经存在
			msg="该用户名已经被注册";
			
		}else {
			//号码不存在
			msg="该用户名可以注册";
		}
		//设置返回的信息
		CommonMessage comm = new CommonMessage();
		comm.setMsg(msg);
		Gson gson =  new  Gson();
		String commstr=gson.toJson(comm);
		//返回信息
		out.print(commstr);
		out.close();
		
	}
	
	
	
	
	
	
	
	
	
	/**
	 * 注册时Ajax校验手机号码是否存在的方法
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doCheckPhone(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//设置response 的格式
		response.setContentType("application/json");
		//接收uPhone
		String uPhone =request.getParameter("uPhone");
		boolean flag = uUsersService.checkPhone(uPhone);
		PrintWriter out = response.getWriter();
		String msg = "";
		if(flag) {
			//号码已经存在
			msg="该号码已经被注册";
			
		}else {
			//号码不存在
			msg="该号码可以注册";
		}
		//设置返回的信息
		CommonMessage comm = new CommonMessage();
		comm.setMsg(msg);
		Gson gson =  new  Gson();
		String commstr=gson.toJson(comm);
		//返回信息
		out.print(commstr);
		out.close();
		
	}
	
	
	
	/**
	 * 注册的方法
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doRegist(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//设置响应的格式
		response.setContentType("application/json");
		String uName= request.getParameter("uName");
		String uPassword=request.getParameter("uPassword");
		String uSex=request.getParameter("uSex");
		String uPhone = request.getParameter("uPhone");
		String uBirthday= request.getParameter("uBirthday");
		String uAddress=request.getParameter("uAddress");
		Users users = new Users(0, uName, uPassword, uSex, uPhone, uBirthday, uAddress, 0);
		boolean flag = uUsersService.saveUser(users);
		CommonMessage comm = new CommonMessage();
		comm.setMsg(flag?"注册成功":"注册失败");
		Gson gson = new Gson();
		String commStr = gson.toJson(comm);
		PrintWriter out = response.getWriter();
		out.print(commStr);
	//		if(flag) {
//			//注册成功，跳转到首页
//			response.sendRedirect("user/index.jsp");
//		}else {
//			//注册失败
//			
//		}
	}
	
	
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
