package com.etc.dysys.user.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.etc.dysys.entity.Doctor;
import com.etc.dysys.entity.Users;
import com.etc.dysys.user.service.UDoctorService;
import com.etc.dysys.user.service.UUsersService;
import com.etc.dysys.user.service.impl.UDoctorServiceImpl;
import com.etc.dysys.user.service.impl.UUsersServiceImpl;
import com.etc.dysys.util.CommonMessage;
import com.google.gson.Gson;

/**
 * Servlet implementation class ULogin 前台医生或者用户登录、退出的servlet
 */
@WebServlet("/ULogin.action")
public class ULoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UUsersService uUsersService = new UUsersServiceImpl();
	private UDoctorService uDoctorService = new UDoctorServiceImpl();

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// 设置request的编码格式
		request.setCharacterEncoding("utf-8");
		// 设置response的编码格式
		response.setCharacterEncoding("utf-8");
		String op = "";
		if (request.getParameter("op") != null) {
			op = request.getParameter("op");
		}
		if (op.equals("login")) {
			doLogin(request, response);
		}else if(op.equals("exit")) {
			doExit(request, response);
				
			
		}

	}
	
	/**
	 * 用户或医生退出系统登录的方法
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doExit(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession();
		session.invalidate();
	}
	
	
	
	
	

	/**
	 * 用户登录和医生登录的方法
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doLogin(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String account = request.getParameter("account");
		String Password = request.getParameter("Password");
		//普通用户登录
		Users users = uUsersService.getUserByuPhoneanduPassword(account, Password);
		//医生登录
		Doctor doctor = uDoctorService.getDoctorByNameandPassword(account,Password);
		// 设置响应的格式
		response.setContentType("application/json");
		String msg = "";
		//判断是医生登录还是用户登录
		if (users != null) {
			// 登录成功
			HttpSession session = request.getSession();
			// 将用户信息存放在session域中
			session.setAttribute("users", users);
			// 设置返回信息
			msg = "用户登录成功";
		} else if (doctor != null) {
			// 医生登录成功
			HttpSession session = request.getSession();
			// 将用户信息存放在session域中
			session.setAttribute("doctor", doctor);
			// 设置返回信息
			msg = "医生登录成功";
		}else{
			msg = "账号或密码错误，登录失败，请重新登录";
		}
		CommonMessage comm = new CommonMessage();
		comm.setMsg(msg);
		Gson gson = new Gson();
		String commStr = gson.toJson(comm);
		PrintWriter out = response.getWriter();
		out.print(commStr);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
