package com.etc.dysys.user.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.etc.dysys.entity.Doctor;
import com.etc.dysys.user.service.UDoctorService;
import com.etc.dysys.user.service.impl.UDoctorServiceImpl;
import com.etc.dysys.util.AjaxLogicBean;
import com.google.gson.Gson;

/**
 * Servlet implementation class UDoctor
 */
@WebServlet("/UDoctor.action")
public class UDoctorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UDoctorService uDoctorService = new  UDoctorServiceImpl();
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//设置请求编码
		request.setCharacterEncoding("utf-8");
		response.setCharacterEncoding("utf-8");
		String op ="";
		if(request.getParameter("op")!=null) {
			op= request.getParameter("op");
		} 
		if(op.equals("personal")) {
			doPersonal(request,response);
		}else if(op.equals("modify")) {
			doModify(request,response);
		}else if(op.equals("queryDocs")) {
			doQuery(request,response);
			}else if(op.equals("queryDocTeam")) {
				doQueryDocTeam(request,response);
			}
		
	}
	
	/**
	 * 医生进入修改个人信息的方法
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doModify(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int dId = Integer.parseInt(request.getParameter("dId"));
		Doctor doctor=uDoctorService.getDoctorBydId(dId);
		request.setAttribute("doctor", doctor);
		request.getRequestDispatcher("/user/doctorModify.jsp").forward(request, response);;
	}
	
	
	/**
	 * 医生个人中心的方法
	 * @param request
	 * @param response
	 * @throws ServletException
	 * @throws IOException
	 */
	protected void doPersonal(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int dId = Integer.parseInt(request.getParameter("dId"));
		Doctor doctor=uDoctorService.getDoctorBydId(dId);
		request.setAttribute("doctor", doctor);
		request.getRequestDispatcher("/user/doctorPersonal.jsp").forward(request, response);;
	}
	
	protected void doQuery(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("application/json");
		AjaxLogicBean<Doctor> alb=uDoctorService.getDoctors();
		Gson gson=new Gson();
		String str=gson.toJson(alb);
		PrintWriter out=response.getWriter();
		out.print(str);
		out.close();
	}
	
	protected void doQueryDocTeam(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.setContentType("application/json");
		AjaxLogicBean<Doctor> alb=uDoctorService.getDoctorTeam();
		Gson gson=new Gson();
		String str=gson.toJson(alb);
		PrintWriter out=response.getWriter();
		out.print(str);
		out.close();
	}


}
